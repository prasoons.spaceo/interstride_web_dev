// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require sweetalert2
// require jquery-2.1.1.min
//= require jquery
//= require jquery_ujs
// require rails-ujs
//= require jquery-ui
// require readmore
//= require activestorage
//= require bootstrap.min
//= require bootstrap-datepicker
//= require bootstrap-timepicker
//= require jquery.touchSwipe.min
//= require rAF
//= require ResizeSensor
//= require sticky-sidebar
//= require select2
// require script
// require custom
//= require underscore
// require gmaps/google
//= require toastr.min
//= require jquery_nested_form
//= require jquery.validate
//= require social-share-button
// require data-confirm-modal
//= require jquery.mCustomScrollbar.min
//= require exif
//= require croppie
//= require demo
// require lazyr
//= require jquery.lazyload
// require sweet-alert2-rails
// require jquery.smartWizard
// require markerclusterer
// require Chart.min
// require_self
// require turbolinks
// require_tree .

$(function() {
  $('.loader')
    .hide()  // hide it initially.
    .ajaxStart(function() {
      $(this).show(); // show on any Ajax event.
    })
    .ajaxStop(function() {
      $(this).hide(); // hide it when it is done.
  });
  // $( "form" ).submit(function( ) {
  //     if(("form").valid()){
  //         $('.loader').show()
  //       }
  //   });

});

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};


$(function() {
  $('.loader').hide();  // hide it initially.
  $(document)
    .ajaxStart(function() {
        console.log('ajaxStart');
      $('.loader').show(); // show on any Ajax event.
    })
    .ajaxStop(function() {
        // console.log('ajaxend');
      $('.loader').hide(); // hide it when it is done.
  });
});


(function($){
  $(window).on("load",function(){
    $(".vr_scroll").mCustomScrollbar({
      axis:"y",
      theme:"dark-3",
      advanced: {
        autoExpandVerticalScroll:true //optional (remove or set to false for non-dynamic/static elements)
      }
    });
  });
})(jQuery);

// document.onreadystatechange = function () {
//     var state = document.readyState
//     if( (state == 'interactive') || (state == 'loading')) {
//         $('.loader').show()
//     }else if (state == 'complete') {
//         setTimeout(function () {
//             $('.loader').hide()
//         }, 1000)
//     }
// }

$(document).ready(function(){

    // init instance for lazy loading
        // const instance = Layzr()
        $(function() {
            $("img.lazy").lazyload();
          });
        // $("img").lazyload({
        //     threshold : 500,
        //     effect : "fadeIn"
        // })

    // Sweet Alert settings
        $.rails.allowAction = function(link){
          if (link.data("confirm") == undefined){
            return true;
          }
          $.rails.showConfirmationDialog(link);
          return false;
        }

        //User click confirm button
        $.rails.confirmed = function(link){
          link.data("confirm", null);
          link.trigger("click.rails");
        }

        //Display the confirmation dialog
        $.rails.showConfirmationDialog = function(link){
          var message = link.data("confirm");
          swal({
            title: message,
            type: 'warning',
            confirmButtonText: 'Yes',
            confirmButtonColor: '#839ca8',
            cancelButtonColor: '#ff5a5f',
            showCancelButton: true
          }).then(function(e){
            $.rails.confirmed(link);
          }, function(dismiss) {});
        };
    // End Sweet Alert settings
     $('.loader').hide()
     $('.preview-card').hide()
    $('.questions').hide()
    $('#question_0').show()
    $('#signup2').hide()
    $('.company').hide();
    // $('.jobs_list').hide()
    $('.type_select').select2({placeholder: 'Select Type'})
    $('#job_type_dropdown').select2({placeholder: 'Select JobType'})
    $('.areatype_select').select2({placeholder: 'Select Area Type'})
    $('.year_select').select2({placeholder: 'Select Year'})
    $('.gameplan_question_select').select2({placeholder: 'Please Select a Country'})
    $('.steps').hide()
    // $('#sort_by_select').select2({placeholder: 'Select Type'});
    // $('#job_type_select').select2({placeholder: 'Select Type'});
    // $('#publish_select').select2({placeholder: 'Select Type'});
    $('#job_search_company_name').select2({placeholder: 'Select Type'});
    // $('.list_company').addClass('hide_list')

    $('.post-footer .btn-default').css('display', 'none')
    // $('#search_birth_country_ids').hide()
    // $('#search_university_ids').hide()
    // $('#search_graduation_year_ids').hide()
    // $('#search_emp_function_ids').hide()

    $('#GreenCard').hide()
    $('#alumni').hide()

    // $('.chart-cell .star_icon').hide()
    // $('.chart-cell').on('hover', function(){
    //     $('.chart-cell .star_icon').show()
    // })
    //
    // $('.delete_post').hide()
    //
    // $('.post-content').on('mouseover', function(){
    //     console.log($(this).children());
    //     $(this).children().find('a.delete_post').show()
    //     // $('.delete_post').show()
    // })
    //
    // $('.post-content').on('mouseout', function(){
    //     $(this).children().find('a.delete_post').hide()
    // })

    $('.chart-cell .star_icon').hide()
    $('.chart-cell').on('mouseover', function(){
        // alert(1)
        // console.log($(this).children('div.star_icon'));
        $(this).children('div.star_icon').show()
        // $('.chart-cell .star_icon').show()
    })
    $('.chart-cell').on('mouseout', function(){
        // alert(1)
        $('.chart-cell .star_icon').hide()
    })


    $('.cell .delete_icon_network').hide()
    $('.cell').on('mouseover', function(){
        // alert(1)
        // console.log($(this).children('div.star_icon'));
        $(this).children().children('div.delete_icon_network').show()
        // $('.chart-cell .star_icon').show()
    })
    $('.cell').on('mouseout', function(){
        // alert(1)
        $('.cell .delete_icon_network').hide()
    })

    $('.step_1').show()
    $('.steps').removeClass('active')
    $('.step_1').addClass('active')

    // $('.chart-cell .star_icon').on('click', function(e){
    //     console.log($(this).children('a.fav-icon'));
    //     var company_id = $(this).children('a.fav-icon').data('company-id')
    //     // var tab_type = $(this).data('tab-type')
    //     var company_name = $(this).children('a.fav-icon').data('company-name')
    //     var like = 'unlike'
    //     if($(this).children('a.fav-icon').hasClass('active') == true){
    //         $(this).children('a.fav-icon').removeClass('active')
    //         like = 'unlike'
    //     }else {
    //         $(this).children('a.fav-icon').addClass('active')
    //         like = 'like'
    //     }
    //     e.preventDefault()
    //     e.stopImmediatePropagation()
    //     console.log(company_id);
    //     console.log(company_name);
    //     $.ajax({
    //         url: '/companies/add_company_as_favorite',
    //         // url: '/companies/add_company_as_favorite',
    //         method: 'post',
    //         data: { company_id: company_id, company_name: company_name, like: like },
    //         dataType: 'script',
    //         // success: function(response){
    //         //     $('.loader').hide()
    //         //     console.log(response);
    //         //     toastr.success(response['message'])
    //         //     window.location.reload(true)
    //         // },
    //         // failure: function(response){
    //         //     $('.loader').hide()
    //         // }
    //
    //     })
    // })

    $('.fav-comp .remove-fav-icon').hide()
    $('.fav-comp').on('mouseover', function(){
        $(this).children().find('a.remove-fav-icon').show()
        // $('.fav-comp .remove-fav-icon').show()
    })
    $('.fav-comp').on('mouseout', function(){
        // $('.fav-comp .remove-fav-icon').hide()
        $(this).children().find('a.remove-fav-icon').hide()
    })

    $('.fav-comp .remove-fav-icon').on('click', function(e){
        // console.log($(this).children('a.fav-icon'));
        var company_id = $(this).data('company-id')
        // var tab_type = $(this).data('tab-type')
        var company_name = $(this).children('a.fav-icon').data('company-name')
        var like = 'unlike'
        // if($(this).children('a.fav-icon').hasClass('active') == true){
        //     $(this).children('a.fav-icon').removeClass('active')
        //     like = 'unlike'
        // }else {
        //     $(this).children('a.fav-icon').addClass('active')
        //     like = 'like'
        // }
        e.preventDefault()
        e.stopImmediatePropagation()
        // console.log(company_id);
        // console.log(company_name);
        $.ajax({
            // url: '/companies/add_company_as_favorite',
            url: '/companies/add_company_as_favorite',
            method: 'post',
            data: { company_id: company_id, company_name: company_name, like: like },
            dataType: 'script',
            // success: function(response){
            //     $('.loader').hide()
            //     console.log(response);
            //     toastr.success(response['message'])
            //     window.location.reload(true)
            // },
            // failure: function(response){
            //     $('.loader').hide()
            // }

        })
        $('.chart-cell .star_icon').children("a.fav-icon-"+company_id).removeClass('active')
    })

    // $('.delete_post').hide()

    // Home screen remove Bookmark
    remove_bookmark_sidebar();


    $('#forgot-btn').click(function(){
        $('#forgot').addClass("show");
        $('#login').addClass("hide");
    });

    $('#login-btn').click(function(){
        $('#forgot').removeClass("show");
        $('#login').removeClass("hide");
    });
    // $('.list_company').css('display', 'none !important');
    load_home_content();
    video_screen_sidebar();
    video_screen_ordering();
    gameplan_anchor();
    delete_post();
    // $( ".select_state" ).select2({
    //     // theme: "bootstrap"
    //     placeholder: 'Select State',
    //     // allowClear: true,
    //     // multiple: true
    // });
    // $( ".select_city" ).select2({
    //     // theme: "bootstrap"
    //     placeholder: 'Select City',
    // });
    // $( ".select_title" ).select2({
    //     // theme: "bootstrap"
    //     placeholder: 'Select Job Title',
    //     // allowClear: true,
    //     // multiple: true
    // });
    visa_insight_select2()

    // fill_city();
    update_company_industry()
    $('.view_more_link').click(function(){
        var id = $(this).data('id')
        // alert(1)
        // console.log($(this).next().next());
        // $(this).parent().find('.collapsed').hide()
        // $(this).parent()).hide();
        // $(this).next().find('.detailed').show()
        $(".collapsed_"+id).hide()
        $(".detailed_"+id).show()
    })
    $('.view_less_link').click(function(){
        var id = $(this).data('id')
        // alert(1)
        // console.log($(this).next().next());
        // $(this).parent().find('.collapsed').hide()
        // $(this).parent()).hide();
        // $(this).next().find('.detailed').show()
        $(".collapsed_"+id).show()
        $(".detailed_"+id).hide()
    })
    $('.view_more_gameplan').click(function(){
        var id = $(this).data('id')
        // alert(1)
        // console.log($(this).next().next());
        // $(this).parent().find('.collapsed').hide()
        // $(this).parent()).hide();
        // $(this).next().find('.detailed').show()
        $(".collapsed_"+id).hide()
        $(".detailed_"+id).show()
    })
    $('.view_less_gameplan').click(function(){
        var id = $(this).data('id')
        // alert(1)
        // console.log($(this).next().next());
        // $(this).parent().find('.collapsed').hide()
        // $(this).parent()).hide();
        // $(this).next().find('.detailed').show()
        $(".collapsed_"+id).show()
        $(".detailed_"+id).hide()
    })
    //browse input trigger hidden
    $("#upload-resume-btn").click(function(){
       $("#upload-resume")[0].click();
    });

    $('.view_more_content_link').click(function(){
        // var id = $(this).data('id')
        // alert(1)
        // console.log($(this).next().next());
        // $(this).parent().find('.collapsed').hide()
        // $(this).parent()).hide();
        // $(this).next().find('.detailed').show()
        $(".collapsed").hide()
        $(".detailed").show()
    })
    $('.view_less_content_link').click(function(){
        // var id = $(this).data('id')
        // alert(1)
        // console.log($(this).next().next());
        // $(this).parent().find('.collapsed').hide()
        // $(this).parent()).hide();
        // $(this).next().find('.detailed').show()
        $(".collapsed").show()
        $(".detailed").hide()
    })

    //Home Screen Comment Section
    //Placeholder image replace to browse
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).next('#imgsrc').attr('src', e.target.result);
                //remove icon appear
                $('.remove-icon').css("display", "block");
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    //browse input get url
    $("#inputfile").change(function () {
        readURL(this);
    });

    //browse input trigger hidden
    // $("#uploadimg").click(function(){
    //    $("#inputfile").click();
    //     $('.post-box').css("display", "block");
    //     $('.add-article label').css("display", "none");
    // });




    //Add/View Comment Home Screen
    home_screen_hide_show()
    //  Show/Hide home post comments
    show_hide_comments()
    // Like/Dislike post Home feed
    like_posts()
    // BookMark articles
    bookmark_articles()
    // Delete comment on posts
    delete_home_comment()
    // List bookmarked articles
    list_bookmarked_articles()
    // Remove Empty
    remove_empty_checklist()
    // Delete Checklists
    // delete_checklist()


    // // Like Dislike Post
    // $('.mark_favorite').click(function(){
    //     var id = $(this).data('id')
    //     console.log($('.icon_'+id).hasClass('active'));
    //     var fav = false
    //     if($('.icon_'+id).hasClass('active') == false){
    //         console.log(1);
    //         fav = true
    //         $('.icon_'+id).addClass('active')
    //     }else{
    //         console.log(2);
    //         fav = false
    //         $('.icon_'+id).removeClass('active')
    //     }
    //     console.log(fav);
    //     $('.loader').show()
    //     $.ajax({
    //         url: '/favorite_post',
    //         // url: '_web/favorite_post',
    //         // url: '../favorite_post',
    //         data: { user_post_id: id, is_favorite: fav },
    //         method: 'post',
    //         dataType: 'script',
    //         success: function(response){
    //             $('.loader').hide()
    //         },
    //         failure: function(response){
    //             $('.loader').hide()
    //         },
    //     })
    //     $('.loader').hide()
    // })
    //
    //
    // //mobile expand filter
    // $('#mexpand').click(function(){
    //     $('.mobile-expand').toggleClass("show");
    //     $('.expand').toggleClass("collaps");
    // });
    //
    // //expand filter
    // $('#expand').click(function(){
    //     $('.indsec').toggleClass("show");
    //     $('.expand').toggleClass("collaps");
    //     // $('.share-article').attr('disabled', true)
    // });


    validate_email()


    // Initialize date picker & timepicker & validate date
    var date = new Date();
    date.setDate(date.getDate());

    $('.appt_date').datepicker({
        format: {
            toDisplay: function (date, format, language) {
                var d = new Date(date);
                d.setDate(d.getDate() - 7);
                return d.toISOString();
            },
            toValue: function (date, format, language) {
                var d = new Date(date);
                d.setDate(d.getDate() + 7);
                return new Date(d);
            }

        },
        startDate: date,
    });
    // $('.timepicker').timepicker({ defaultTime: false, placeholder: 'Select'});

    // if($('send_feedback[rate]').)

    $('.social-share-button').find('a').each(function(e){
        if($(this).data('site') == 'twitter'){
            $(this).html("<span class='social-links'>Twitter<span>")
        }else if ($(this).data('site') == 'facebook') {
            $(this).html("<span class='social-links'>Facebook</span>")
        }else if ($(this).data('site') == 'linkedin') {
            $(this).html("<span class='social-links'>LinkedIn</span>")
        }else if ($(this).data('site') == 'email') {
            $(this).html("<span class='social-links'>Email</span>")
            // console.log($(this).parent());
            // $(this).parent().attr('data-target', '_blank')
        }
        // $(this).after('<br />')
        // $(this).after('&nbsp;')
    })

    $('#job_region_US').prop('checked', true)
    $('.company_name').hide()
    $('.country').hide()

    // if($('input[name=checklist]').hasClass('active')){
    //     // alert(2)
    //     var id = $(this).data('id')
    //     console.log($(this));
    //     $(this).setAttribute('checked', 'checked')
    //     // $(this).parent().addClass('selected')
    //     // $('.ccheckbox input:checked ~ .checkmark').css('background-color', '#ff7e7e')
    //     // $('.checklist_'+id).attr('checked', 'checked')
    // }

    $('input[name=checklist]').each(function(){
        if($(this).hasClass('active')){
            $(this).attr('checked', 'checked')
        }
    })

    $('.filter-sec .select2').on('change', function(){
        $('.search_companies .btn-submit').addClass('btn-theme')
        $('.search_companies .btn-submit').css('height', '38px')
        $('.search_companies .btn-submit').removeClass('btn-submit')
    })



    custom_checkbox_class('search[industry_ids]');
    custom_checkbox_class('search[company_ids]');

    custom_checkbox_class('checklist');

    visa_insight_filter_type_change()

    get_meeting_info();

    gameplan_next_questions();

    validate_forms();

    search_jobs();

    add_company_as_favorite();

    country_search();
    // add_checklist();
    add_main_checklist();
    add_sub_checklist();
    edit_checklist();
    mark_checklist();
    delete_checklist();
    network_tracker_search();
    resources_search();
    gameplan_prev();
    scroll_pagination_on_home();
    open_jobs_filter();
    search_industry_visa();
    get_country_detail();
    video_screen_toggle_views();
    visa_screen_company_list();
    visa_screen_industry_list();
    clear_all_select2();
    home_screen_hide_on_lost_focus();
    open_jobs_load_more();
    jobs_screen_load_steps();
    next_steps_jobs();
    timepicker_meeting();
    delete_meeting();
    home_search();
    trigger_social_share();
    clear_network_search();

    // video_screen_ordering();
    // home_filter_bookmarked_articles()
    // company_details()

})
// $('.view_more_link').click(function(){
//     // alert(1)
//     $(this).parent().find('.collapsed').hide()
//     $(this).next().find('.detailed').show()
//     // $('.collapsed').hide()
//     // $('.detailed').show()
// })

//comments loop
$('.show-comment').on('click', function(){
    $('#post-1-comment').toggleClass("show");
});

$('#show-comment2').click(function(){
    $('#post-2-comment').toggleClass("show");
});


function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(input).next('#imgsrc').attr('src', e.target.result);
            //remove icon appear
            $('.remove-icon').css("display", "block");
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function validate_email(){
    $('#signupnext').on('click', function(){
        $('.loader').show()
        $.ajax({
            // url: '/verify_school_user',
            url: '/verify_school_user',
            // url: '_web/verify_school_user',
            data: { email: $('#user_email').val() },
            method: 'post',
            dataType: 'json',
            success: function(response){
                $('.loader').hide()
                if(response['success'] == true){
                    // toastr.success(response['message'])
                    $('#user_school_id').val(response['data']['id'])
                    $('#signup2').show()
                    $('#signup').hide()
                }
                else{
                    toastr.error(response['message'])
                    $('#signup2').hide()
                    $('#signup').show()
                }
            },
            failure: function(response){
                $('.loader').hide()
            },
        })
        $('.loader').hide()
    })
}
function load_home_content(){
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).next('#imgsrc').attr('src', e.target.result);
                //remove icon appear
                $('.remove-icon').css("display", "block");
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    //browse input get url
    $("#inputfile").change(function () {
        readURL(this);
    });

    //browse input trigger hidden
    $("#uploadimg").click(function(){
       $("#inputfile").click();
        $('.post-box').css("display", "block");
        $('.add-article label').css("display", "none");
        $('.post-footer .btn-default').css('display', 'block')
    });


    //remove icon event
    $('.remove-icon').click(function(){
        $('#imgsrc').removeAttr('src');
        $('.remove-icon').css("display", "none");
    });

    //add post box appear
    $('.add-article label').click(function(){
        // alert(1)
        $('.post-box').css("display", "block");
        $('#user_post_title').focus();
        $('.add-article label').css("display", "none");
        $('.post-footer .btn-default').css('display', 'block')
    });

    //remove icon event
    $('.remove-icon').click(function(){
        $('#imgsrc').removeAttr('src');
        $('.remove-icon').css("display", "none");
    });

    //comments loop
    $('#show-comment').click(function(){
        $('#post-1-comment').toggleClass("show");
    });

    $('#show-comment2').click(function(){
        $('#post-2-comment').toggleClass("show");
    });

    //mobile expand filter
    $('#mexpand').click(function(){
        $('.mobile-expand').toggleClass("show");
        $('.expand').toggleClass("collaps");
    });

    $('.post-footer .btn-submit').click(function(){
        $('.post-box').css('display', 'none')
        $('.add-article label').css('display', 'block')
        $('#user_post_title').val('')
        $('#user_post_share_link').val('')
        $('#user_post_description').val('')
        $('#user_post_image_url').val('')
        $('#imgsrc').val('')
        $('#imgsrc').attr('src', '')
        $('.share-article').attr('disabled', 'disabled')
        $('.preview-card').css('display', 'none');
    })

    //expand filter
    // $('#expand').click(function(){
    //     $('.indsec').toggleClass("show");
    //     $('.expand').toggleClass("collaps");
    //     // $('.share-article').attr('disabled', true)
    // });

    $('.expand_industry').click(function(){
        $('.indsec').toggleClass("show");
        $('.expand').toggleClass("collaps");
        // $('.share-article').attr('disabled', true)
    });

    $('.expand_company').click(function(){
        $('.indcom').toggleClass("show");
        $('.expand').toggleClass("collaps");
        // $('.share-article').attr('disabled', true)
    });

}
function fill_city(){
    $('.select_state_main').on('change', function(e){
        // e.stopImmediatePropagation()
        var ids = $(this).val();
        $('#state_select_val').val(ids)
        var tab_type = $('.type_select').val()
        // $('.loader').show()
        $( ".select_state_main" ).select2({
            placeholder: 'Select State',
            ajax: {
                // url: '/companies/state_select2?tab_type='+$('#type_select_val').val(),
               url: '/companies/state_select2?tab_type='+$('#type_select_val').val(),
               dataType: 'json',
               data: function(term){
                   return{
                       term: term
                   };
               },
               processResults: function (data) {
                   return {
                       results: $.map(data, function (item) {
                           return {
                               text: item.state_name.toProperCase(),
                               // slug: item.slug,
                               id: item.id
                           }
                       })
                   };
               }
            }
        })
        // $('.city_select').select2().trigger('change')

        $( ".select_city" ).val("").select2({
            placeholder: 'Select City',
            ajax: {
                // url: '/companies/city_select2?tab_type='+$('#type_select_val').val()+"&state_ids="+$(this).val(),
                url: '/companies/city_select2?tab_type='+$('#type_select_val').val()+"&state_ids="+$(this).val(),
                dataType: 'json',
                data: function(term){
                    return{
                        term: term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.city_name.toProperCase(),
                                // slug: item.slug,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        // $('.loader').hide()
    })
    $('.select_job_title').on('change', function(e){
        // e.stopImmediatePropagation()
        var ids = $(this).val();
        // $('#state_select_val').val(ids)
        var tab_type = $('.type_select').val()
        // $('.loader').show()
        $( ".select_job_title" ).select2({
            placeholder: 'Select Job Title',
            ajax: {
                // url: '/companies/job_title_select2?tab_type='+$('#type_select_val').val(),
               url: '/companies/job_title_select2?tab_type='+$('#type_select_val').val(),
               dataType: 'json',
               data: function(term){
                   return{
                       term: term
                   };
               },
               processResults: function (data) {
                   return {
                       results: $.map(data, function (item) {
                           return {
                               text: item.job_title_name.toProperCase(),
                               // slug: item.slug,
                               id: item.id
                           }
                       })
                   };
               }
            }
        })
    })

}
function update_company_industry(){
    /********** Commented on 25 Sep 2018 *********/
    // $('input[type=radio][name=areatype]').change(function() {
    //     if($(this).val() == 'company'){
    //         $('.company_list').show()
    //         $('.industry_list').hide()
    //         $('.company_list').removeClass('hide')
    //     }else{
    //         $('.industry_list').show()
    //         $('.company_list').hide()
    //     }
    // })
    /********** End Comment *********/

    $('.areatype_select').change(function() {
        console.log($(this).val());
        if($(this).val() == 'company'){
            $('.company_list').show()
            $('.industry_list').hide()
            $('.company_list').removeClass('hide')
        }else{
            $('.industry_list').show()
            $('.company_list').hide()
        }
    })

    // // $('input[type=radio][name=areatype]').change(function() {
    // //     if($(this).val() == 'company'){
    // //         $('.company').show();
    // //         // $('.list_company').show();
    // //         $('.list_industry').removeClass('show_list')
    // //         $('.list_company').removeClass('hide_list')
    // //         $('.list_company').addClass('show_list')
    // //         $('.industry').hide();
    // //         // $('.list_industry').hide();
    // //         $('.list_industry').addClass('hide_list')
    // //         $('.expand_company').click(function(){
    // //             alert(1)
    // //             $('.comsec').toggleClass("show");
    // //             $('.expand_company').toggleClass("collaps");
    // //             // $('.share-article').attr('disabled', true)
    // //         });
    // //     }
    // //     else{
    // //         $('.industry').show();
    // //         // $('.list_industry').show();
    // //         $('.list_industry').removeClass('hide_list')
    // //         $('.list_company').removeClass('show_list')
    // //         $('.list_industry').addClass('show_list')
    // //         $('.company').hide();
    // //         // $('.list_company').hide();
    // //         $('.list_company').addClass('hide_list')
    // //         $('.expand_industry').click(function(){
    // //             alert(2)
    // //             $('.indsec').toggleClass("show");
    // //             $('.expand_industry').toggleClass("collaps");
    // //             // $('.share-article').attr('disabled', true)
    // //         });
    // //     }
    // //     $('#expand_industry').click(function(){
    // //         $('.indsec').toggleClass("show");
    // //         $('.expand_industry').toggleClass("collaps");
    // //         // $('.share-article').attr('disabled', true)
    // //     });
    // //
    // //     $('#expand_company').click(function(){
    // //         alert(1)
    // //         $('.comsec').toggleClass("show");
    // //         $('.expand_company').toggleClass("collaps");
    // //         // $('.share-article').attr('disabled', true)
    // //     });
    // });
    // $('#expand_industry').click(function(){
    //     $('.indsec').toggleClass("show");
    //     $('.expand').toggleClass("collaps");
    //     // $('.share-article').attr('disabled', true)
    // });
    //
    // $('#expand_company').click(function(){
    //     alert(4)
    //     $('.comsec').toggleClass("show");
    //     $('.expand_company').toggleClass("collaps");
    //     // $('.share-article').attr('disabled', true)
    // });
}
$('#expand_company').unbind().on('click', function(){
    // $('.share-article').attr('disabled', true)
});
function video_screen_sidebar(){
    $('.category_list').on('click', function(e){
        var order_by = $('#list_order_by').attr('value')
        console.log($(this).children('a').text());
        $('#category_val').val($(this).children('a').text())
        // $('#search_key').val(name)
        $('.category_list').removeClass('active')
        $(this).addClass('active')
        $('.loader').show()
        $.ajax({
            // url: '/videos/get_video_list',
            url: '/videos/get_video_list',
            // url: '_web/videos/get_video_list',
            method: 'get',
            dataType: 'script',
            data: { category_id: $(this).data('id'), order_by: order_by },
            success: function(response){
                // $('.loader').hide()
                resources_search();
            },
            error: function(response){
                // $('.loader').hide()
            }
        })
        // $('.loader').hide()
        // e.preventDefault();
    })
}
function get_meeting_info(){
    $('.meeting_info').click(function(){
        var id = $(this).data('id')
        $('.loader').show()
        $.ajax({
            // url: '/network_trackers/'+id,
            url: '/network_trackers/'+id,
            // url: '_web/network_trackers/'+id,
            method: 'get',
            dataType: 'script',
            success: function(data){
                $('.loader').hide()
            },
            failure: function(data){
                $('.loader').hide()
            }
        })
        $('.loader').hide()
    })
}
function gameplan_next_questions(){
    $(this).click('on')
    $('.btn_next').on('click', function(e){
        $(this).click('on')
        var curr_btn = $(this)
        var curr = $(this).data('id')
        var total = $(this).data('total')
        $('.back-icon').attr('data-id', curr)
        // console.log($('.gameplan_question_'+curr).attr('type'))
        // console.log($('.gameplan_question_'+curr+':checked'));
        var checked = false
        /****************************************************************************************************************************/
        // if(($('.gameplan_question_'+curr).attr('type') == 'checkbox') || ($('.gameplan_question_'+curr).attr('type') == 'radio')){
        //     if($('.gameplan_question_'+curr+':checked').length > 0){
        //         console.log('checked');
        //         checked = true
        //     }else{
        //         console.log('not checked');
        //         checked = false
        //     }
        // }else{
        //     if($('.gameplan_question_'+curr).val() === ""){
        //         console.log('not selected');
        //         checked = false
        //     }else{
        //         console.log('selected');
        //         checked = true
        //     }
        // }
        // if(checked == true){
        //     $('#question_'+curr).hide();
        //     $('#question_'+(curr+1)).show();
        // }else{
        //     $(this).click('off')
        //     toastr.error('Please select an answer to move further')
        // }
        /****************************************************************************************************************************/
        // alert($('.question_type_'+curr).data('type'))
        if($('.question_type_'+curr).data('type') =='drop-down'){
            // alert(1)
            if($('.gameplan_question_'+curr).val() === ""){
                checked = false
            }else{
                checked = true
            }

        }else if (($('.question_type_'+curr).attr('data-type') == 'single-choice') || ($('.question_type_'+curr).attr('data-type') == 'multiple-choice')) {
            // alert(2)
            if($('.gameplan_question_'+curr+':checked').length > 0){
                checked = true
            }else{
                checked = false
            }
        }else if ($('.question_type_'+curr).data('type') == 'selection') {
            if($(".child_question_"+curr).children().find('input[type=radio]:checked').length == $(".child_question_"+curr).data('count')){
                checked = true
            }else{
                checked = false
            }
            // $(".child_question_"+curr).each(function(){
            //     alert(5)
            //     console.log($(this).children().find('input[type=radio]:checked').length)
            //     // $(this).children().find('input[type=radio]').each(function(){
            //     //     if($(this).)
            //     // })
            // })

        }
        else {
        }
        if(checked == true){
            if(curr == total){
                $('#gameplan_questions_form').submit();
            }else{
                $('#question_'+curr).hide();
                $('#question_'+(curr+1)).show();
            }
        }else{
            $(this).click('off')
            toastr.error('Please make a selection to proceed further')
        }
    })
}

function convert_to_24_hour(time){
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if(AMPM == "PM" && hours<12) hours = hours+12;
    if(AMPM == "AM" && hours==12) hours = hours-12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if(hours<10) sHours = "0" + sHours;
    if(minutes<10) sMinutes = "0" + sMinutes;
    // alert(sHours + ":" + sMinutes);
    return (sHours + ":" + sMinutes)
}

function validate_forms(){
    $('#new_send_feedback').validate({
        errorElement: 'div',
    });

    $('#new_contact_us').validate({
        errorElement: 'div',
    });

    // // $.validator.addMethod("timeValidation", function (value, element, options)
    // // {
    // //     var targetEl = $
    // // }
    $.validator.addMethod("validEndTime", function (value, element, options)
    {
        //we need the validation error to appear on the correct element
        var start = convert_to_24_hour($('.start_time').val())
        value = convert_to_24_hour(value)
        console.log("========================");
        console.log(element);
        console.log(value);
        console.log(start);
        console.log("========================");
        if(start == ""){

        }else{
            if(value < start ){
                // element.addClass('error')
            }else{
                // element.removeClass('error')
            }

            return !(value < start );
        }
    }, "End time should be greater than start time");

    // $.validator.addMethod("validStartTime", function (value, element, options)
    // {
    //     //we need the validation error to appear on the correct element
    //     var end = convert_to_24_hour($('.end_time').val())
    //     value = convert_to_24_hour(value)
    //     console.log("========================");
    //     console.log(element);
    //     console.log(value);
    //     console.log(end);
    //     console.log("========================");
    //     if(end == ""){
    //
    //     }
    //     else{
    //         if(value > end ){
    //             // element.addClass('error')
    //         }else{
    //             // element.removeClass('error')
    //         }
    //
    //         return !(value > end );
    //     }
    // }, "Start time should be earlier than end time");


    $('.network_tracker_form').validate({
        errorElement: 'div',
        onkeyup: false,
        onchange: function( element, event ) {
            this.element( element );
        }
        // rules: {
        //     "network_tracker[end_time]": { required: "required" },
        //     "network_tracker[company_name]": { required: "required" },
        //     "network_tracker[appointment_date]": { required: "required" },
        //     "network_tracker[start_time]": { required: "required" },
        // }
    })
}
function home_screen_hide_show() {


    //add post box appear
    $('.add-article label').on('click', function(){
        // alert(1)
        $('.post-box').css("display", "block");
        $('.add-article label').css("display", "none");
        $('.share-article').attr('disabled', true)
    });

    if(($('#user_post_title').length > 0) || ($('#imgsrc').attr('src') != '.')){
        $('.share-article').attr('disabled', false)
    }else{
        $('.share-article').attr('disabled', true)
    }

    $('#user_post_title').on('focusout', function(){
        if(($('#user_post_title').length > 0)||($('#imgsrc').attr('src') != '.')){
            $('.share-article').attr('disabled', false)
        }else{
            $('.share-article').attr('disabled', true)
        }
    });

    $('#user_post_title').on('onpaste', function(){
        // alert(1)
    });

    $('#uploadimg').on('click',  function(){
        // alert(1)
        if(($('#user_post_title').length > 0)||($('#imgsrc').attr('src') != '.')){
            $('.share-article').attr('disabled', false)
        }else{
            $('.share-article').attr('disabled', true)
        }
    })

    $('.remove-icon').on('click', function(){
        // alert(1)
        if(($('#user_post_title').length > 1)){
            $('.share-article').attr('disabled', false)
        }else{
            $('.share-article').attr('disabled', true)
        }
    })
}
function search_jobs(){
    $("input[type=radio][name=job_region]").change(function(){
        if($(this).val() == 'International'){
            $('#job_search_companies').val('')
            $('#job_search_company_name').val('')
            $('.company_name').show()
            $('.company_list').hide()
            $('.country').show()
            $('.companies').hide()
            $('#job_search_search').attr('required', true)
        }else{
            $('.company_name').hide()
            $('.company_list').show()
            $('.country').hide()
            $('.companies').show()
            $('#company_name').val('')
            $('#job_search_country').val('')
            $('#job_search_search').attr('required', false)
        }
    })
    $('#job_search_companies').change(function(){
        if($(this).val() == 'top'){
            $('.company_list').show()
            $('#company_name').val('')
            $('.company_name').hide()
        }else{
            $('#job_search_company_name').val('')
            $('.company_name').show()
            $('.company_list').hide()
        }
    })
}
function custom_checkbox_class(checkboxName){
    var checkBox = $('input[name="'+ checkboxName +'"]');
    $(checkBox).each(function(){
        // $(this).wrap( "<span class='custom-checkbox'></span>" );
        if($(this).is(':checked')){
            $(this).parent().addClass("selected");
        }
    });
    $(checkBox).click(function(){
        $(this).parent().toggleClass("selected");
    });
}
function visa_insight_filter_type_change(){
    $('.year_select').focusout(function(){
        if($(this).val() == ""){
            $(this).val('2017').select2().trigger('change')
        }
    })
    // $("input[type=radio][name=type]").change(function(){
    $('#type_select').change(function(){
        // alert(1)
        // console.log($('.loader'));
        $('.loader').show()
        var type_val = $(this).val()
        $('#type_select_val').val(type_val)
        var stateSelect = $('#search_state_ids');
        $('#favorite_company_type_radio_val').val(type_val)
        $.ajax({
            // url: '/companies/fetch_master_data?type='+type_val,
            url: '/companies/fetch_master_data?type='+type_val,
            // url: '_web/companies/fetch_master_data?type='+type_val,
            method: 'get',
            dataType: 'script',
            success: function(response){
            },
            failure: function(response){
            }
        })
    })
}
function add_company_as_favorite(){
    // $('#fav_company').click(function(){
        $('.chart-cell .fav-icon').on('click', function(e){
            $(this).off('click')
        // alert(1)
        // alert(1)
        e.preventDefault()
        e.stopImmediatePropagation()
            var company_id = $(this).data('company-id')
            var tab_type = $(this).data('tab-type')
            var company_name = $(this).data('company-name')
            var like = 'unlike'
            if($(this).hasClass('active') == true){
                $(this).removeClass('active')
                like = 'unlike'
            }else {
                $(this).addClass('active')
                like = 'like'
            }
            $('.loader').show()
            $.ajax({
                url: '/companies/add_company_as_favorite',
                method: 'post',
                data: { company_id: company_id, company_name: company_name, like: like },
                dataType: 'script',
            })
            $(this).on('click')
            // $('.loader').hide()
        })

        $('.article.cdetails .fav-icon').on('click', function(e){
            $(this).off('click')
        // alert(1)
        // alert(1)
        e.preventDefault()
        e.stopImmediatePropagation()
            var company_id = $(this).data('company-id')
            var tab_type = $(this).data('tab-type')
            var company_name = $(this).data('company-name')
            var like = 'unlike'
            if($(this).hasClass('active') == true){
                $(this).removeClass('active')
                like = 'unlike'
            }else {
                $(this).addClass('active')
                like = 'like'
            }
            $('.loader').show()
            $.ajax({
                url: '/companies/add_company_as_favorite',
                method: 'post',
                data: { company_id: company_id, company_name: company_name, like: like },
                dataType: 'script',
            })
            $(this).on('click')
            // $('.loader').hide()
        })

}
function country_search(){
    $('.country_search').on('keyup', function(){
        var country = $(this).val()
        $('.loader').show()
        $.ajax({
            // url: '/country_insights/search?search='+country,
            url: '/country_insights/search?search='+country,
            // url: '_web/country_insights/search?search='+country,
            method: 'get',
            dataType: 'script',
            success: function(response){
                $('.loader').hide()
            },
            failure: function(response){
                $('.loader').hide()
            }
        })
        $('.loader').show()
    })
}
// function add_checklist(){
//     $('.addtask').click(function(e){
//         // alert(1)
//         // e.stopImmediatePropagation()
//         // $(this).off('click')
//         var task_type = $(this).data('check-type')
//         var id = $(this).data('id')
//         // $('#task_type').val(task_type)
//         // $('#type_id').val(id)
//         console.log($(this).next())
//
//         //var html_element = "<li class='sub_list sub_list_"+id+"' data-id="+id+"><div class='cell'><div class='row cat_row_"+id+"'><div class='col-xs-10'><label class='ccheckbox'><input type='checkbox' name='checklist' class='active'><span class='checkmark checklist_checkmark'></span></label><input type='text' name='checklist_label' class='checklist_label'></div><div class='col-xs-2'><a class='remove_empty' data-id="+id+" data-type='main'><i class='fa fa-remove'></i></a></div></div></div></li>"
//         var html_element = "<li class='sub_list sub_list_"+id+"' data-id="+id+"><div class='cell'><div class='row cat_row_"+id+"'><div class='col-xs-10'><label class='ccheckbox'><input type='checkbox' name='checklist' class='active'><span class='checkmark checklist_checkmark'></span></label><input type='text' name='checklist_label' class='checklist_label'></div><div class='col-xs-2 text-right'><a class='remove_empty delete-icon' data-id="+id+" data-type='main'>+</a></div></div></div></li>"
//         $(this).next().prepend(html_element)
//
//         // console.log($(this).parents('li').children('ul').find('li input.checklist_label'));
//         var new_ele = $(this).next().find('li input.checklist_label')
//         new_ele.on('focusout', function(){
//             console.log('out')
//             console.log($(this).val());
//             if($(this).val() != ''){
//                 $.ajax({
//                     url: '/checklists',
//                     // url: '/checklists',
//                     method: 'post',
//                     data: { task_type: task_type, type_id: id, checklist: { title: $(this).val() } },
//                     dataType: 'script',
//                     // success: function(response){
//                     //     toastr.success(response['message'])
//                     //     window.location.reload(true)
//                     // },
//                     // failure: function(response){
//                     //     toastr.error(response['message'])
//                     //     window.location.reload(true)
//                     // }
//
//                 })
//             }
//         })
//         // $(this).on('click')
//         remove_empty_checklist()
//     })
//     $('.add_sub_task').unbind().click(function(e){
//         // e.stopImmediatePropagation()
//         // $(this).off('click')
//         // e.preventDefault()
//         // $(this).off('click')
//         var task_type = $(this).data('check-type')
//         var id = $(this).data('id')
//         // $('#task_type').val(task_type)
//         // $('#type_id').val(id)
//         if($(this).parents('li').children('ul').length > 0){
//             var html_element = "<li  class='subchecklist'><div class='cell'><div class='row cat_row_"+id+"'><div class='col-xs-10'><label class='ccheckbox'><input type='checkbox' name='checklist' class='active'><span class='checkmark checklist_checkmark'></span></label><input type='text' name='checklist_label' class='checklist_label'></div><div class='col-xs-2 text-right'><a class='remove_empty delete-icon' data-id="+id+" data-type='sub'>+</a></div></div></div></li>"
//             $(this).parents('li').children('ul').append(html_element)
//         }else{
//             var html_element = "<ul><li class='subchecklist'><div class='cell'><div class='row cat_row_"+id+"'><div class='col-xs-10'><label class='ccheckbox'><input type='checkbox' name='checklist' class='active'><span class='checkmark checklist_checkmark'></span></label><input type='text' name='checklist_label' class='checklist_label'></div><div class='col-xs-2 text-right'><a class='remove_empty delete-icon' data-id="+id+" data-type='sub'>+</a></div></div></div></li></ul>"
//             console.log(html_element)
//             $(this).parents('li').append(html_element)
//         }
//         // console.log($(this).parents('li').children('ul').find('li input.checklist_label'));
//         var new_ele = $(this).parents('li').children('ul').find('li input.checklist_label')
//         new_ele.on('focusout', function(){
//             // console.log('out')
//             if($(this).val() != ''){
//                 $.ajax({
//                     url: '/checklists',
//                     // url: '/checklists',
//                     method: 'post',
//                     data: { task_type: task_type, type_id: id, checklist: { title: $(this).val() } },
//                     dataType: 'script',
//                     // success: function(response){
//                     //     toastr.success(response['message'])
//                     //     window.location.reload(true)
//                     // },
//                     // failure: function(response){
//                     //     toastr.error(response['message'])
//                     //     window.location.reload(true)
//                     // }
//
//                 })
//             }
//         })
//         remove_empty_checklist()
//         // $(this).click('on')
//         // $(this).on('click')
//         // console.log($('.checklist_label'));
//     })
//     $('.edit_task').click(function(){
//         var task_type = $(this).data('check-type')
//         var id = $(this).data('id')
//         var submit_type = 'edit'
//         var title = $(this).data('value')
//         $('#edit_task_type').val(task_type)
//         $('#edit_type_id').val(id)
//         var is_checked = false
//         if(task_type=='main'){
//             console.log(1);
//             console.log($('.parent_'+id).is(':checked'));
//             if($('.parent_'+id).is(':checked')){
//                 is_checked = true
//             }
//         }else{
//             var parent_id = $(this).closest('li.sub_list').data('id')
//             console.log(2);
//             console.log($(this).closest('li.sub_list').data('id'));
//             console.log($('.child_'+parent_id).is(':checked'));
//             if($('.child_'+parent_id).is(':checked')){
//                 is_checked = true
//             }
//         }
//
//         // if(task_type == 'sub'){
//
//             var html_element = "<div class='col-xs-10'><label class='ccheckbox'><input type='checkbox' name='checklist' class='active'><span class='checkmark checklist_checkmark'></span></label><input type='text' name='checklist_label' class='checklist_label' value='"+title+"'></div><div class='col-xs-2 text-right'><a class='remove_empty_edit delete-icon' data-id="+id+" data-type="+task_type+" data-content="+title+">+</a></div>"
//
//             $('.cat_row_'+id).empty().append(html_element)
//             // console.log($('.cat_row_'+id).find('input.checklist_label'));
//             var new_ele = $('.cat_row_'+id).find('input.checklist_label')
//             new_ele.on('focusout', function(){
//                 // console.log('out')
//
//                 $.ajax({
//                     url: '/checklists/edit_checklist',
//                     // url: '/checklists/edit_checklist',
//                     method: 'post',
//                     data: { task_type: task_type, type_id: id, checklist: { title: $(this).val() } },
//                     dataType: 'script',
//                     // success: function(response){
//                     //     toastr.success(response['message'])
//                     //     window.location.reload(true)
//                     // },
//                     // failure: function(response){
//                     //     toastr.error(response['message'])
//                     //     window.location.reload(true)
//                     // }
//
//                 })
//             })
//         // }
//         cancel_checklist_edit(is_checked)
//         // $('#edit_submit_type').val(submit_type)
//         // $('#title').val(title)
//         // $('#edit_checklist_title').val(title)
//     })
//     // $('.add_checklist').click(function(){
//     //
//     // })
// }
function mark_checklist(){
    // $('.checklist_checkmark').click(function(){
    //     var id = $(this).data('id')
    //     console.log($('.checklist_'+id+':checked').length)
    // })
    $('input[type=checkbox][name=checklist]').change(function(e){
        e.stopImmediatePropagation()
        $(this).off('click')
        var id = $(this).data('id')
        var check_type = $(this).data('check-type')
        var check = false
        var active_tab = $('.nav-tabs li.active a')
        if($(this).is(':checked')){
            // $(this).addClass('active')
            $(this).parent().addClass('selected')
            check = true
        }else {
            // $(this).removeClass('active')
            $(this).parent().removeClass('selected')
            check = false
        }
        $('.loader').show()
        console.log(active_tab);
        // alert(active_tab)
        $.ajax({
            // url: '/checklists/mark_checklist',
            url: '/checklists/mark_checklist',
            data: {
                type_id: id,
                check_type: check_type,
                is_mark: check
            },
            dataType: 'script',
            method: 'post',
            success: function(response){
                $('.loader').hide()
                // window.location.reload()
                active_tab.trigger('click')
            },
            failure: function(response){
                $('.loader').hide()
            }
        })
        $(this).on('click')
    })

}
function network_tracker_search(){
    $('.network_tracker_search').on('keyup', function(){
        if($(this).val().length >= 3){
            var name = $(this).val()
            $('.loader').show()
            $.ajax({
                // url: '/network_trackers/search?search='+name,
                url: '/network_trackers/search?search='+name,
                // url: '_web/country_insights/search?search='+country,
                method: 'get',
                dataType: 'script',
                // success: function(response){
                //     console.log('success');
                // },
                // failure: function(response){
                //     console.log('failure');
                // }
            })
        }
    })
    $('.loader').hide()
}
function resources_search(){
    // $('.resources_search').on('focusout', function(){
    //     var category = $(this).data('category-name')
    //     var name = $(this).val()
    //     $('#category_val').val(category)
    //     $('#search_key').val(name)
    //     $.ajax({
    //         // url: '/videos/search?search='+name+'&category='+category+'&order_by='+$('#list_order_by').attr('val'),
    //         url: '/videos/search?search='+name+'&category='+category+'&order_by='+$('#list_order_by').attr('val'),
    //         // url: '_web/country_insights/search?search='+country,
    //         method: 'get',
    //         dataType: 'script',
    //         // success: function(response){
    //         //     console.log('success');
    //         // },
    //         // failure: function(response){
    //         //     console.log('failure');
    //         // }
    //     })
    // })
    $('.resources_search').on('keypress', function(e){
        if(e.which == 13){
            var category = $(this).data('category-name')
            var name = $(this).val()
            console.log("====================");
            console.log(category);
            console.log("====================");
            $('#category_val').val(category)
            $('#search_key').val(name)
            $.ajax({
                // url: '/videos/search?search='+name+'&category='+category+'&order_by='+$('#list_order_by').attr('val'),
                url: "/videos/search?search="+name+"&category='"+category+"'&order_by="+$('#list_order_by').attr('val'),
                // url: '_web/country_insights/search?search='+country,
                method: 'get',
                dataType: 'script',
                // success: function(response){
                //     console.log('success');
                // },
                // failure: function(response){
                //     console.log('failure');
                // }
            })
        }
    })
}
function gameplan_prev(){
    $('.back-icon').on('click', function(){
        // $(this).off('click')
        var id = parseInt($(this).attr('data-id'))
        // console.log(1);
        // console.log(id);
        if(id == -1){
            // console.log(2);
            window.location = '/gameplan'
        }else{
            $('#question_'+id).show();
            $('#question_'+(id+1)).hide();
            $('#question_'+id+' .btn_next').attr('data-id', id)
            $('.back-icon').attr('data-id', id-1 )
            // id = null
            // gameplan_prev()
        }
        // $(this).on('click')
    })
}
function scroll_pagination_on_home(){
    if ($('#infinite-scrolling').size() > 0) {
        var flg = 0
        $(window).on('scroll', function(e) {
            var more_posts_url;
            more_posts_url = $('.pagination a.next_page').attr('href');
            // e.stopImmediatePropagation()
            if (more_posts_url && $(window).scrollTop() > $(document).height() - $(window).height() - 60) {
                // $('.pagination').html('<img src="/assets/loader.gif" alt="Loading..." title="Loading..." />');
                if(flg == 0){
                    var search = $('.home_article_search').val();
                    $.getScript(more_posts_url, {search: search});
                    flg = 1
                }
            }
            // e.stopImmediatePropagation()
            return;
        });

        return;
    }
}
function open_jobs_filter(){
    // $('input[type=radio][name=sort_by]').on('change', function(){
    //     console.log($(this).val());
    // })
    $('.open_jobs_filter_apply').click(function(){
        var sort = $('input[type=radio][name=sort_by]:checked').val()
        var job_type = $('input[type=radio][name=open_job_type]:checked').val()
        var city = $('#open_jobs_filter_city').val()
        var company_name = $('#open_jobs_filter_company').val()
        var keyword = $('#open_jobs_filter_search_keyword').val()
        // var page = parseInt($('#page_number').val())

        $.ajax({
            // url: '/jobs/get_open_jobs?company_name='+company_name+'&sort='+sort+'&job_type='+job_type+'&city='+city+'&keyword='+keyword+'&page=1',
            url: '/jobs/get_open_jobs?company_name='+company_name+'&sort='+sort+'&job_type='+job_type+'&city='+city+'&keyword='+keyword+'&page=1',
            method: 'get',
            dataType: 'script',
        })
        // console.log(sort);
    })
    $('input[type=radio][name=sort_by]').on('change', function(e){
        e.stopImmediatePropagation()
        // console.log(1);
        var sort = $('input[type=radio][name=sort_by]:checked').val()
        var job_type = $('input[type=radio][name=open_job_type]:checked').val()
        var city = $('#open_jobs_filter_city').val()
        var company_name = $('#open_jobs_filter_company').val()
        var keyword = $('#open_jobs_filter_search_keyword').val()
        $('#page_number').val(1)
        var page = parseInt($('#page_number').val())

        $.ajax({
            // url: '/jobs/get_open_jobs?company_name='+company_name+'&sort='+sort+'&job_type='+job_type+'&city='+city+'&keyword='+keyword+'&page='+page,
            url: '/jobs/get_open_jobs?company_name='+company_name+'&sort='+sort+'&job_type='+job_type+'&city='+city+'&keyword='+keyword+'&page='+page,
            method: 'get',
            dataType: 'script',
        })
    })
    $('input[type=radio][name=open_job_type]').on('change', function(e){
        e.stopImmediatePropagation()
        // console.log(1);
        var sort = $('input[type=radio][name=sort_by]:checked').val()
        var job_type = $('input[type=radio][name=open_job_type]:checked').val()
        var city = $('#open_jobs_filter_city').val()
        var company_name = $('#open_jobs_filter_company').val()
        var keyword = $('#open_jobs_filter_search_keyword').val()
        $('#page_number').val(1)
        var page = parseInt($('#page_number').val())

        $.ajax({
            // url: '/jobs/get_open_jobs?company_name='+company_name+'&sort='+sort+'&job_type='+job_type+'&city='+city+'&keyword='+keyword+'&page='+page,
            url: '/jobs/get_open_jobs?company_name='+company_name+'&sort='+sort+'&job_type='+job_type+'&city='+city+'&keyword='+keyword+'&page='+page,
            method: 'get',
            dataType: 'script',
        })
    })
}
function search_industry_visa(){
    $('input[type=text][name=search_industry]').keyup(function(){
        var search_text = $(this).val()
        $('.loader').show();
        $.ajax({
            // url: '/companies/search_industry?search='+search_text,
            url: '/companies/search_industry?search='+search_text,
            method: 'get',
            dataType: 'script'
        })
        $('.loader').hide();
    })
    $('input[type=text][name=search_company]').keyup(function(){
        var search_text = $(this).val()
        $('.loader').show();
        $.ajax({
            // url: '/companies/search_company?search='+search_text,
            url: '/companies/search_company?search='+search_text,
            method: 'get',
            dataType: 'script',
            success: function(response){
                $('.loader').hide();
            },
            failure: function(response){
                $('.loader').hide();
            },
        })
        // $('.loader').hide();
    })
}
// function company_details(){
//     $('#company_details').on('click', function(){
//         var id = $(this).data('id')
//         var type = $('input[type=radio][name=type]:checked').val()
//         // console.log(type);
//         $.ajax({
//             url: '/companies/show?id='+id+'&tab_type='+type,
//             method: 'get',
//             dataType: 'html'
//         })
//
//     })
// }
function get_country_detail(){
    $('.country_flag').on('click', function(){
        var id = $(this).data('id')
        $.ajax({
            // url: '/country_insights/'+id,
            url: '/country_insights/'+id,
            method: 'get',
            dataType: 'script'
        })
    })
}
function delete_home_comment(){
    $('.delete-comment').on('click', function(e){
        e.stopImmediatePropagation()
        var comment_id = $(this).data('comment-id')
        $.ajax({
            // url: '/delete_comment?comment_id='+comment_id,
            url: '/delete_comment?comment_id='+comment_id,
            method: 'post',
            dataType: 'script'
        })
    })
}
// function home_filter_bookmarked_articles(){
//     $('#fixed-favorite .title').on('click', function(){
//
//     })
// }
function show_hide_comments(){
    $('.show-comment').click(function(e){
        e.stopImmediatePropagation()
        // $(this).off('click')
        // $('#post-1-comment').toggleClass('active')
        var id = $(this).data('id')
        $('.loader').show()
        $.ajax({
            // url: '/list_post_comments?post_id='+id,
            url: '/list_post_comments?post_id='+id,
            method: 'get',
            dataType: 'script',
            // success: function(response){
            //     $('.loader').hide()
            // },
            // failure: function(response){
            //     $('.loader').hide()
            // },
        })
        // $(this).on('click')
        $('.loader').hide()
    });

    $('.likes-comments').click(function(e){
        // $(this).off('click');
        e.stopImmediatePropagation()
        // $('#post-1-comment').toggleClass('active')
        var id = $(this).data('id')
        $('.loader').show()
        $.ajax({
            // url: '/list_post_comments?post_id='+id,
            url: '/list_post_comments?post_id='+id,
            method: 'get',
            dataType: 'script',
            // success: function(response){
            //     $('.loader').hide()
            // },
            // failure: function(response){
            //     $('.loader').hide()
            // },
        })
        // $(this).on('click')
        $('.loader').hide()
    });
}
function like_posts(){
    // $('.like-icon').click(function(e){
    //     e.stopImmediatePropagation()
    //     // $(this).on('click')
    //     var id = $(this).data('id')
    //     $(this).toggleClass('active')
    //     var like = "0"
    //     if($(this).hasClass('active')){
    //         like = "1"
    //     }else{
    //         like = "0"
    //     }
    //     $('.loader').show()
    //     $.ajax({
    //         // url: '/like_post',
    //         url: '/like_post',
    //         data: { user_post_id: id, like: like },
    //         method: 'post',
    //         dataType: 'script',
    //         success: function(response){
    //             $('.loader').hide()
    //         },
    //         failure: function(response){
    //             $('.loader').hide()
    //         },
    //     })
    //     // $(this).off('click')
    //     $('.loader').hide()
    // })
    $('.like-post').click(function(e){
        e.stopImmediatePropagation()
        // $(this).on('click')
        var id = $(this).data('id')
        $(this).toggleClass('active')
        $(this).children('i').toggleClass('active')
        var like = "0"
        if($(this).hasClass('active')){
            like = "1"
        }else{
            like = "0"
        }
        $('.loader').show()
        $.ajax({
            // url: '/like_post',
            url: '/like_post',
            data: { user_post_id: id, like: like },
            method: 'post',
            dataType: 'script',
            success: function(response){
                $('.loader').hide()
            },
            failure: function(response){
                $('.loader').hide()
            },
        })
        // $(this).off('click')
        $('.loader').hide()
    })
}
function bookmark_articles(){
    // delete_home_comment()
    $('.mark_favorite').click(function(e){
        e.stopImmediatePropagation()
        // $(this).off('click')
        var id = $(this).data('id')
        var fav
        if($('.icon_'+id).hasClass('active') == false){
            fav = true
            $('.icon_'+id).addClass('active')
        }else{
            fav = false
            $('.icon_'+id).removeClass('active')
        }
        $('.loader').show()
        $.ajax({
            // url: '/favorite_post',
            url: '/favorite_post',
            // url: '../favorite_post',
            data: { user_post_id: id, is_favorite: fav },
            method: 'post',
            dataType: 'script',
            success: function(response){
                $('.loader').hide()
            },
            failure: function(response){
                $('.loader').hide()
            },
        })
        // $(this).on('click')
        $('.loader').hide()
    })
}
function list_bookmarked_articles(){
    $('.bookmarked_articles').click(function(e){
        e.stopImmediatePropagation()
        $(this).off('click')
        $.ajax({
            // url: '/bookmarked_articles',
            url: '/bookmarked_articles',
            method: 'post',
            dataType: 'script'
        })
        $(this).on('click')
    })
}
function remove_empty_checklist(){
    $('.remove_empty').on('click', function(){
        var id = $(this).data('id')
        var type = $(this).data('type')
        if(type == 'main'){
            $('.sub_list_'+id).remove()
        }else{
            $(this).parents().find('li.subchecklist').remove()
            // $('.cat_row_'+id).parents('li').remove()
        }
        $('.addtask').on('click')
    })
    // add_main_checklist()
    // add_sub_checklist()
}
function cancel_checklist_edit(is_checked){
    // console.log(1);
    $('.remove_empty_edit').on('click', function(){
        // console.log(2);
        var id = $(this).data('id')
        var type = $(this).data('type')
        var content = $(this).data('content')
        console.log("======typwe============");
        console.log(type);
        // var html_element = "<div class='col-xs-10'><label class='ccheckbox'>"+content+"<input type='checkbox' name='checklist' class='parent_checklist parent_"+id+"' data-id="+id+" data-check-type="+type+"><span class='checkmark checklist_checkmark checklist_"+id+"' data-id="+id+"></span></label></div><div class='col-xs-2'><a href='#' class='add-icon add_sub_task pull-right' data-id="+id+" data-check-type="+type+">+</a><a href='/checklists/delete_checklist?subcategory_id="+id+"&type="+type+"' class='delete-icon pull-right' data-confirm='Are you sure?' data-method='post'>+</a><a href='#' class='edit-icon pull-right edit_task' data-check-type="+type+" data-id="+id+" data-value="+content+"><img class='img-responsive' src='/assets/upload2.png'></a></div>"
        // $('.cat_row_'+id).empty().append(html_element)
        if(type == 'main'){
            if(is_checked){
                var html_element = "<div class='col-xs-10'><label class='ccheckbox selected'>"+content+"<input type='checkbox' name='checklist' class='parent_checklist parent_"+id+" active' data-id="+id+" data-check-type="+type+" checked='checked'><span class='checkmark checklist_checkmark checklist_"+id+"' data-id="+id+"></span></label></div><div class='col-xs-2'><a href='javascript:void(0)' class='delete-icon pull-right' data-id="+id+" data-check-type="+type+" data-method='post'><img class='img-responsive' src='/assets/delete-icon.png'></a><a href='javascript:void(0)' class='edit-icon pull-right edit_task' data-check-type="+type+" data-id="+id+" data-value="+content+"><img class='img-responsive' src='/assets/upload_icon.png'></a><a href='#' class='add-icon add_sub_task pull-right' data-id="+id+" data-check-type="+type+"><img src='/assets/add-icon.png' class='img-responsive'></a></div>"
            }else{
                var html_element = "<div class='col-xs-10'><label class='ccheckbox'>"+content+"<input type='checkbox' name='checklist' class='parent_checklist parent_"+id+"' data-id="+id+" data-check-type="+type+"><span class='checkmark checklist_checkmark checklist_"+id+"' data-id="+id+"></span></label></div><div class='col-xs-2'><a href='javascript:void(0)' class='delete-icon pull-right' data-id="+id+" data-check-type="+type+" data-method='post'><img class='img-responsive' src='/assets/delete-icon.png'></a><a href='javascript:void(0)' class='edit-icon pull-right edit_task' data-check-type="+type+" data-id="+id+" data-value="+content+"><img class='img-responsive' src='/assets/upload_icon.png'></a><a href='javascript:void(0)' class='add-icon add_sub_task pull-right' data-id="+id+" data-check-type="+type+"><img src='/assets/add-icon.png' class='img-responsive'></a></div>"
                }
            $('.cat_row_'+id).empty().append(html_element)
        }else{
            var parent_id = $(this).closest('li.sub_list').data('id')
            if(is_checked){
                var html_element = "<div class='col-xs-10'><label class='ccheckbox selected'>"+content+"<input type='checkbox' name='checklist' class='child_checklist child_"+parent_id+" active' data-id="+id+" data-check-type="+type+" checked='checked'><span class='checkmark checklist_checkmark' data-id="+id+"></span></label></div><div class='col-xs-2'><a href='javascript:void(0)' class='delete-icon pull-right' data-id="+id+" data-check-type="+type+" data-method='post'><img class='img-responsive' src='/assets/delete-icon.png'></a><a href='javascript:void(0)' class='edit-icon pull-right edit_task' data-check-type="+type+" data-id="+id+" data-value="+content+"><img class='img-responsive' src='/assets/upload_icon.png'></a></div>"
            }else{
                var html_element = "<div class='col-xs-10'><label class='ccheckbox'>"+content+"<input type='checkbox' name='checklist' class='child_checklist child_"+parent_id+"' data-id="+id+" data-check-type="+type+"><span class='checkmark checklist_checkmark' data-id="+id+"></span></label></div><div class='col-xs-2'><a href='javascript:void(0)' class='delete-icon pull-right' data-id="+id+" data-check-type="+type+" data-method='post'><img class='img-responsive' src='/assets/delete-icon.png'></a><a href='javascript:void(0)' class='edit-icon pull-right edit_task' data-check-type="+type+" data-id="+id+" data-value="+content+"><img class='img-responsive' src='/assets/upload_icon.png'></a></div>"
                }
            $('.cat_row_'+id).empty().append(html_element)
        }
        // $('.edit_task').on('click')
        edit_checklist()
        add_sub_task()
    })
}
function edit_checklist(){
    $('.edit_task').click(function(){
        var task_type = $(this).data('check-type')
        var id = $(this).data('id')
        var submit_type = 'edit'
        var title = $(this).data('value')
        $('#edit_task_type').val(task_type)
        $('#edit_type_id').val(id)
        var is_checked = false
        if(task_type=='main'){
            console.log(1);
            console.log($('.parent_'+id).is(':checked'));
            if($('.parent_'+id).is(':checked')){
                is_checked = true
            }
        }else{
            var parent_id = $(this).closest('li.sub_list').data('id')
            console.log(2);
            console.log($(this).closest('li.sub_list').data('id'));
            console.log($('.child_'+parent_id).is(':checked'));
            if($('.child_'+parent_id).is(':checked')){
                is_checked = true
            }
        }

        // if(task_type == 'sub'){

        var html_element = "<div class='col-xs-10'><label class='ccheckbox'><input type='checkbox' name='checklist' class='active'><span class='checkmark checklist_checkmark'></span></label><input type='text' name='checklist_label' class='checklist_label' value='"+title+"'></div><div class='col-xs-2'><a class='remove_empty_edit delete-icon pull-right' data-id="+id+" data-type="+task_type+" data-content="+title+"><img class='img-responsive' src='/assets/delete-icon.png'></a></div>"

        $('.cat_row_'+id).empty().append(html_element)
        // console.log($('.cat_row_'+id).find('input.checklist_label'));
        console.log($('input.checklist_label'));
        $('.checklist_label').focus();
        var new_ele = $('.cat_row_'+id).find('input.checklist_label')
        new_ele.on('focusout', function(){
            // console.log('out')

            $.ajax({
                // url: '/checklists/edit_checklist',
                url: '/checklists/edit_checklist',
                method: 'post',
                data: { task_type: task_type, type_id: id, checklist: { title: $(this).val() } },
                dataType: 'script',
                // success: function(response){
                //     toastr.success(response['message'])
                //     window.location.reload(true)
                // },
                // failure: function(response){
                //     toastr.error(response['message'])
                //     window.location.reload(true)
                // }

            })
        })
        new_ele.on('keypress', function(e){
            if(e.which == 13){
                $.ajax({
                    // url: '/checklists/edit_checklist',
                    url: '/checklists/edit_checklist',
                    method: 'post',
                    data: { task_type: task_type, type_id: id, checklist: { title: $(this).val() } },
                    dataType: 'script',
                    // success: function(response){
                    //     toastr.success(response['message'])
                    //     window.location.reload(true)
                    // },
                    // failure: function(response){
                    //     toastr.error(response['message'])
                    //     window.location.reload(true)
                    // }

                })
            }
        })
        // }
        cancel_checklist_edit(is_checked)
        // $('#edit_submit_type').val(submit_type)
        // $('#title').val(title)
        // $('#edit_checklist_title').val(title)
    })
    // $('.add_checklist').click(function(){
    //
    // })}
}
function add_sub_checklist(){
    $('.add_sub_task').click(function(e){
        e.stopImmediatePropagation()
        // $(this).off('click')
        // e.preventDefault()
        // $(this).off('click')
        if($('.checklist_label').length == 0){
            var task_type = $(this).data('check-type')
            var id = $(this).data('id')
            // $('#task_type').val(task_type)
            // $('#type_id').val(id)
            if($(this).parents('li').children('ul').length > 0){
                var html_element = "<li  class='subchecklist'><div class='cell'><div class='row cat_row_"+id+"'><div class='col-xs-10'><label class='ccheckbox'><input type='checkbox' name='checklist' class='active'><span class='checkmark checklist_checkmark'></span></label><input type='text' name='checklist_label' class='checklist_label'></div><div class='col-xs-2'><a class='remove_empty delete-icon pull-right' data-id="+id+" data-type='sub'><img class='img-responsive' src='/assets/delete-icon.png'></a></div></div></div></li>"
                $(this).parents('li').children('ul').append(html_element)
            }else{
                var html_element = "<ul><li class='subchecklist'><div class='cell'><div class='row cat_row_"+id+"'><div class='col-xs-10'><label class='ccheckbox'><input type='checkbox' name='checklist' class='active'><span class='checkmark checklist_checkmark'></span></label><input type='text' name='checklist_label' class='checklist_label'></div><div class='col-xs-2'><a class='remove_empty delete-icon pull-right' data-id="+id+" data-type='sub'><img class='img-responsive' src='/assets/delete-icon.png'></a></div></div></div></li></ul>"
                console.log(html_element)
                $(this).parents('li').append(html_element)
            }
            // console.log($(this).parents('li').children('ul').find('li input.checklist_label'));
            var new_ele = $(this).parents('li').children('ul').find('li input.checklist_label')
            new_ele.on('focusout', function(){
                // console.log('out')
                if($(this).val() != ''){
                    $.ajax({
                        // url: '/checklists',
                        url: '/checklists',
                        method: 'post',
                        data: { task_type: task_type, type_id: id, checklist: { title: $(this).val() } },
                        dataType: 'script',
                        // success: function(response){
                        //     toastr.success(response['message'])
                        //     window.location.reload(true)
                        // },
                        // failure: function(response){
                        //     toastr.error(response['message'])
                        //     window.location.reload(true)
                        // }

                    })
                }
            })
            new_ele.on('keypress', function(e){
                if(e.which == 13){
                    $.ajax({
                        // url: '/checklists',
                        url: '/checklists',
                        method: 'post',
                        data: { task_type: task_type, type_id: id, checklist: { title: $(this).val() } },
                        dataType: 'script',
                        // success: function(response){
                        //     toastr.success(response['message'])
                        //     window.location.reload(true)
                        // },
                        // failure: function(response){
                        //     toastr.error(response['message'])
                        //     window.location.reload(true)
                        // }

                    })
                }
            })
        }
        remove_empty_checklist()
        // $(this).click('on')
        // $(this).on('click')
        // console.log($('.checklist_label'));
    })
}
function add_main_checklist(){
    $('.addtask').click(function(e){
        // alert(1)
        e.stopImmediatePropagation()
        if($('.checklist_label').length == 0){
            var that = $(this)
            // that.off('click')
            var task_type = $(this).data('check-type')
            var id = $(this).data('id')
            // $('#task_type').val(task_type)
            // $('#type_id').val(id)
            console.log($(this).next())

            var html_element = "<li class='sub_list sub_list_"+id+"' data-id="+id+"><div class='cell'><div class='row cat_row_"+id+"'><div class='col-xs-10'><label class='ccheckbox'><input type='checkbox' name='checklist' class='active'><span class='checkmark checklist_checkmark'></span></label><input type='text' name='checklist_label' class='checklist_label'></div><div class='col-xs-2'><a class='remove_empty delete-icon pull-right' href='javascript:void(0)' data-id="+id+" data-type='main'><img  class='img-responsive' src='/assets/delete-icon.png'></a></div></div></div></li>"
            $(this).next().prepend(html_element)

            // console.log($(this).parents('li').children('ul').find('li input.checklist_label'));
            var new_ele = $(this).next().find('li input.checklist_label')
            new_ele.on('focusout', function(){
                console.log('out')
                console.log($(this).val());
                if($(this).val() != ''){
                    $.ajax({
                        // url: '/checklists',
                        url: '/checklists',
                        method: 'post',
                        data: { task_type: task_type, type_id: id, checklist: { title: $(this).val() } },
                        dataType: 'script',
                        success: function(response){
                            // toastr.success(response['message'])
                            // window.location.reload(true)
                            // that.on('click')
                        },
                        // failure: function(response){
                        //     toastr.error(response['message'])
                        //     window.location.reload(true)
                        // }

                    })
                }
            })
            new_ele.on('keypress', function(e){
                if(e.which == 13){
                    $.ajax({
                        // url: '/checklists',
                        url: '/checklists',
                        method: 'post',
                        data: { task_type: task_type, type_id: id, checklist: { title: $(this).val() } },
                        dataType: 'script',
                        // success: function(response){
                        //     toastr.success(response['message'])
                        //     window.location.reload(true)
                        // },
                        // failure: function(response){
                        //     toastr.error(response['message'])
                        //     window.location.reload(true)
                        // }

                    })
                }
            })
        }
        remove_empty_checklist()
        // $(this).on('click')
    })
}
function delete_checklist(){
    console.log(1);
    $('.delete-icon').on('click', function(e){
        console.log(2);
        e.stopImmediatePropagation()
        if(!$(this).hasClass('remove_empty')){
            // console.log(1);
            var task_type = $(this).data('check-type')
            var id = $(this).data('id')
            $.ajax({
                // url: '/checklists/delete_checklist',
                url: '/checklists/delete_checklist',
                method: 'post',
                data: { type: task_type, type_id: id },
                dataType: 'script',
            })
        }
    })
}
function video_screen_toggle_views(){
    if($('#current_view').val() == 'thumb'){
        $('#list_view').removeClass('hidden')
        $(this).addClass('hidden')
        $('#thumb_view_content').removeClass('hidden')
        $('#list_view_content').addClass('hidden')
        $('#current_view').val('thumb')
    }else{
        $('#thumb_view').removeClass('hidden')
        $(this).addClass('hidden')
        $('#thumb_view_content').addClass('hidden')
        $('#list_view_content').removeClass('hidden')
        $('#current_view').val('list')
    }
    $('#list_view').on('click', function(){
        $('#thumb_view').removeClass('hidden')
        $(this).addClass('hidden')
        $('#thumb_view_content').addClass('hidden')
        $('#list_view_content').removeClass('hidden')
        $('#current_view').val('list')
    })
    $('#thumb_view').on('click', function(){
        $('#list_view').removeClass('hidden')
        $(this).addClass('hidden')
        $('#thumb_view_content').removeClass('hidden')
        $('#list_view_content').addClass('hidden')
        $('#current_view').val('thumb')
    })
}
function video_screen_ordering(){
    if($('#list_order_by').attr('value') == 'name'){
        $('.video_order .dropdown-menu li.order_by_name').addClass('active')
    }else{
        $('.video_order .dropdown-menu li.order_by_type').addClass('active')
    }
    $('.video_order .dropdown-menu li').on('click', function(){
        $('.video_order .dropdown-menu li').removeClass('active')
        console.log($(this));
        $(this).addClass('active')
        var order_by = $(this).attr('value')
        $('#list_order_by').val(order_by)
        $.ajax({
            // url: '/videos/order_list?order_by='+order_by+'&view='+$('#current_view').attr('value')+'&search='+$('#search_key').attr('value')+'&category_name='+$('#category_val').attr('value'),
            url: '/videos/order_list?order_by='+order_by+'&view='+$('#current_view').attr('value')+'&search='+$('#search_key').attr('value')+'&category_name='+$('#category_val').attr('value'),
            method: 'get',
            dataType: 'script'
        })
        console.log('change');
        console.log($(this));
        console.log($(this).attr('value'))
    })
}
function visa_screen_company_list(){
    $( ".select_company_main" ).select2({
        // theme: "bootstrap"
        placeholder: 'Select Company',
        // allowClear: true,
        // minimumInputLength: 3,
        ajax: {
            // url: '/companies/company_select2?tab_type='+$('#type_select_val').val(),
            url: '/companies/company_select2?tab_type='+$('.type_select').val(),
            dataType: 'json',
            data: function(term){
                return{
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.company_name,
                            // slug: item.slug,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
}
function visa_screen_industry_list(){
    $( ".select_industry_main" ).select2({
        // theme: "bootstrap"
        placeholder: 'Select Industry',
        // allowClear: true,
        // maximumInputLength: 20,
        // minimumInputLength: 3,
        ajax: {
            // url: '/companies/industry_select2?tab_type='+$('#type_select_val').val(),
            url: '/companies/industry_select2?tab_type='+$('#type_select_val').val(),
            dataType: 'json',
            data: function(term){
                return{
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.industry_name,
                            // slug: item.slug,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
}
function clear_all_select2(){
    // $('.clear-all').on('mouseover', function(){
    //     $(this).css('mouse', 'pointer')
    // });
    $('.clear-all').on('click', function(){
        console.log($(this).parent().children().closest('select'));
        var id = $(this).parent().children().closest('select').attr('id')
        console.log($('#'+id));
        if($("#"+id).val() != ''){
            $("#"+id).val('').trigger('change')
        }
    })
}
function home_screen_hide_on_lost_focus(){
    $('#user_post_title').on('focusout', function(){
        if($(this).val() == ''){
            $('.add-article label').css("display", "block");
            $('.post-box').css("display", "none");
            $('#user_post_title').val('')
            $('user_post_description').val('')
            $('#imgsrc').attr('src', "")
            $('.preview-card .title').empty()
            $('.preview-card .description').empty()
            $('.preview-card').css('display', 'none')
        }
    })
}
function remove_bookmark_sidebar(){
    $('.bookmarked_article .remove_bookmark').hide()
    $('.bookmarked_article').on('mouseover', function(){
        // console.log($(this).children('span.remove_bookmark'))
        $(this).children('span.remove_bookmark').show()
        // $('.fav-comp .remove-fav-icon').show()
    })
    $('.bookmarked_article').on('mouseout', function(){
        // $('.fav-comp .remove-fav-icon').hide()
        $(this).children('span.remove_bookmark').hide()
    })

    $('.bookmarked_article .remove_bookmark').on('click', function(e){
        // console.log($(this).children('a.fav-icon'));
        var user_post_id = $(this).data('user-post-id')
        // var tab_type = $(this).data('tab-type')
        // var company_name = $(this).children('a.fav-icon').data('company-name')
        var is_favorite = 'false'
        // if($(this).children('a.fav-icon').hasClass('active') == true){
        //     $(this).children('a.fav-icon').removeClass('active')
        //     like = 'unlike'
        // }else {
        //     $(this).children('a.fav-icon').addClass('active')
        //     like = 'like'
        // }
        e.preventDefault()
        e.stopImmediatePropagation()
        // console.log(company_id);
        // console.log(company_name);
        $.ajax({
            // url: '/favorite_post',
            url: '/favorite_post',
            // url: '/companies/add_company_as_favorite',
            data: { user_post_id: user_post_id, is_favorite: is_favorite },
            method: 'post',
            dataType: 'script',

        })
        // $('.chart-cell .star_icon').children("a.fav-icon-"+company_id).removeClass('active')
        $('.post-footer').children().find('a.mark_favorite > i.icon_'+user_post_id).removeClass('active')

    })
}
function open_jobs_load_more(){
    $('.load_open_jobs').on('click', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var that = $(this);
        that.off('click');
        var sort = $('input[type=radio][name=sort_by]:checked').val()
        var job_type = $('input[type=radio][name=open_job_type]:checked').val()
        var city = $('#open_jobs_filter_city').val()
        var company_name = $('#open_jobs_filter_company').val()
        var keyword = $('#open_jobs_filter_search_keyword').val()

        var page = parseInt($('#page_number').val())
        // var sort = $("input[type=radio][name=sort]").val()
        // var sort = $('#sort_by_val').val()

        $('.loader').show()
        $.ajax({
            // url: '/jobs/get_open_jobs?company_name='+company_name+'&sort='+sort+'&job_type='+job_type+'&city='+city+'&keyword='+keyword+'&page='+ (page + 1),
            url: '/jobs/get_open_jobs?company_name='+company_name+'&sort='+sort+'&job_type='+job_type+'&city='+city+'&keyword='+keyword+'&page='+ (page + 1),
            // url: '/jobs/search',
            dataType: 'script',
            method: 'get',
        }).always(function() {
            $('.loader').hide()
            that.on('click') // add handler back after ajax
        });
        $('.loader').hide()
    })
}
function home_sticky_sidebar(){
    var stickySidebar = new StickySidebar('#left_sidebar', {
        topSpacing: 90,
        bottomSpacing: 20,
        containerSelector: '.container',
        innerWrapperSelector: '.sidebar__inner'
    });
    var stickySidebar = new StickySidebar('#right_sidebar', {
        topSpacing: 90,
        bottomSpacing: 20,
        containerSelector: '.container',
        innerWrapperSelector: '.sidebar__inner'
    });
 var stickySidebar = new StickySidebar('#add_article', {
        topSpacing: 90,
        bottomSpacing: 20,
        containerSelector: '.container',
        innerWrapperSelector: '.sidebar__inner'
    });
}

function jobs_screen_load_steps(){
    // alert(1)

    // $('input[type=radio][name=job_region]').change(function(){
    //     console.log(($(this).val()));
    //     $('.job_region_value').val($(this).val())
    //     $('.step_1').children().last().find('.btn-theme').removeClass('disabled')
    //     console.log($('.step_1').children().last().find('.btn-theme').attr('disabled', false))
    // })
}
function next_steps_jobs(){
    $('input[type=radio][name=job_region]').change(function(){
       console.log(($(this).val()));
       var current_active = '1'
       $('.job_region_value').val($(this).val())
       $('.step_'+current_active).children().last().find('.btn-theme').removeClass('disabled')
       // console.log($('.step_1').children().last().find('.btn-theme').attr('disabled', false))
    })
    $('input[type=radio][name=job]').change(function(){
       console.log(($(this).val()));
       var current_active = '2'
       $('.us_jobs_companies').val($(this).val())
       $('.step_'+current_active).children().last().find('.btn-theme').removeClass('disabled')
       // console.log($('.step_1').children().last().find('.btn-theme').attr('disabled', false))
    })
    $('#job_search_country').change(function(){
        console.log(($(this).val()));
        var current_active = '2'
        // $('.us_jobs_companies').val($(this).val())
        $('.step_'+current_active).children().last().find('.btn-theme').removeClass('disabled')
    })
    $('#job_search_company_name').change(function(){
        $('.submit_jobs').removeClass('disabled')
    })
    $('.steps-back-btn').click(function(){
        console.log($(this));
        var current_active = $(this).data('current-step-id')
        var prev_step = parseInt(current_active) - 1
        console.log(prev_step);
        $('.steps').hide()
        $('.steps').removeClass('active')
        $('.step_'+prev_step).show()
        $('.step_'+prev_step).addClass('active')
    })
    $('.btn-next').on('click', function(){
        var current_active = parseInt($(this).data('current-step-id'))
        var next_step = current_active + 1
        $('.steps').hide()
        $('.steps').removeClass('active')
        $('.step_'+next_step).show()
        $('.step_'+next_step).addClass('active')
        if(current_active == 1){
            // console.log($('input[type=radio][name=job_region]').val());
            if($('.job_region_value').val() == 'international'){
                $('.international_jobs').show();
                $('.us_jobs').hide();
                if($('#job_search_country').val() == "" ){
                    $('.international_jobs').next().first().find('.btn-next').addClass('disabled')
                }else {
                    $('.international_jobs').next().first().find('.btn-next').removeClass('disabled')
                }
                $('#job_search_country').change(function(){
                    if($(this).val() != ""){
                        $('.international_jobs').next().first().find('.btn-next').removeClass('disabled')
                    }else{
                        $('.international_jobs').next().first().find('.btn-next').addClass('disabled')
                    }
                })
            }else{
                $('.international_jobs').hide();
                $('.us_jobs').show();
            }
        }else if (current_active == 2) {
            console.log('================2=============');
            console.log($('.job_region_value').val())
            console.log($('.us_jobs_companies').val())
            if($('.job_region_value').val() == 'us'){
                if($('.us_jobs_companies').val() == 'all'){
                    console.log('in if');
                    $('#company_name').removeClass('hide');
                    $('#company_name').show();
                    $('#job_search_company_name').hide();
                    if(($('#job_search_search').val() == "") || ($('#company_name').val() == "") || ($('#job_search_city').val() != "")){
                        $('.submit_jobs').addClass('disabled')
                    }else {
                        $('.submit_jobs').removeClass('disabled')
                    }
                    $('#select2-job_search_company_name-container').parents().find('span.select2').hide()
                    $('#job_search_search').on('focusout', function(){
                        if(($(this).val() != "") || ($('#company_name').val() != "") || ($('#job_search_city').val() != "")){
                            $('.submit_jobs').removeClass('disabled')
                            // $('#company_name').removeClass('error_class')
                            // $(this).removeClass('error_class')
                        }else{
                            $('.submit_jobs').addClass('disabled')
                            // $(this).addClass('error_class')
                        }
                    })
                    $('#company_name').on('focusout', function(){
                        if(($(this).val() != "") || ($('#job_search_search').val() != "") || ($('#job_search_city').val() != "")    ){
                            $('.submit_jobs').removeClass('disabled')
                            // $(this).removeClass('error_class')
                            // $('#job_search_search').removeClass('error_class')
                        }else{
                            $('.submit_jobs').addClass('disabled')
                            // $(this).addClass('error_class')
                        }
                    })
                    $('#job_search_city').on('focusout', function(){
                        if(($(this).val() != "") || ($('#job_search_search').val() != "") || ($('#company_name').val() != "")    ){
                            $('.submit_jobs').removeClass('disabled')
                            // $(this).removeClass('error_class')
                            // $('#job_search_search').removeClass('error_class')
                        }else{
                            $('.submit_jobs').addClass('disabled')
                            // $(this).addClass('error_class')
                        }
                    })
                }else{
                    console.log('in else');
                    $('#company_name').hide();
                    $('#job_search_company_name').show();
                    // $('#job_search_company_name').attr('required', true)
                    if($('#job_search_company_name').val() == ""){
                        // $('#job_search_search').removeClass('error_class')
                        $('.submit_jobs').addClass('disabled')
                    }
                    else{
                        $('.submit_jobs').removeClass('disabled')
                        // $('#job_search_search').removeClass('error_class')
                    }
                    $('#select2-job_search_company_name-container').parents().find('span.select2').show()
                }
            }
            else{
                $('#job_search_company_name').hide();
                $('#select2-job_search_company_name-container').parents().find('span.select2').hide()
                $('#job_search_city').hide();
                if($('#job_search_search').val() == ""){
                    $('.submit_jobs').addClass('disabled')
                }else{
                    $('.submit_jobs').removeClass('disabled')
                }

                $('#job_search_search').on('focusout', function(){
                    if(($(this).val() != "")){
                        $('.submit_jobs').removeClass('disabled')
                        // $('#company_name').removeClass('error_class')
                        // $(this).removeClass('error_class')
                    }else{
                        $('.submit_jobs').addClass('disabled')
                        // $(this).addClass('error_class')
                    }
                })

                // $('#company_name').removeClass('hide');
                // $('#company_name').show();
            }
            // if(($('.job_region_value').val() == 'international'){
            //     $('#job_search_company_name').hide();
            //
            // }
        }
    })
    // $('.submit_jobs').click(function(){
    //     $('.job_search_form').validate()
    //     if($('.job_region_value').val() == 'us' && $('.us_jobs_companies') == 'top')
    //     {
    //         if($('#job_search_company_name').val() == ""){
    //
    //         }
    //     }
    // })
}
function gameplan_anchor(){
    $('.refer_detail').on('click', function(){
        var id = $(this).data('id')
        $('.collapse').removeClass('in')
        $('#collapse'+id).addClass('in')
    })
}
function timepicker_meeting(){
    // $('#create_meeting').on('show.bs.modal', function(){
    //     // alert(1)
    //     setTimeout(function(){
    //
    //         $('.timepicker').timepicker({placeholder: 'Select Time', useCurrent: 'false'});
    //     }, 300)
    //     // $('.start_time').timepicker({placeholder: 'Select Start Time'})
    //     // $('.end_time').timepicker({placeholder: 'Select End Time'})
    // })
}
function delete_meeting(){
    $('.delete_network').on('click', function(e){
        e.stopImmediatePropagation();
        var parent = $(this).parents().find('div.tab-pane.active');
        console.log(parent.children().length);
        var id = $(this).data('id')
        $.ajax({
            // url: '/network_trackers/'+id,
            url: '/network_trackers/'+id,
            method: 'delete',
            dataType: 'script',
            success: function(data){
                $('.meeting-detail-'+id).remove();
                if(!(parent.children().length > 0)){
                    if(parent.attr('id') == 'tab1'){
                        parent.append('<h3> No upcoming meetings. Please add a new meeting. </h3>')
                    }else{
                        parent.append('<h3> No past meetings. </h3>')
                    }
                }
                toastr.success('Meeting Deleted')
            },
            failure: function(data){
                toastr.error('Something went wrong')
            }
        })
    })
}
function home_search(){
    $('.home_article_search').on('focusout', function(e){
        e.stopImmediatePropagation();
        $(this).off('focusout')
        // console.log('search');
        var search = $('#home_search').val()
        var page = $('.current').text()
        if(search == ""){
            $.ajax({
                // url: '/',
                url: '/',
                // data: { page: 1 },
                method: 'get',
                dataType: 'script'
            })
        }else{
            $.ajax({
                // url: '/search',
                url: '/search',
                data: { search: search, page: page },
                method: 'get',
                dataType: 'script'
            })
        }
    })


    $('.home_article_search').on('keypress', function(e){
        if(e.which == 13){
            // e.stopImmediatePropagation()
            var search = $('#home_search').val()
            var page = $('.current').text()
            if(search == ""){
                $.ajax({
                    // url: '/',
                    url: '/',
                    // data: { page: 1 },
                    method: 'get',
                    dataType: 'script'
                })
            }else{
                $.ajax({
                    // url: '/search',
                    url: '/search',
                    data: { search: search, page: page },
                    method: 'get',
                    dataType: 'script'
                })
            }
        }
    })
}
function trigger_social_share(){
    $('.home_social_share .social .fb-icon').click(function(){
        $('.ssb-facebook').trigger('click')
    })
    $('.home_social_share .social .twt-icon').click(function(){
        $('.ssb-twitter').trigger('click')
    })
    $('.home_social_share .social .lnk-icon').click(function(){
        $('.ssb-linkedin').trigger('click')
    })
    $('.home_social_share .social .mail-icon').click(function(){
        $('.ssb-email').trigger('click')
    })
}
function clear_network_search(){
    $('.search-input i.fa-remove').click(function(){
        $('.network_tracker_search').val('')
        $('.loader').show()
        $.ajax({
            // url: '/network_trackers/search',
            url: '/network_trackers/search',
            // url: '_web/country_insights/search?search='+country,
            method: 'get',
            dataType: 'script',
            // success: function(response){
            //     console.log('success');
            // },
            // failure: function(response){
            //     console.log('failure');
            // }
        })
        // $('.network_tracker_search').trigger('change')
    })
}
function visa_insight_select2(){
    $( ".select_state" ).val("").select2({
        ajax: {
            // url: '/companies/state_select2?tab_type='+$('#type_select_val').val(),
            url: '/companies/city_selgetect2?tab_type='+$('#type_select_val').val(),
            dataType: 'json',
            data: function(term){
                return{
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.state_name,
                            // slug: item.slug,
                            id: item.id
                        }
                    })
                };
            }
        }
    })
    $( ".select_state_main" ).select2({
        placeholder: 'Select State',
        ajax: {
            // url: '/companies/state_select2?tab_type='+$('#type_select_val').val(),
            url: '/companies/state_select2?tab_type='+$('#type_select_val').val(),
            dataType: 'json',
            data: function(term){
                return{
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.state_name.toProperCase(),
                            // slug: item.slug,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
    var state_ids = $('#state_select_val').val()
    $( ".select_city" ).val("").select2({
        placeholder: 'Select City',
        ajax: {
            // url: '/companies/city_select2?tab_type='+$('#type_select_val').val()+"&state_ids="+state_ids,
            url: '/companies/city_select2?tab_type='+$('#type_select_val').val()+"&state_ids="+state_ids,
            dataType: 'json',
            data: function(term){
                return{
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.city_name.toProperCase(),
                            // slug: item.slug,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
    $( ".select_job_title" ).select2({
        placeholder: 'Select Job Title',
        ajax: {
            // url: '/companies/job_title_select2?tab_type='+$('#type_select_val').val(),
            url: '/companies/job_title_select2?tab_type='+$('#type_select_val').val(),
            dataType: 'json',
            data: function(term){
                return{
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.job_title_name.toProperCase(),
                            // slug: item.slug,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $( ".select_company" ).val("").select2({
        // theme: "bootstrap"
        placeholder: 'Select Company',
        tags: true,
        // allowClear: true,
        // minimumInputLength: 3,
        ajax: {
            // url: '/companies/company_select2',
            url: '/companies/company_select2',
            dataType: 'json',
            data: function(term){
                return{
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.company_name.toProperCase(),
                            // slug: item.slug,
                            id: item.company_name
                        }
                    })
                };
            }
        }
    });
    $( "#job_search_company_name" ).select2({
        // theme: "bootstrap"
        placeholder: 'Company Name (Required)',
        // allowClear: true,
    });
}
function get_country_jobs(){
    $('.country_jobs_search').on('click', function(){
        var country = $(this).data('name')
        $.ajax({
            // url: '../jobs/search',
            url: '/jobs/search',
            dataType: 'html',
            method: 'post',
            data: { 'job_search[job_region]': 'international', 'job_search[search]': country, 'job_search[job_type]': 'all', 'job_search[publish_date]': 'any', 'job_search[city]': '', 'job_search[page]': 1, 'job_search[country]': country, 'job_search[company_name]': '', 'sort':  'relevance'},
            success: function(response){
                // $('.loader').hide()

                (function($){
                  $(window).on("load",function(){
                    $(".vr_scroll").mCustomScrollbar({
                      axis:"y",
                      theme:"dark-3",
                      advanced: {
                        autoExpandVerticalScroll:true //optional (remove or set to false for non-dynamic/static elements)
                      }
                    });
                  });
                })(jQuery);
                // e.stopImmediatePropagation();
            },
        });
    })
}
function delete_post() {
    $('.delete_post').on('click', function(){
        var id = $(this).data('id')
        $.ajax({
            // url: '/delete_post?id='+id,
            url: '/delete_post?id='+id,
            method: 'get',
            dataType: 'script'
        })
    })
}
function favorite_jobs(){
    $('.fav-icon-jobs').on('click', function(e){
        console.log($(this));
        // console.log($(this).children('a.fav-icon'));
        var id = $(this).data('job-id')
        var title = $(this).data('job-title')
        var job_link = $(this).data('url')
        var company = $(this).data('company-name')
        // var tab_type = $(this).data('tab-type')
        var like = ''
        if($(this).hasClass('active') == true){
            $(this).removeClass('active')
            like = 'unlike'
        }else {
            $(this).addClass('active')
            like = 'like'
        }
        e.preventDefault()
        e.stopImmediatePropagation()
        // console.log(company_id);
        // console.log(company_name);
        $.ajax({
            // url: '/jobs/favorite_job',
            url: '/jobs/favorite_job',
            method: 'post',
            data: { job_id: id, like: like, job_title: title, job_link: job_link, company_name: company },
            dataType: 'script',
            // success: function(response){
            //     $('.loader').hide()
            //     console.log(response);
            //     toastr.success(response['message'])
            //     window.location.reload(true)
            // },
            // failure: function(response){
            //     $('.loader').hide()
            // }

        })
        // $('.cell .star_icon_jobs').children("a.fav-icon-jobs-"+id).toggleClass('active')
    })
}
function remove_favorite_jobs_sidebar(){
    $('.fav-job .remove-fav-icon-jobs').hide()
    $('.fav-job').on('mouseover', function(){
        $(this).children().find('a.remove-fav-icon-jobs').show()
        // $('.fav-comp .remove-fav-icon').show()
    })
    $('.fav-job').on('mouseout', function(){
        // $('.fav-comp .remove-fav-icon').hide()
        $(this).children().find('a.remove-fav-icon-jobs').hide()
    })

    $('.fav-job .remove-fav-icon-jobs').on('click', function(e){
        // console.log($(this).children('a.fav-icon'));
        var id = $(this).data('job-id')
        // var tab_type = $(this).data('tab-type')
        var title = $(this).children('a.fav-icon-jobs').data('job-title')
        var job_link = $(this).children('a.fav-icon-jobs').data('url')
        var like = 'unlike'
        var company = $(this).data('company-name')
        // if($(this).children('a.fav-icon').hasClass('active') == true){
        //     $(this).children('a.fav-icon').removeClass('active')
        //     like = 'unlike'
        // }else {
        //     $(this).children('a.fav-icon').addClass('active')
        //     like = 'like'
        // }
        e.preventDefault()
        e.stopImmediatePropagation()
        // console.log(company_id);
        // console.log(company_name);
        $.ajax({
            // url: '/jobs/favorite_job',
            url: '/jobs/favorite_job',
            method: 'post',
            data: { job_id: id, like: like, job_title: title, job_link: job_link, company_name: company },
            dataType: 'script',
            // success: function(response){
            //     $('.loader').hide()
            //     console.log(response);
            //     toastr.success(response['message'])
            //     window.location.reload(true)
            // },
            // failure: function(response){
            //     $('.loader').hide()
            // }

        })
        $('.cell .star_icon_jobs').children("a.fav-icon-jobs-"+id).removeClass('active')
    })
}
