//Placeholder image replace to browse
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(input).next('#imgsrc').attr('src', e.target.result);
            //remove icon appear
            $('.remove-icon').css("display", "block");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

//browse input get url
$("#inputfile").change(function () {
    readURL(this);
});

//browse input trigger hidden
$("#uploadimg").click(function(){
   $("#inputfile").click();
    $('.post-box').css("display", "block");
    $('.add-article label').css("display", "none");
});


//remove icon event
$('.remove-icon').click(function(){
    $('#imgsrc').removeAttr('src');
    $('.remove-icon').css("display", "none");
});


//add post box appear
$('.add-article label').click(function(){
    // alert(1)
    $('.post-box').css("display", "block");
    $('.add-article label').css("display", "none");
});

//comments loop
$('#show-comment').click(function(){
    $('#post-1-comment').toggleClass("show");
});

$('#show-comment2').click(function(){
    $('#post-2-comment').toggleClass("show");
});



//mobile expand filter
$('#mexpand').click(function(){
    $('.mobile-expand').toggleClass("show");
    $('.expand').toggleClass("collaps");
});

//expand filter
$('#expand').click(function(){
    $('.indsec').toggleClass("show");
    $('.expand').toggleClass("collaps");
});

//login forgot
$('#forgot-btn').click(function(){
    $('#forgot').addClass("show");
    $('#login').addClass("hide");
});

$('#login-btn').click(function(){
    $('#forgot').removeClass("show");
    $('#login').removeClass("hide");
});

$('#signup-btn').click(function(){
    $('#login').addClass("hide");
    $('#signup').addClass("show");
});
$('#login-btn2').click(function(){
    $('#login').removeClass("hide");
    $('#signup').removeClass("show");
});

$('#signupnext').click(function(){
    $('#signup').removeClass("show");
    $('#signup2').addClass("show");
});



//browse input trigger hidden
$("#upload-resume-btn").click(function(){
   $("#upload-resume").click();
});





// media query event handler for < 767px
var mql = window.matchMedia("screen and (max-width: 767px)");
if (mql.matches){ // if media query matches

// search field for header
$('header .searchfield').focus(function(){
    /*to make this flexible, I'm storing the current width in an attribute*/
    $(this).attr('data-default', $(this).width());
    $(this).animate({ width: 250 }, 'slow');
    $(this).css("position", "absolute");
    $(this).css("right", "55px");
    $(this).css("padding-left", "35px");
    $('.navbar-brand').css("display", "none");
}).blur(function(){
    /* lookup the original width */
  var w = $(this).attr('data-default');
    $(this).css({ width: w }, 'fast');
    $(this).css("position", "relative");
    $(this).css("right", "0px");
    $(this).css("padding-left", "27px");
    $('.navbar-brand').css("display", "block");
});
// search field for header end here


}
else{
}
// media end


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});





// textarea auto height
var textarea = document.querySelector('textarea');

if(textarea){
    textarea.addEventListener('keydown', autosize);
}

function autosize(){
  var el = this;
  setTimeout(function(){
    el.style.cssText = 'height:auto; padding:0';
    // for box-sizing other than "content-box" use:
    // el.style.cssText = '-moz-box-sizing:content-box';
    el.style.cssText = 'height:' + el.scrollHeight + 'px';
  },0);
}






// media query event handler for < 767px
var mql = window.matchMedia("screen and (min-width: 1023px) and (max-width: 2500px)");
if (mql.matches){ // if media query matches


// sticky sidebar
//$('#fixed-profile').affix({
   // offset: {
 //     top: $('#fixed-profile').offset().top,
  //  }
//});
//$('#fixed-favorite').affix({
  //  offset: {
 //     top: $('#fixed-favorite').offset().top,
 //   }
//});
}
else{
}
// media end
