class ApplicationController < ActionController::Base
    def configure_permitted_parameters
            added_attrs = [:email, :password, :password_confirmation]
            devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
            devise_parameter_sanitizer.permit :sign_in, keys: added_attrs
            devise_parameter_sanitizer.permit :account_update, keys: added_attrs
    end

    private

        def after_sign_in_path_for(resource_or_scope)
            # if resource.class_name.eql?('User')
            #     setting = SchoolSetting.find_by(school_id: resource.school_id)
            #     if setting.present? && setting.force_gameplan == true
            #         gameplan_path
            #     else
            #         authenticated_root_path
            #     end
            #
            # else
            #     authenticated_root_path
            # end
            logger.info { "=======signing in=========" }
            authenticated_root_path
        end

        def after_sign_out_path_for(resource_or_scope)
            unauthenticated_root_path
        end
end
