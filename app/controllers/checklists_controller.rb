class ChecklistsController < ApplicationController

    def index
        # @data = Hash.new
        @checklists = Checklist.get_list(current_user)
        @checklist_categories = @checklists.group_by{ |c| {category_id: c[:category_id], category_name: c[:category_name]} }.keys
        # @data = Hash[@checklists.map {|key, value| [key, value]}]
        # @hash = Hash[@checklists.collect { |key, value| [key, value] } ]
        logger.info "===class==#{@checklists.inspect}===#{}"
        # exit(0)
    end

    def create
        logger.info "=====params===#{params.inspect}"
        if params[:task_type].present?  && params[:task_type].eql?('main')
            # permit(:subcategory_name,:category_id,:school_id,:ctype)
            max_sort_order = ChecklistSubcategory.where('category_id = ? and sort_order <> 0 and is_deleted <> 1', params[:type_id]).present? ? ChecklistSubcategory.where('category_id = ? and sort_order <> 0 and is_deleted <> 1', params[:type_id]).order('sort_order DESC').first.sort_order : 0
            @subchecklist = ChecklistSubcategory.new(category_id: params[:type_id], subcategory_name: params[:checklist][:title])
            @subchecklist.sort_order = max_sort_order + 1
		    if current_user.user_type == 3
	        	@subchecklist.ctype = 1
	        	@subchecklist.school_id = current_user.school_id
	        elsif current_user.user_type == 2
	        	school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
	        	@subchecklist.ctype = 1
	        	@subchecklist.school_id = school_user
	        end
	        @subchecklist.user_id = current_user.id
            logger.info "==cgeck----#{@subchecklist.inspect}"
            @is_marked = UserSubchecklist.find_by(subcategory_id: params[:type_id], user_id: current_user.id).try(:is_mark)
	        if @subchecklist.save
                logger.info "===save==="
                respond_to do |format|
                    logger.info "===req=== #{request.format}"
                    format.json { render json: {success: true} }
                    format.html { redirect_to checklists_path }

                    format.js {  }
                end
            else
                # logger.info "===err===#{subchecklist.errors.full_messages}"
                # redirect_to checklists_path, alert: subchecklist.errors.full_messages.join(', ')
                respond_to do |format|
                    format.html { redirect_to checklists_path, alert: subchecklist.errors.full_messages.join(', ') }
                    format.json { render json: {success: false, message: subchecklist.errors.full_messages.join(', ')} }
                    format.js {  }
                end
		    end
        else
            # .permit(:title,:subcategory_id,:school_id,:ctype)
            # checklist = Checklist.new(checklist_params)
            @checklist = Checklist.new(subcategory_id: params[:type_id], title: params[:checklist][:title])
		    if current_user.user_type == 3
	        	@checklist.ctype = 1
	        	@checklist.school_id = current_user.school_id
	        elsif current_user.user_type == 2
	        	school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
	        	@checklist.ctype = 1
	        	@checklist.school_id = school_user
	        end
	        @checklist.user_id = current_user.id
            @is_marked = UserChecklist.find_by(checklist_id: params[:type_id], user_id: current_user.id).try(:is_mark)
	        if @checklist.save
                logger.info "===save==="
                respond_to do |format|
                    logger.info "===req=== #{request.format}"
                    format.json { render json: {success: true} }
                    format.html { redirect_to checklists_path }
                    format.js {  }
                end
		        # redirect_to checklists_path, notice: 'Checklist Added'
            else
                respond_to do |format|
                    format.html { redirect_to checklists_path, alert: checklist.errors.full_messages.join(', ') }
                    format.json { render json: {success: false, message: checklist.errors.full_messages.join(', ')} }
                    format.js {  }
                end
                # redirect_to checklists_path, alert: checklist.errors.full_messages.join(', ')
		    end
            # redirect_to checklists_path, alert: 'Something went wrong, Please try again'
        end
    end

    def delete_checklist
        if params[:type].eql?('main')
            # subcategory = ChecklistSubcategory.find_by(:id => params[:subcategory_id])
            subcategory = ChecklistSubcategory.find_by(:id => params[:type_id])
            if subcategory.destroy
                # checklist = Checklist.where(:subcategory_id => params[:subcategory_id])
                checklist = Checklist.where(:subcategory_id => params[:type_id])
                checklist.update_all(:is_deleted => true)
                respond_to do |format|
                    # flash[:notice] = 'Checklist deleted'
                    format.html { redirect_to checklists_path }
                    format.js { }
                end
                # redirect_to checklists_path, notice: 'Checklist deleted'
            else
                respond_to do |format|
                    flash[:alert] = subcategory.errors.full_messages.join(', ')
                    format.html { redirect_to checklists_path, alert: subcategory.errors.full_messages.join(', ') }
                    format.js { }
                end
            end
        else
            # checklist = Checklist.find_by(:id => params[:checklist_id])
            checklist = Checklist.find_by(:id => params[:type_id])
            @parent_id = checklist.subcategory_id
            if checklist.destroy
                # redirect_to checklists_path, notice: 'Checklist deleted'
                respond_to do |format|
                    # flash[:notice] = 'Checklist deleted'
                    format.html { redirect_to checklists_path }
                    format.js { }
                end
            else
                # redirect_to checklists_path, alert: checklist.errors.full_messages.join(', ')
                respond_to do |format|
                    flash[:alert] = subcategory.errors.full_messages.join(', ')
                    format.html { redirect_to checklists_path, alert: subcategory.errors.full_messages.join(', ') }
                    format.js { }
                end
            end
        end
    end

    def edit_checklist
        if params[:task_type].eql?('main')
            logger.info "===params===#{params.inspect}"
            @subcategory = ChecklistSubcategory.find_by(:id => params[:type_id])
            if @subcategory.update_attributes(:subcategory_name => params[:checklist][:title])
                logger.info "====check===#{UserSubchecklist.find_by(subcategory_id: params[:type_id], user_id: current_user.id)}"
                @is_marked = UserSubchecklist.find_by(subcategory_id: params[:type_id], user_id: current_user.id).try(:is_mark)
                @checklist = Checklist.where(:subcategory_id => params[:type_id])
                @checklist.update_all(:is_deleted => true)
                respond_to do |format|
                    # flash[:notice] = 'Checklist Updated'
                    format.html { redirect_to checklists_path, notice: 'Checklist Updated' }
                    format.js {  }
                end
            else
                respond_to do |format|
                    flash[:alert] = @subcategory.errors.full_messages.join(', ')
                    format.html { redirect_to checklists_path, alert: @subcategory.errors.full_messages.join(', ') }
                    format.js {  }
                end

            end
        else
            @checklist = Checklist.find_by(:id => params[:type_id])
            if @checklist.update_attributes(:title => params[:checklist][:title])
                logger.info "====check===#{UserChecklist.find_by(checklist_id: params[:type_id], user_id: current_user.id).inspect}"
                @is_marked = UserChecklist.find_by(checklist_id: params[:type_id], user_id: current_user.id).try(:is_mark)
                respond_to do |format|
                    # flash[:notice] = 'Checklist Updated'
                    format.html { redirect_to checklists_path }
                    format.js {  }
                end
            else
                respond_to do |format|
                    flash[:alert] = @checklist.errors.full_messages.join(', ')
                    format.html { redirect_to checklists_path, alert: @checklist.errors.full_messages.join(', ') }
                    format.js {  }
                end
            end
        end
    end

    def mark_checklist
        if params[:check_type].eql?('main')
            user_subchecklist = UserSubchecklist.find_by(subcategory_id: params[:type_id], user_id: current_user.id )
			if user_subchecklist.blank? == true
                # :subcategory_id,:is_mark
				user_subchecklist = UserSubchecklist.new(subcategory_id: params[:type_id], is_mark: params[:is_mark])
				user_subchecklist.user_id = current_user.id
				if !user_subchecklist.save
                    logger.info "==err===#{user_subchecklist.errors.full_messages}"
                    redirect_to checklists_path, alert: 'Failed to mark checklist'
                    return
                else
				   #Mark all task in that subcategory
                   checklists = Checklist.where(subcategory_id: params[:type_id], user_id: [0,current_user.id], school_id: [0,current_user.school_id]).pluck(:id)
				   checklists.each do |c|
						uchecklist = UserChecklist.find_by(checklist_id: c, user_id: current_user.id )
						if uchecklist.blank? == true
							uchecklist = UserChecklist.new
							uchecklist.checklist_id = c
							uchecklist.is_mark = params[:is_mark]
							uchecklist.user_id = current_user.id
							uchecklist.save
						else
							uchecklist.update_attributes(is_mark: params[:is_mark])
						end
					end
			    end
			else
	            user_subchecklist.update_attributes(is_mark: params[:is_mark])

				uchecklists = Checklist.where(subcategory_id: params[:type_id], user_id: [0,current_user.id]).pluck(:id)
				uchecklists.each do |c|
					uchecklist = UserChecklist.find_by(checklist_id: c, user_id: current_user.id )
					if uchecklist.blank? == true
						uchecklist = UserChecklist.new
						uchecklist.checklist_id = c
						uchecklist.is_mark = params[:is_mark]
						uchecklist.user_id = current_user.id
						uchecklist.save
					else
						uchecklist.update_attributes(:is_mark => params[:is_mark])
					end
				end
			end

			subcategory = ChecklistSubcategory.find_by(id: params[:type_id])
			if params[:is_mark] == 'false'
                category_checklist = UserChecklistCategory.find_by(user_id: current_user.id, category_id: subcategory.category_id)
				if category_checklist.blank? == false
					category_checklist.update_attributes(is_mark: false)
				end
			end

			subchecklists = ChecklistSubcategory.where(category_id: subcategory.category_id).pluck(:id)
			total_checklist = subchecklists.count
			marked_checklist = UserSubchecklist.where(user_id: current_user, subcategory_id: subchecklists, is_mark: true).count

			if total_checklist == marked_checklist
				category_checklist = UserChecklistCategory.find_by(user_id: current_user.id, category_id: subcategory.category_id)
				if category_checklist.blank? == false
					category_checklist.update_attributes(:is_mark => true)
				end
			end
        else
            user_checklist = UserChecklist.find_by(checklist_id: params[:type_id], user_id: current_user.id )
			checklist = Checklist.find_by(:id => params[:type_id])
			if user_checklist.blank? == true
                # permit(:checklist_id,:is_mark)
				user_checklist = UserChecklist.new(checklist_id: params[:type_id], is_mark: params[:is_mark])
				user_checklist.user_id = current_user.id
				if !user_checklist.save
                    logger.info "==err===#{user_checklist.errors.full_messages}"
                    redirect_to checklists_path, alert: 'Failed to mark checklist'
                    return
			    end
			else
				user_checklist.update_attributes(is_mark: params[:is_mark])
			end

			subcategory = ChecklistSubcategory.find_by(id: checklist.subcategory_id)
			if params[:is_mark] == 'false'
				subchecklist = UserSubchecklist.find_by(user_id: current_user.id, subcategory_id: checklist.subcategory_id)
				if subchecklist.blank? == false
					subchecklist.update_attributes(is_mark: false)
					categorylist = UserChecklistCategory.find_by(user_id: current_user.id, category_id: subcategory.category_id)
					if categorylist.blank? == false
						categorylist.update_attributes(is_mark: false)
					end
				end
			end

			checklists = Checklist.where(subcategory_id: checklist.subcategory_id).pluck(:id)
			total_checklist = checklists.count
			marked_checklist = UserChecklist.where(user_id: current_user, checklist_id: checklists, is_mark: true).count

			if total_checklist == marked_checklist
				subchecklist = UserSubchecklist.find_by(user_id: current_user.id, subcategory_id: checklist.subcategory_id)
				if subchecklist.blank? == false
					subchecklist.update_attributes(is_mark: true)

					subchecklists = ChecklistSubcategory.where(category_id: subcategory.category_id).pluck(:id)
					total_subchecklist = subchecklists.count
					marked_subchecklist = UserSubchecklist.where(user_id: current_user, subcategory_id: checklist.subcategory_id, is_mark: true).count
					if total_subchecklist == marked_subchecklist
						categorylist = UserChecklistCategory.find_by(user_id: current_user.id, category_id: subcategory.category_id)
						if categorylist.blank? == false
							categorylist.update_attributes(is_mark: true)
						end
					end
				end
			end
        end
        @checklists = Checklist.get_list(current_user)
        @checklist_categories = @checklists.group_by{ |c| {category_id: c[:category_id], category_name: c[:category_name]} }.keys
        respond_to do |format|
            # flash[:alert] = @checklist.errors.full_messages.join(', ')
            format.html { redirect_to checklists_path }
            format.js {  }
        end
    end
end
