class CompaniesController < ApplicationController
    def index
        begin
            # session[:companies_search] = ""
            logger.info "=====cook===#{session[:companies_search].inspect}"
            @states = State.all.order("state_name asc")#.limit(100)
            @cities = City.all.order("city_name asc").limit(100)
            @job_titles = JobTitle.all.order("job_title_name asc")
            @industries = Industry.all.order('sort_order ASC').limit(500)
            @companies = Company.all.order('sort_order ASC').limit(500)
            @favorite_companies = FavoriteCompany.get_list(current_user)
            @companies_list = Company.all.order("company_name asc").limit(100)
            @birth_countries = BirthCountry.all.order("name asc").limit(500)
            @universities = University.all.order("name asc").limit(500)
            @cities = City.select('id', ' CONCAT(UCASE(LEFT(lower(city_name), 1)), SUBSTRING(lower(city_name), 2)) AS city_name').where('sort_order < 1000').order('city_name asc')
            load_default_companies
        rescue Exception => e
        end
    end

    def get_cities
        state_ids = params[:state_ids].split(',')
        if params[:tab_type].present?
            case params[:tab_type]
            when 'greencard'
                if params[:state_ids].present? && params[:state_ids] != 'null'
                    @cities = GreenCardCity.select('id', ' CONCAT(UCASE(LEFT(lower(city_name), 1)), SUBSTRING(lower(city_name), 2)) AS city_name').where('state_id in (?) and sort_order < 1000', state_ids).order('city_name asc')
                else
                    @cities = GreenCardCity.select('id', ' CONCAT(UCASE(LEFT(lower(city_name), 1)), SUBSTRING(lower(city_name), 2)) AS city_name').where('sort_order < 1000').order('city_name asc')
                end
            when 'alumni'
                if params[:state_ids].present? && params[:state_ids] != 'null'
                    @cities = UniversityCity.select('id', ' CONCAT(UCASE(LEFT(lower(city_name), 1)), SUBSTRING(lower(city_name), 2)) AS city_name').where('state_id in (?) and sort_order < 1000', state_ids).order('city_name asc')
                else
                    @cities = UniversityCity.select('id', ' CONCAT(UCASE(LEFT(lower(city_name), 1)), SUBSTRING(lower(city_name), 2)) AS city_name').where('sort_order < 1000').order('city_name asc')
                end
            else
                if params[:state_ids].present? && params[:state_ids] != 'null'
                    @cities = City.select('id', ' CONCAT(UCASE(LEFT(lower(city_name), 1)), SUBSTRING(lower(city_name), 2)) AS city_name').where('state_id in (?) and sort_order < 1000', state_ids).order('city_name asc')
                else
                    @cities = City.select('id', ' CONCAT(UCASE(LEFT(lower(city_name), 1)), SUBSTRING(lower(city_name), 2)) AS city_name').where('sort_order < 1000').order('city_name asc')
                end
            end
        else
            if params[:state_ids].present? && params[:state_ids] != 'null'
                @cities = City.select('id', ' CONCAT(UCASE(LEFT(lower(city_name), 1)), SUBSTRING(lower(city_name), 2)) AS city_name').where('state_id in (?) and sort_order < 1000', state_ids).order('city_name asc')
            else
                @cities = City.select('id', ' CONCAT(UCASE(LEFT(lower(city_name), 1)), SUBSTRING(lower(city_name), 2)) AS city_name').where('sort_order < 1000').order('city_name asc')
            end
        end
        @cities
        # result =  @cities.map{|x| [
        #     id: x.id,
        #     city_name: x.city_name.titleize
        #     ]
        # }
        # @cities = result.flatten
    end

    def search
        # logger.info { "=====all params------#{params.inspect}" }
        # logger.info { "=========search=============#{params[:search].inspect}" }
        search_data = []
        search_attr = search_params
        # logger.info { "==assigned====#{search_attr}" }
        session[:companies_search] = ""
        # params[:search].permit
        logger.info "====search=============#{search_params.inspect}"
        group_param = params[:is_show_map] == "true" ? " latitude,longitude " : "company_id"
        limit = params[:limit] ? params[:limit]  : "100"
		offset = params[:offset] ? params[:offset]  : "0"


        params[:search].each do |key, value|
            value.each do |v|
                params[:search][key].reject!{ |x| x == v && v == ""}
            end
        end
        params[:search].reject!{ |x| x.empty? || x.blank? }
        logger.info { "======params=====#{search_attr.to_h.class}" }
        additional_params = { 'type' => params[:type], 'areatype' => params[:areatype], 'year' => params[:year] }
        # search_attr.merge(additional_params)
        # search_data = { search: search_attr, type: params[:type], areatype: params[:areatype], year: params[:year] }
        search_data = additional_params.merge(search_attr.to_h)
        logger.info { "==========search paramsp========#{search_data.inspect}" }
        # search_data.merge({ type: params[:type], areatype: params[:areatype], year: params[:year] })
        # logger.info "---data==#{search_data}"
        session[:companies_search] = search_data
        if params[:type].present? && params[:type].eql?('greencard')
            result = find_green_card(limit, offset, group_param, params)
        elsif params[:type].present? && params[:type].eql?('alumni')
            result = find_alumni(limit, offset, group_param, params)
        else
            result = find_company(limit, offset, group_param, params)
        end
        # logger.info "===r3es===#{result.inspect}"
		return
    end

    def fetch_master_data
        if params[:type].eql?('greencard')
            logger.info { "1" }
            @states = GreenCardState.all.order("state_name asc") #.pluck(:id, :state_name)#.limit(100)
            @cities = GreenCardCity.where('sort_order < 1000').order("city_name asc").limit(100) #.pluck(:id, :city_name)
            @job_titles = GreenCardJobTitle.where('sort_order < 1000').order("sort_order asc") #.pluck(:id, :job_title_name)
            @industries = GreenCardIndustry.all.order('industry_name ASC').limit(500) #.pluck(:id, :industry_name)
            @companies = GreenCardCompany.all.order('company_name ASC').limit(500) #.pluck(:id, :company_name)
            @favorite_companies = FavoriteCompany.get_list(current_user)
            @companies_list = GreenCardCompany.all.order("company_name asc").limit(100) #.pluck(:id, :company_name)
            @birth_countries = BirthCountry.all.order("name asc").limit(500) #.pluck(:id, :name)
            @universities = University.where('sort_order < 1000').order("sort_order asc") #.pluck(:id, :name)
            response = { states: @states, industries: @industries, job_titles: @job_titles, companies: @companies, company_list: @company_list, birth_countries: @birth_countries, universities: @universities, cities: @cities }
        elsif params[:type].eql?('alumni')
            logger.info { "2" }
            @states = UniversityState.all.order("state_name asc") #.pluck(:id, :state_name)#.limit(100)
            @cities = UniversityCity.all.order("city_name asc").limit(100) #.pluck(:id, :city_name)
            @job_titles = UniversityJobTitle.all.order("sort_order asc") #.pluck(:id, :job_title_name)
            @industries = UniversityIndustry.all.order('industry_name ASC').limit(500) #.pluck(:id, :industry_name)
            @companies = UniversityCompany.all.order('company_name ASC').limit(500) #.pluck(:id, :company_name)
            @favorite_companies = FavoriteCompany.get_list(current_user)
            @companies_list = UniversityCompany.all.order("company_name asc").limit(100) #.pluck(:id, :company_name)
            @functions = EmpFunction.all.order(:name) #.pluck(:id, :name)
            @graduation_year = GraduationYear.all.order(:year) #.pluck(:id, :year)
            response = { states: @states, industries: @industries, job_titles: @job_titles, companies: @companies, company_list: @company_list, graduation_year: @graduation_year, functions: @functions, cities: @cities }
        else
            logger.info { "3" }
            @states = State.all.order("state_name asc") #.pluck(:id, :state_name)#.limit(100)
            @cities = City.where('sort_order < 1000').order("city_name asc") #.pluck(:id, :city_name)
            @job_titles = JobTitle.where('sort_order < 1000').order("sort_order asc") #.pluck(:id, :job_title_name)
            @industries = Industry.all.order('industry_name ASC').limit(500) #.pluck(:id, :industry_name)
            @companies = Company.where('sort_order < 1000').order('company_name ASC') #.limit(500) #.pluck(:id, :company_name)
            # @favorite_companies = FavoriteCompany.get_list(current_user)
            @companies_list = Company.where('sort_order < 1000').order("company_name asc") #.limit(100) #.pluck(:id, :company_name)
            response = { states: @states, industries: @industries, job_titles: @job_titles, companies: @companies, company_list: @company_list, cities: @cities }
        end
        @tab_type = params[:type]
        # response = { states: @states, industries: @industries, job_titles: @job_titles, companies: @companies, company_list: @company_list }
        respond_to do |format|
            format.js {  }
            # format.json {render json: response}
        end
    end

    def show
        if params[:tab_type] == "greencard"
				@company_details = GreenCardDetail.select("id, employer_id, employer_name, job_title, industry,wage_rate_of_pay_from, worksite_city, worksite_country, worksite_state, worksite_postal_code, website, crunchbase_link, wikipedia_link, indeed_link, linkedin_link,employees, company_description, company_id, industry_id, city_id, state_id, job_title_id, latitude,longitude,glassdoor_link, company_id ").find_by(:company_id => params[:id]).as_json(:only => GreenCardDetail::JSON_LIST,:current_user => current_user.id)
		elsif params[:tab_type] == "alumni"
				@company_details = UniversityDetail.select("id, employer_id, employer_name, job_title, industry, worksite_city, worksite_state, worksite_postal_code, website, crunchbase_link, wikipedia_link, indeed_link, linkedin_link,employees, company_description, company_id, industry_id, city_id, state_id, job_title_id, latitude,longitude,glassdoor_link, company_id ").find_by(:company_id => params[:id]).as_json(:only => UniversityDetail::JSON_LIST,:current_user => current_user.id)
		else
				@company_details = CompanyDetail.select("id, petition_id, employer_id, employer_name, headquaters, job_title, industry,wage_rate_of_pay_from, worksite_city, worksite_country, worksite_state, worksite_postal_code, website, crunchbase_link, wikipedia_link, indeed_link, linkedin_link,employees,no_of_petition, company_description, company_details.is_deleted, company_details.created_at, company_details.updated_at, company_id, industry_id, city_id, state_id, job_title_id, company_logo, revenue, company_ceo, news_link, latitude,longitude,glassdoor_link ").find_by(:company_id => params[:id]).as_json(:only => CompanyDetail::JSON_LIST,:current_user => current_user.id)
		end
        logger.info "===comp===#{@company_details.inspect}"
		# success_response(200,t('get_company_detail'),company_details)
    end

    def find_company(limit, offset, group_param, params)
        params[:year] = params[:year].present? ? params[:year] : ['2017']
        logger.info "===serach=====#{params.inspect}"
        query = "SELECT company_id,employer_name,avg(wage_rate_of_pay_from) as wage_rate_of_pay_from,count(a.id) as no_of_petition,latitude,longitude FROM company_details as a WHERE a.is_deleted =FALSE and latitude <> 0.0 and longitude <> 0.0"
        query_for_markers = "SELECT company_id,employer_name,avg(wage_rate_of_pay_from) as wage_rate_of_pay_from,count(a.id) as no_of_petition,latitude,longitude FROM company_details as a WHERE a.is_deleted =FALSE and latitude <> 0.0 and longitude <> 0.0"
        # logger.info "===query===#{query.inspect}=======#{CompanyDetail.find_by_sql(query)}"
        # logger.info { "======PARAMS=======#{query.inspect}" }
        if params[:search].present?
            logger.info { "======1=======" }
    		if params[:search][:industry_ids].blank? == false
                params[:search][:industry_ids] = params[:search][:industry_ids].join(', ')
    			query += " AND industry_id IN ("+ params[:search][:industry_ids] +")"
                query_for_markers += " AND industry_id IN ("+ params[:search][:industry_ids] +")"
    		end

    		if params[:search][:company_ids].blank? == false
                params[:search][:company_ids] = params[:search][:company_ids].join(', ')
    			query += " AND company_id IN ("+ params[:search][:company_ids] +")"
                query_for_markers += " AND company_id IN ("+ params[:search][:company_ids] +")"
    		end

    		if params[:search][:city_ids].blank? == false
    			query += " AND city_id IN ("+ params[:search][:city_ids].join(',') +")"
                query_for_markers += " AND city_id IN ("+ params[:search][:city_ids].join(',') +")"
    		end

    		if params[:search][:state_ids].blank? == false
    			query += " AND state_id IN ("+ params[:search][:state_ids].join(',') +")"
                query_for_markers += " AND state_id IN ("+ params[:search][:state_ids].join(',') +")"
    		end

    		if params[:search][:job_title_ids].blank? == false
    			query += " AND job_title_id IN ("+ params[:search][:job_title_ids].join(',') +")"
                query_for_markers += " AND job_title_id IN ("+ params[:search][:job_title_ids].join(',') +")"
    		end
        end
        if params[:year].blank? == false

            params[:year] = params[:year].join(', ')
            query += " AND year IN ("+ params[:year] +")"
            query_for_markers += " AND year IN ("+ params[:year] +")"
        end
        if params['search'].present?
            logger.info "===3==="
            if params['search']['industry_ids'].blank? == false
                # params['search']['industry_ids'] = params['search']['industry_ids'].join(', ')
    			query += " AND industry_id IN ("+ params['search']['industry_ids'] +")"
                query_for_markers += " AND industry_id IN ("+ params['search']['industry_ids'] +")"
    		end

    		if params['search']['company_ids'].blank? == false
                # params['search']['company_ids'] = params['search']['company_ids'].join(', ')
    			query += " AND company_id IN ("+ params['search']['company_ids'] +")"
                query_for_markers += " AND company_id IN ("+ params['search']['company_ids'] +")"
    		end

    		if params['search']['city_ids'].blank? == false
    			query += " AND city_id IN ("+ params['search']['city_ids'].join(',') +")"
                query_for_markers += " AND city_id IN ("+ params['search']['city_ids'].join(',') +")"
    		end

    		if params['search']['state_ids'].blank? == false
    			query += " AND state_id IN ("+ params['search']['state_ids'].join(',') +")"
                query_for_markers += " AND state_id IN ("+ params['search']['state_ids'].join(',') +")"
    		end

    		if params['search']['job_title_ids'].blank? == false
    			query += " AND job_title_id IN ("+ params['search']['job_title_ids'].join(',') +")"
                query_for_markers += " AND job_title_id IN ("+ params['search']['job_title_ids'].join(',') +")"
    		end
            logger.info "===4===#{query.inspect}"
        end
        if params['year'].blank? == false
            query += " AND year IN ("+ params['year'] +")"
            query_for_markers += " AND year IN ("+ params['year'] +")"
        end
        logger.info { "======2=======" }
        query_by_wage = []
        query_by_wage = query
        query += " GROUP BY "+group_param+" order by no_of_petition desc LIMIT "+ limit + " offset " + offset
        # query_by_wage += " GROUP BY "+group_param+" order by wage_rate_of_pay_from desc LIMIT "+ limit + " offset " + offset
        query_by_wage += " GROUP BY "+group_param+" order by no_of_petition desc LIMIT "+ limit + " offset " + offset
        logger.info { "======wage=======#{query_by_wage.inspect}" }
        # query += " order by no_of_petition"
		# query += " order by industry asc"
        # logger.info { "snfksdfjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkksn========query=======#{CompanyDetail.find_by_sql(query)}" }
        # logger.info { "peti=============#{CompanyDetail.find_by_sql(query)}" }
		response_petition = CompanyDetail.find_by_sql(query)
        response_wage = CompanyDetail.find_by_sql(query_by_wage)

        group_param = "latitude, longitude"
        query_for_markers += " GROUP BY "+group_param + " order by no_of_petition desc LIMIT "+limit
        logger.info "==mark ===#{query_for_markers.inspect}"
        # response_markers = CompanyDetail.find_by_sql(query_for_markers)

        # logger.info "==qweur===#{response_markers.count}"
        # logger.info "===mark==#{response_markers.inspect}"

        # logger.info { "snfksdfjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkksn1111111111" }
        @companies_petition = response_petition
        # logger.info "====res===#{response_petition.sort_by{ |x| x[:wage_rate_of_pay_from] }.reverse}"
        # @companies_wage = response_wage
        @companies_wage = response_petition.sort_by{ |x| x[:wage_rate_of_pay_from] }.reverse
        # logger.info { "=wage=============#{@companies_wage.inspect}" }
        # @markers = []
        # response_markers.each do |mark|
        #     # logger.info mark.inspect
        #     @markers << {lat: mark.latitude.to_f, lng: mark.longitude.to_f, petition: mark.no_of_petition.to_i}
        # end
        # @markers = @markers

        # logger.info "===mark==#{@markers.inspect}"
        # @markers = eval @markers.to_s.gsub('"', '').to_json
        # data = { companies_petition: response_petition, companies_wage: response_wage, markers: @markers }
        # return data
    end

    def fetch_map_data
        query_for_markers = "SELECT company_id,employer_name,avg(wage_rate_of_pay_from) as wage_rate_of_pay_from,count(a.id) as no_of_petition,latitude,longitude FROM company_details as a WHERE a.is_deleted =FALSE and latitude <> 0.0 and longitude <> 0.0"
        group_param = "latitude, longitude"
        if params[:search].present?
            logger.info { "======1=======" }
            if params[:search][:industry_ids].blank? == false
                params[:search][:industry_ids] = params[:search][:industry_ids].join(', ')
                # query += " AND industry_id IN ("+ params[:search][:industry_ids] +")"
                query_for_markers += " AND industry_id IN ("+ params[:search][:industry_ids] +")"
            end

            if params[:search][:company_ids].blank? == false
                params[:search][:company_ids] = params[:search][:company_ids].join(', ')
                # query += " AND company_id IN ("+ params[:search][:company_ids] +")"
                query_for_markers += " AND company_id IN ("+ params[:search][:company_ids] +")"
            end

            if params[:search][:city_ids].blank? == false
                # query += " AND city_id IN ("+ params[:search][:city_ids].join(',') +")"
                query_for_markers += " AND city_id IN ("+ params[:search][:city_ids].join(',') +")"
            end

            if params[:search][:state_ids].blank? == false
                # query += " AND state_id IN ("+ params[:search][:state_ids].join(',') +")"
                query_for_markers += " AND state_id IN ("+ params[:search][:state_ids].join(',') +")"
            end

            if params[:search][:job_title_ids].blank? == false
                # query += " AND job_title_id IN ("+ params[:search][:job_title_ids].join(',') +")"
                query_for_markers += " AND job_title_id IN ("+ params[:search][:job_title_ids].join(',') +")"
            end
        end
        query_for_markers += " GROUP BY "+group_param + " order by no_of_petition desc LIMIT 100"
        response_markers = CompanyDetail.find_by_sql(query_for_markers)
        @markers = []
        response_markers.each do |mark|
            # logger.info mark.inspect
            @markers << {lat: mark.latitude.to_f, lng: mark.longitude.to_f, petition: mark.no_of_petition.to_i}
        end
        logger.info "===mark==#{@markers.inspect}"
        data = {data: @markers}
        render json: data.as_json
    end

    def find_green_card(limit, offset, group_param, params)
        params[:year] = params[:year].present? ? params[:year] : '2017'
        query = "SELECT company_id,employer_name,avg(wage_rate_of_pay_from) as wage_rate_of_pay_from,count(a.id) as no_of_petition,latitude,longitude FROM green_card_details as a WHERE a.is_deleted =FALSE and latitude <> 0.0 and longitude <> 0.0"
        query_for_markers = "SELECT company_id,employer_name,avg(wage_rate_of_pay_from) as wage_rate_of_pay_from,count(a.id) as no_of_petition,latitude,longitude FROM green_card_details as a WHERE a.is_deleted =FALSE and latitude <> 0.0 and longitude <> 0.0"

		if params[:search][:industry_ids].blank? == false
			query += " AND industry_id IN ("+ params[:search][:industry_ids].join(',') +")"
            query_for_markers += " AND industry_id IN ("+ params[:search][:industry_ids].join(',') +")"
		end

		if params[:search][:company_ids].blank? == false
			query += " AND company_id IN ("+ params[:search][:company_ids].join(',') +")"
            query_for_markers += " AND industry_id IN ("+ params[:search][:company_ids].join(',') +")"
		end

		if params[:search][:city_ids].blank? == false
			query += " AND city_id IN ("+ params[:search][:city_ids].join(',') +")"
            query_for_markers += " AND city_id IN ("+ params[:search][:city_ids].join(',') +")"
		end

		if params[:search][:state_ids].blank? == false
			query += " AND state_id IN ("+ params[:search][:state_ids].join(',') +")"
            query_for_markers += " AND state_id IN ("+ params[:search][:state_ids].join(',') +")"
		end

		if params[:search][:job_title_ids].blank? == false
			query += " AND job_title_id IN ("+ params[:search][:job_title_ids].join(',') +")"
            query_for_markers += " AND state_id IN ("+ params[:search][:job_title_ids].join(',') +")"
		end

		if params[:search][:birth_country_ids].blank? == false
			query += " AND birth_country_id IN ("+ params[:search][:birth_country_ids].join(',') +")"
            query_for_markers += " AND state_id IN ("+ params[:search][:birth_country_ids].join(',') +")"
		end

		if params[:search][:university_ids].blank? == false
            query += " AND university_id IN ("+ params[:search][:university_ids].join(',') +")"
            query_for_markers += " AND state_id IN ("+ params[:search][:university_ids].join(',') +")"
		end

        if params[:year].blank? == false
            params[:year] = params[:year].join(', ')
            query += " AND year IN ("+ params[:year] +")"
            query_for_markers += " AND year IN ("+ params[:year] +")"
        end

        query_by_wage = []
        query_by_wage = query
		query += " GROUP BY "+group_param+" order by no_of_petition desc LIMIT "+ limit + " offset " + offset
        query_by_wage += " GROUP BY "+group_param+" order by no_of_petition desc LIMIT "+ limit + " offset " + offset

        response_petition = GreenCardDetail.find_by_sql(query)
        response_wage = GreenCardDetail.find_by_sql(query_by_wage)

        group_param = "latitude, longitude"
        query_for_markers += " GROUP BY "+group_param
        response_markers = GreenCardDetail.find_by_sql(query_for_markers)

        @companies_petition = response_petition
        # @companies_wage = response_wage
        @companies_wage = response_petition.sort_by{ |x| x[:wage_rate_of_pay_from] }.reverse
        @markers = []
        sum = 0
        sum = response_petition.map{ |x| sum = sum + x[:no_of_petition] }
        logger.info "===sim==#{sum}"
        # response_markers.each do |mark|
        #     # logger.info mark.inspect
        #     @markers << {lat: mark.latitude.to_f, lng: mark.longitude.to_f}
        # end
        response_petition.each do |mark|
            # logger.info mark.inspect
            @markers << {lat: mark.latitude.to_f, lng: mark.longitude.to_f, petition: mark.no_of_petition.to_i}
        end
        @markers = @markers
		# response_data = GreenCardDetail.find_by_sql(query).as_json(:only => [:company_id,:employer_name,:wage_rate_of_pay_from,:no_of_petition,:longitude,:latitude] ,:current_user => current_user.id,:cSponser => true)
        # logger.info "===res===#{response_data}"
    end


    def find_alumni(limit, offset, group_param, params)
        params[:year] = params[:year].present? ? params[:year] : '2017'
		query = "SELECT company_id,employer_name,count(a.id) as no_of_petition,latitude,longitude FROM university_details as a WHERE a.is_deleted =FALSE and latitude <> 0.0 and longitude <> 0.0"
        query_for_markers = "SELECT company_id,employer_name,count(a.id) as no_of_petition,latitude,longitude FROM university_details as a WHERE a.is_deleted =FALSE and latitude <> 0.0 and longitude <> 0.0"

		if params[:search][:industry_ids].blank? == false
			query += " AND industry_id IN ("+ params[:search][:industry_ids].join(',') +")"
            query_for_markers += " AND industry_id IN ("+ params[:search][:industry_ids].join(',') +")"
		end

		if params[:search][:company_ids].blank? == false
			query += " AND company_id IN ("+ params[:search][:company_ids].join(',') +")"
            query_for_markers += " AND company_id IN ("+ params[:search][:company_ids].join(',') +")"
		end

		if params[:search][:city_ids].blank? == false
			query += " AND city_id IN ("+ params[:search][:city_ids].join(',') +")"
            query_for_markers += " AND city_id IN ("+ params[:search][:city_ids].join(',') +")"
		end

		if params[:search][:state_ids].blank? == false
			query += " AND state_id IN ("+ params[:search][:state_ids].join(',') +")"
            query_for_markers += " AND state_id IN ("+ params[:search][:state_ids].join(',') +")"
		end

		if params[:search][:job_title_ids].blank? == false
            query += " AND job_title_id IN ("+ params[:search][:job_title_ids].join(',') +")"
            query_for_markers += " AND job_title_id IN ("+ params[:search][:job_title_ids].join(',') +")"
		end

		if params[:search][:graduation_year_ids].blank? == false
			query += " AND graduation_year_id IN ("+ params[:search][:graduation_year_ids].join(',') +")"
            query_for_markers += " AND graduation_year_id IN ("+ params[:search][:graduation_year_ids].join(',') +")"
		end

		if params[:search][:emp_function_ids].blank? == false
			query += " AND emp_function_id IN ("+ params[:search][:emp_function_ids].join(',') +")"
            query_for_markers += " AND emp_function_id IN ("+ params[:search][:emp_function_ids].join(',') +")"
		end


		# query += " GROUP BY "+group_param+" order by no_of_petition desc LIMIT "+ limit + " offset " + offset

		query += " GROUP BY "+group_param+" order by no_of_petition desc LIMIT "+ limit + " offset " + offset

        response_petition = UniversityDetail.find_by_sql(query)

        group_param = "latitude, longitude"
        query_for_markers += " GROUP BY "+group_param
        response_markers = UniversityDetail.find_by_sql(query_for_markers)

        @companies_petition = response_petition
        @markers = []
        response_markers.each do |mark|
            # logger.info mark.inspect
            @markers << {lat: mark.latitude.to_f, lng: mark.longitude.to_f, petition: mark.no_of_petition.to_i}
        end
        @markers = @markers

		# response_data = UniversityDetail.find_by_sql(query) #.as_json(:only => [:company_id,:employer_name,:no_of_petition,:longitude,:latitude] ,:current_user => current_user.id,:cSponser => true)
    end

    def add_company_as_favorite
        # company = FavoriteCompany.new(favorite_company_params)
        # company.user_id = current_user.id
        # if params[:favorite_company][:company_id] != "0"
        #     company.company_name =  Company.find_by(:id => params[:favorite_company][:company_id]).company_name
        # end

        begin
            # params[:tab_type] = params[:tab_type].blank? ? 'h1b' : params[:tab_type]
            # fav_company = FavoriteCompany.where(user_id: current_user.id, tab_type: params[:tab_type], company_id: params[:company_id])
            @fav_company = FavoriteCompany.where(user_id: current_user.id, company_id: params[:company_id])
            logger.info "===compfav==#{@fav_company.inspect}"
            @companies_list = Company.all.order("company_name asc").limit(100)
            if params[:like].eql?('like')
                if @fav_company.present?
                    logger.info "===1==="
                    @fav_company.destroy_all
                end
                logger.info "===2==="
                params[:user_id] = current_user.id
                @fav_company = FavoriteCompany.new(favorite_company_params)
                logger.info "===facv===#{@fav_company.inspect}"
                @fav_company.save
                @favorite_companies = FavoriteCompany.get_list(current_user)

                # flash[:notice] = 'Company added as favorite'
                respond_to do |format|
                    format.json { render json: { success: true } }
                    format.js {  }
                end
                # render json: { success: true, message: 'Company added as favorite' }
            else
                # @companies_list = Company.all.order("company_name asc").limit(100)
                if @fav_company.present?
                    logger.info "===3==="
                    @fav_company.destroy_all
                    @favorite_companies = FavoriteCompany.get_list(current_user)

                    # flash[:notice] = 'Company removed from favorite'
                    respond_to do |format|
                        format.json { render json: { success: true } }
                        format.js {  }
                    end
                end
            end
            #redirect_to request.referer
        rescue Exception => e
            logger.info "====error===#{e.message}"
        end
    end

    def sponsored_petitions
        begin
            if params[:tab_type] == "greencard"
    			company_details = GreenCardDetail.select("job_title,avg(replace(replace(wage_rate_of_pay_from,'$',''),',','')) as wage_rate_of_pay_from, count(green_card_details.id) as no_of_petition, employer_name, employees, company_id, industry").where(:company_id => params[:company_id]).group("job_title")
                # @company = GreenCardCompany.find_by(id: params[:company_id])
    		elsif params[:tab_type] == "alumni"
    			company_details = UniversityDetail.select("job_title,0 as wage_rate_of_pay_from, count(university_details.id) as no_of_petition, employer_name, employees, company_id, industry").where(:company_id => params[:company_id]).group("job_title")
                # @company = UniversityCompany.find_by(id: params[:company_id])
    		else
    			company_details = CompanyDetail.select("job_title,avg(replace(replace(wage_rate_of_pay_from,'$',''),',','')) as wage_rate_of_pay_from, count(company_details.id) as no_of_petition, employer_name, employees, company_id, industry").where(:company_id => params[:company_id]).group("job_title")
                # @company = Company.find_by(id: params[:company_id])
    		end
            # @petition_details = company_details.order('no_of_petition DESC').as_json(:only => [:job_title,:wage_rate_of_pay_from,:no_of_petition,:employer_name, :employees, :company_id, :industry, :is_favorite],:cSponser => true)
            # @average_details = company_details.order('wage_rate_of_pay_from DESC').as_json(:only => [:job_title,:wage_rate_of_pay_from,:no_of_petition,:employer_name, :employees, :company_id, :industry, :is_favorite],:cSponser => true)
            company = CompanyDetail.find_by(company_id: params[:company_id])
            logger.info "===comp===#{company.inspect}"
            params[:tab_type] = params[:tab_type].blank? ? 'h1b' : params[:tab_type]
            @petition_details = company_details.order('no_of_petition DESC').as_json(:only => CompanyDetail::JSON_LIST,:current_user => current_user.id,:cSponser => true)
            @average_details = company_details.order('wage_rate_of_pay_from DESC').as_json(:only => CompanyDetail::JSON_LIST,:current_user => current_user.id,:cSponser => true)
            # @company_logo = Rails.application.secrets.BASE_URL_IMAGE + "/assets/CompanyLogos/"+company.employer_id.to_s+".png"
            @company_logo = File.exist?(Rails.root + "public/assets/CompanyLogos/#{company.employer_id}.png") ? Rails.application.secrets.BASE_URL_IMAGE + "/assets/CompanyLogos/"+company.employer_id.to_s+".png" : Rails.application.secrets.BASE_URL_IMAGE + "/assets/company_logo.png"
            # @company_logo = File.exist?(Rails.root + "public/assets/CompanyLogos/#{self.employer_id}.png") ? Rails.application.secrets.BASE_URL_IMAGE + "/assets/CompanyLogos/"+self.employer_id.to_s+".png" : Rails.application.secrets.BASE_URL_IMAGE + "/assets/company_logo.png"
            logger.info "===compnay-===#{@petition_details.inspect}"
        rescue Exception => e
            logger.info "====error===#{e.message}"
        end
    end

    def search_industry
        if params[:search].present? && !params[:search].blank?
            @industries = Industry.where('industry_name like ?', "%#{params[:search]}%").order('industry_name ASC')
        else
            @industries = Industry.all.order('industry_name ASC').limit(500)
        end
    end

    def search_company
        if params[:search].present? && !params[:search].blank?
            @companies = Company.where('company_name like ?', "%#{params[:search]}%").order('company_name ASC')
        else
            @companies = Company.all.order('company_name ASC').limit(500)
        end
    end

    def company_select2
        if params[:term][:term].present? && !params[:term][:term].blank?
            if params[:tab_type].present?
                if params[:tab_type].eql?('greencard')
                    @companies = GreenCardCompany.where('company_name like ?', "%#{params[:term][:term]}%").order('company_name ASC')
                elsif params[:tab_type].eql?('alumni')
                    @companies = UniversityCompany.where('company_name like ?', "%#{params[:term][:term]}%").order('company_name ASC')
                else
                    @companies = Company.where('company_name like ?', "%#{params[:term][:term]}%").order('sort_order ASC')
                end
            else
                @companies = Company.where('company_name like ?', "%#{params[:term][:term]}%").order('sort_order ASC')
            end
        else
            if params[:tab_type].present?
                if params[:tab_type].eql?('greencard')
                    @companies = GreenCardCompany.where('sort_order < 1000').order('company_name ASC')
                elsif params[:tab_type].eql?('alumni')
                    @companies = UniversityCompany.where('sort_order < 1000').order('company_name ASC')
                else
                    @companies = Company.where('sort_order < 1000').order('sort_order ASC')
                end
            else
                @companies = Company.where('sort_order < 1000').order('sort_order ASC').limit(500)
            end
        end
        render json: @companies
    end

    def industry_select2
        if params[:term][:term].present? && !params[:term][:term].blank?
            logger.info "===1=="
            if params[:tab_type].present?
                logger.info "===2==#{params[:tab_type]}"
                if params[:tab_type].eql?('greencard')
                    logger.info "===3=="
                    @industries = GreenCardIndustry.where('industry_name like ?', "%#{params[:term][:term]}%").order('industry_name ASC')
                elsif params[:tab_type].eql?('alumni')
                    logger.info "===4=="
                    @industries = UniversityIndustry.where('industry_name like ?', "%#{params[:term][:term]}%").order('industry_name ASC')
                else
                    logger.info "===5=="
                    @industries = Industry.where('industry_name like ?', "%#{params[:term][:term]}%").order('sort_order ASC')
                end
            else
                logger.info "===6=="
                @industries = Industry.where('industry_name like ?', "%#{params[:term][:term]}%").order('sort_order ASC')
            end
        else
            if params[:tab_type].present?
                logger.info "===7==#{params[:tab_type]}"
                if params[:tab_type].eql?('greencard')
                    logger.info "===8=="
                    @industries = GreenCardIndustry.all.order('industry_name ASC')
                elsif params[:tab_type].eql?('alumni')
                    logger.info "===9=="
                    @industries = UniversityIndustry.all.order('industry_name ASC').limit(500)
                else
                    @industries = Industry.where('sort_order < 1000').order('sort_order ASC').limit(500)
                end
            else
                @industries = Industry.where('sort_order < 1000').order('sort_order ASC').limit(500)
            end
        end
        render json: @industries
    end

    def load_default_companies
        logger.info "======default=========sess======#{session[:companies_search].inspect}"
        group_param = params[:is_show_map] == "true" ? " latitude,longitude " : "company_id"
        limit = "10"
        offset = "0"
        result = session[:companies_search].present? ? find_company(limit, offset, group_param, session[:companies_search]) : find_company(limit, offset, group_param, {})
        return
    end

    def university_select2
        if params[:term][:term].present? && !params[:term][:term].blank?
            @universities = University.where('name like ?', "%#{params[:term][:term]}%") #.order("sort_order asc")
        else
            @universities = University.where('sort_order < 1000').order("sort_order asc")
        end
        render json: @universities
    end

    def birth_country_select2
        if params[:term][:term].present? && !params[:term][:term].blank?
            @birth_countries = BirthCountry.where('name like ?', "%#{params[:term][:term]}%").order("name asc")
        else
            @birth_countries = BirthCountry.all.order("name asc").limit(500)
        end
        render json: @birth_countries
    end

    def job_title_select2
        if params[:term][:term].present? && !params[:term][:term].blank?
            logger.info "===1=="
            if params[:tab_type].present?
                logger.info "===2==#{params[:tab_type]}"
                if params[:tab_type].eql?('greencard')
                    logger.info "===3=="
                    @job_titles = GreenCardJobTitle.where('job_title_name like ?', "%#{params[:term][:term]}%").order('job_title_name ASC')
                elsif params[:tab_type].eql?('alumni')
                    logger.info "===4=="
                    @job_titles = UniversityJobTitle.where('job_title_name like ?', "%#{params[:term][:term]}%").order('job_title_name ASC')
                else
                    logger.info "===5=="
                    @job_titles = JobTitle.where('job_title_name like ?', "%#{params[:term][:term]}%").order('job_title_name ASC')
                end
            else
                logger.info "===6=="
                @job_titles = JobTitle.where('job_title_name like ?', "%#{params[:term][:term]}%").order('sort_order ASC')
            end
        else
            if params[:tab_type].present?
                logger.info "===7==#{params[:tab_type]}"
                if params[:tab_type].eql?('greencard')
                    logger.info "===8=="
                    @job_titles = GreenCardJobTitle.where('sort_order < 1000').order("sort_order asc") #.pluck(:id, :job_title_name)
                elsif params[:tab_type].eql?('alumni')
                    logger.info "===9=="
                    @job_titles = UniversityJobTitle.where('sort_order < 1000').order("job_title_name asc") #.pluck(:id, :job_title_name)
                else
                    @job_titles = JobTitle.where('sort_order < 1000').order("sort_order asc") #.pluck(:id, :job_title_name)
                end
            else
                @job_titles = JobTitle.where('sort_order < 1000').order("job_title_name asc") #.pluck(:id, :job_title_name)
            end
        end
        render json: @job_titles
    end

    def state_select2
        if params[:term][:term].present? && !params[:term][:term].blank?
            logger.info "===1=="
            if params[:tab_type].present?
                logger.info "===2==#{params[:tab_type]}"
                if params[:tab_type].eql?('greencard')
                    logger.info "===3=="
                    @states = GreenCardState.where('state_name like ?', "%#{params[:term][:term]}%").order("state_name asc") #.pluck(:id, :state_name)#.limit(100)
                elsif params[:tab_type].eql?('alumni')
                    logger.info "===4=="
                    @states = UniversityState.where('state_name like ?', "%#{params[:term][:term]}%").order("state_name asc")
                else
                    logger.info "===5=="
                    @states = State.where('state_name like ?', "%#{params[:term][:term]}%").order("state_name asc")
                end
            else
                logger.info "===6=="
                @states = State.where('state_name like ?', "%#{params[:term][:term]}%").order("state_name asc")
            end
        else
            if params[:tab_type].present?
                logger.info "===7==#{params[:tab_type]}"
                if params[:tab_type].eql?('greencard')
                    logger.info "===8=="
                    @states = GreenCardState.all.order("state_name asc") #.pluck(:id, :state_name)#.limit(100)
                elsif params[:tab_type].eql?('alumni')
                    logger.info "===9=="
                    @states = UniversityState.all.order("state_name asc") #.pluck(:id, :state_name)#.limit(100)
                else
                    @states = State.all.order("state_name asc") #.pluck(:id, :state_name)#.limit(100)
                end
            else
                @states = State.all.order("state_name asc") #.pluck(:id, :state_name)#.limit(100)
            end
        end
        render json: @states
    end

    def city_select2
        if params[:term][:term].present? && !params[:term][:term].blank?
            logger.info "===1=="
            if params[:tab_type].present?
                logger.info "===2==#{params[:tab_type]}"
                if params[:tab_type].eql?('greencard')
                    logger.info "===3=="
                    if params[:state_ids].present? && params[:state_ids] != 'null'
                        @cities = GreenCardCity.where('city_name like ? and state_id in (?)', "%#{params[:term][:term]}%", params[:state_ids]).order("city_name asc").limit(100)
                    else
                        @cities = GreenCardCity.where('city_name like ?', "%#{params[:term][:term]}%").order("city_name asc").limit(100) #.pluck(:id, :city_name)
                    end
                elsif params[:tab_type].eql?('alumni')
                    logger.info "===4=="
                    if params[:state_ids].present? && params[:state_ids] != 'null'
                        @cities = UniversityCity.where('city_name like ? and state_id in (?)', "%#{params[:term][:term]}%", params[:state_ids]).order("city_name asc").limit(100)
                    else
                        @cities = UniversityCity.where('city_name like ?', "%#{params[:term][:term]}%").order("city_name asc").limit(100) #.pluck(:id, :city_name)
                    end
                else
                    logger.info "===5=="
                    if params[:state_ids].present? && params[:state_ids] != 'null'
                        @cities = City.where('city_name like ? and state_id in (?)', "%#{params[:term][:term]}%", params[:state_ids]).order("city_name asc").limit(100)
                    else
                        @cities = City.where('city_name like ?', "%#{params[:term][:term]}%").order("city_name asc").limit(100) #.pluck(:id, :city_name)
                    end
                end
            else
                logger.info "===6=="
                if params[:state_ids].present? && params[:state_ids] != 'null'
                    @cities = City.where('city_name like ? and state_id in (?)', "%#{params[:term][:term]}%", params[:state_ids]).order("city_name asc").limit(100)
                else
                    @cities = City.where('city_name like ?', "%#{params[:term][:term]}%").order("city_name asc").limit(100) #.pluck(:id, :city_name)
                end
            end
        else
            if params[:tab_type].present?
                logger.info "===7==#{params[:tab_type]}"
                if params[:tab_type].eql?('greencard')
                    logger.info "===8=="
                    if params[:state_ids].present? && params[:state_ids] != 'null'
                        @cities = GreenCardCity.where('sort_order < 1000 and state_id in (?)', params[:state_ids]).order("city_name asc").limit(100)
                    else
                        @cities = GreenCardCity.where('sort_order < 1000').order("city_name asc").limit(100) #.pluck(:id, :city_name)
                    end
                elsif params[:tab_type].eql?('alumni')
                    logger.info "===9=="
                    if params[:state_ids].present? && params[:state_ids] != 'null'
                        @cities = UniversityCity.where('sort_order < 1000 and state_id in (?)', params[:state_ids]).order("city_name asc").limit(100)
                    else
                        @cities = UniversityCity.all.order("city_name asc").limit(100) #.pluck(:id, :city_name)
                    end
                else
                    if params[:state_ids].present? && params[:state_ids] != 'null'
                        @cities = City.where('sort_order < 1000 and state_id in (?)', params[:state_ids]).order("city_name asc").limit(100)
                    else
                        @cities = City.where('sort_order < 1000').order("city_name asc") #.pluck(:id, :city_name)
                    end
                end
            else
                if params[:state_ids].present? && params[:state_ids] != 'null'
                    @cities = City.where('sort_order < 1000 and state_id in (?)', params[:state_ids]).order("city_name asc").limit(100)
                else
                    @cities = City.where('sort_order < 1000').order("city_name asc") #.pluck(:id, :city_name)
                end
            end
        end
        render json: @cities
    end

    private
        def favorite_company_params
    		params.permit(:company_id,:company_name, :tab_type, :user_id)
    	end

        def search_params
            params.require(:search).permit(:type, :areatype, year: [], company_ids: [], industry_ids: [], state_ids: [], city_ids: [], birth_country_ids: [], university_ids: [], graduation_year_ids: [], emp_function_ids: [], job_title_ids: [])
        end

end
