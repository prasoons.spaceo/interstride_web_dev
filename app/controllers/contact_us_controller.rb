class ContactUsController < ApplicationController
    def new
        @contact_us = ContactUs.new
    end

    def create
        ActiveRecord::Base.transaction do
			begin
				if params[:contact_us]
					contact = ContactUs.new(contact_us_params)
					contact.user_id = current_user.id
			        if !contact.save
                        flash[:error] = t('something_went_wrong')
                        redirect_to request.referer
                        return
				    end

				    ##Send Details to Admin email address
				    UserMailer.contact_us(contact).deliver_now
                    flash[:notice] = t('contact_created')
                    redirect_to authenticated_root_path
				    # success_response(200,t('contact_created'),contact.as_json(only: ContactUs::JSON_LIST))
				    # contact = nil
				    return
				else
                    flash[:error] = t('something_went_wrong')
                    redirect_to request.referer
                    return
				end
			rescue Exception => e
				Rails.logger.info "v1=======create(ContactUsController) errors=========#{e}"
				raise ActiveRecord::Rollback
                flash[:error] = t('something_went_wrong')
                redirect_to request.referer
				return
			end
		end
    end

    private

    def contact_us_params
        params.require(:contact_us).permit(:topic,:comment)
    end
end
