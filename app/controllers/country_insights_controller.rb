class CountryInsightsController < ApplicationController
    include ActionView::Helpers::NumberHelper
    require 'nokogiri'
    require "net/http"

    def index
        # @countries = CountryGuide.joins(" left join apps_countries as a on a.country_name=country_guides.country_name").select("LOWER(country_code) as country_code,country_guides.*") #.where("country_guides.country_name LIKE ?",wildcard_search )
        @countries = Country.joins('left join country_guides as a on a.country_name = countries.name').order('name ASC') #.limit(100)
        # resp = Net::HTTP.get_response(URI.parse('https://restcountries.eu/rest/v2/all'))
        # data = JSON.parse(resp.body)
        # @countries = data.map{ |x| {name:  x['name'], flag: x['flag']}}
        get_country_info(@countries.first.id)
        logger.info "=======#{@countries.inspect}=========="
    end

    def show
        get_country_info(params[:id])
        respond_to do |format|
            format.js
        end
    end

    def get_country_info(id)
        @country = Country.find(id)
        @country_guide = CountryGuide.joins("left join apps_countries as a on a.country_name=country_guides.country_name").where("country_guides.country_name = ?", @country.name).select("LOWER(country_code) as country_code,country_guides.*")[0]
        logger.info "==guide===#{@country_guide.inspect}"
        @is_indeed_company = IndeedCountry.find_by_name(@country.name).present?
        if @country_guide.cia_link.present? && @country_guide.cia_link != 'NA'
            logger.info "====in===="
            object = Nokogiri::HTML(open(@country_guide.cia_link))
            @overview = object.css('.expandcollapse').css('li')[9].css('.category_data')[0].text + "<br />" + object.css('.expandcollapse').css('li')[9].css('.category_data')[1].text + "<br />" + object.css('.expandcollapse').css('li')[9].css('.category_data')[2].text + "<br />" + object.css('.expandcollapse').css('li')[9].css('.category_data')[3].text + "<br />" + object.css('.expandcollapse').css('li')[9].css('.category_data')[4].text + "<br />" + object.css('.expandcollapse').css('li')[9].css('.category_data')[5].text

            uri = URI("http://api.worldbank.org/v2/countries/#{@country_guide.country_code}/indicators/SP.POP.TOTL?format=json")
            res = Net::HTTP.get(uri)
            res = JSON.parse(res)
            logger.info { "re------------#{res[0]['page']}" }
            @population = res[0]['page'] > 0 ? res[1][0]['value'].present? ? number_to_human(res[1][0]['value'], units: {million: 'M'}) : res[1][1]['value'].present? ? number_to_human(res[1][1]['value'], units: {million: 'M'}) : (res[2][0]['value'].present? && !res[2][0]['value'].nil?) ? number_to_human(res[2][0]['value'], units: {million: 'M'}) : '0' :'0'
            #@population = res.present? ? res[1].present? ? number_to_human(res[1][1]['value'], units: {million: 'M'}) : number_to_human(res[2][0]['value'], units: {million: 'M'}) : ''

            uri = URI("http://api.worldbank.org/v2/countries/#{@country_guide.country_code}/indicators/SL.UEM.TOTL.ZS?format=json")
            res = Net::HTTP.get(uri)
            res = JSON.parse(res)
            @unemployment = res[0]['page'] > 0 ? (res[1][0]['value'].present? && !res[1][0]['value'].nil?) ? number_to_human(res[1][0]['value'].round(2)) : (res[1][1].present? && !res[1][1].nil?) ? res[1][1]['value'] : (res[2][0].present? && !res[2][0]['value'].nil?)  ? res[2][0]['value'] : '0.0' : '0.0'
            #@unemployment = number_to_human(res[1][0]['value'].round(2))

            uri = URI("http://api.worldbank.org/v2/countries/#{@country_guide.country_code}/indicators/NY.GDP.MKTP.KD.ZG?format=json")
            res = Net::HTTP.get(uri)
            res = JSON.parse(res)
            @gdp_growth = res[0]['page'] > 0 ? !res[1][0]['value'].nil? ? number_to_human(res[1][0]['value'].round(2)) : (res[1][1].present? && !res[1][1]['value'].nil?) ? number_to_human(res[1][1]['value'].round(2)) : '0.0' : '0.0'
            #@gdp_growth = number_to_human(res[1][1]['value'].round(2))

            uri = URI("http://api.worldbank.org/v2/countries/#{@country_guide.country_code}/indicators/NY.GDP.PCAP.CD?format=json")
            res = Net::HTTP.get(uri)
            res = JSON.parse(res)
            # logger.info "===res-==#{res[1][1]['value'].round(2)}"
            @gdp_per_cap = res[0]['page'] > 0 ? !res[1][0]['value'].nil? ? number_to_human(res[1][0]['value'].round(2), units: {million: 'M', thousand: 'K'}) : (res[1][0].present? && !res[1][0]['value'].nil?) ? number_to_human(res[1][1]['value'].round(0), units: {million: 'M', thousand: 'K'}) : '0.0' : '0.0'
            #@gdp_per_cap = number_to_human(res[1][1]['value'].round(0), units: {million: 'M', thousand: 'K'})


            #+ object.css('.expandcollapse').css('li')[9].css('.category_data')[1] + object.css('.expandcollapse').css('li')[9].css('.category_data')[2] + object.css('.expandcollapse').css('li')[9].css('.category_data')[3] + object.css('.expandcollapse').css('li')[9].css('.category_data')[4] + object.css('.expandcollapse').css('li')[9].css('.category_data')[5]
        else
            logger.info "=====out-=="
            @overview = 'No description found'
            @population = 0.0
            @unemployment = 0.0
            @gdp_per_cap = 0.0
            @gdp_growth = 0.0
        end
    end

    def search
        if params[:search].present? && !params[:search].blank?
            @countries = Country.joins('left join country_guides as a on a.country_name = countries.name').where('countries.name like ?', "%#{params[:search]}%").order('countries.name')
        else
            @countries = Country.joins('left join country_guides as a on a.country_name = countries.name').order('name ASC').limit(100)
        end
    end
end
