class FavoriteCompaniesController < ApplicationController
    def create
        ActiveRecord::Base.transaction do
            begin
                params[:favorite_company][:company_name] = params[:favorite_company][:company_id]
                company = FavoriteCompany.new(favorite_company_params)
                logger.info "==com=-=#{company.inspect}"
				company.user_id = current_user.id
				# if params[:favorite_company][:company_id] != "0"
				# 	#company.company_name =  CompanyDetail.find_by(:company_id => params[:favorite_company][:company_id]).employer_name
				# 	# company.company_name =  Company.find_by(:id => params[:favorite_company][:company_id]).company_name
                    company.company_id =  Company.find_by(:company_name => params[:favorite_company][:company_id]).id
				# end
				company.save
                flash[:notice] = 'Company added as favorite'
                redirect_to request.referer
            rescue Exception => e
                Rails.logger.info "v1=======create(FavoritePostsController) errors=========#{e}"
                raise ActiveRecord::Rollback
                flash[:error] = t('something_went_wrong')
                # error_response(400, t('something_went_wrong'))
                return
            end
        end
    end

    private
    def favorite_company_params
        params.require(:favorite_company).permit(:id, :company_name, :company_id, :is_deleted )
    end
end
