class FeedbacksController < ApplicationController

    def new
        @feedback = SendFeedback.new
    end

    def create
        ActiveRecord::Base.transaction do
			begin
				if params[:send_feedback]
                    feedback = SendFeedback.new(send_feedback_params)
                    if params[:send_feedback][:rate].present?
    					feedback.user_id = current_user.id
    			        if !feedback.save
                            logger.info "===err===#{feedback.error.full_messages}"
                            flash[:error] = t('something_went_wrong')
                            redirect_to request.referer
                            return
    				        # error_response(400,t('something_went_wrong'))
    				        # return
    				    end
                        flash[:notice] = t('feedback_created')
                        redirect_to authenticated_root_path
    				    # success_response(200,t('feedback_created'),feedback.as_json(only: SendFeedback::JSON_LIST))
    				    # feedback = nil
    				    return
                    else
                        feedback.errors.add(:rate, 'is required')
                        flash[:error] = feedback.errors.full_messages.join(', ')
                        redirect_to request.referer
                        return
                    end
				else
                    flash[:error] = t('something_went_wrong')
                    redirect_to request.referer
                    return
				end
			rescue Exception => e
				Rails.logger.info "v1=======create(SendFeedbacksController) errors=========#{e}"
				raise ActiveRecord::Rollback
                flash[:error] = t('something_went_wrong')
                redirect_to request.referer
                return
			end
		end
    end

    protected

	def send_feedback_params
      params.require(:send_feedback).permit(:rate, :comment)
    end
end
