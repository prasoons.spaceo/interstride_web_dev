class GameplanController < ApplicationController
    def index
        begin
            user_overview = UserAnswer.get_user_answers(params,current_user).to_json(:only => [:category_id,:category_name,:question_points,:answer_points,:total,:response])
			if JSON.parse(user_overview).present? && !JSON.parse(user_overview).blank?
				response_data = JSON.parse(user_overview).map{ |p|
					marks = ( p["question_points"] * p["answer_points"])/p["total"]
					percentage = (marks * 100)/ p["question_points"]
					# response_arr = p["response"].split("@@")
					if p["category_id"] == 4
						response_arr = ["Employers list “lack of cultural fit” as one of the top reasons for not recruiting international students. International students acquire their own unique cultural traits during their upbringing at home. However, these traits might differ significantly from those in the U.S. To show that you can adapt to a U.S. based workplace, a thorough understanding of the American professional culture and etiquette is important. ","A great way to demonstrate cultural fit to employers is through successful past employment in the U.S. or employment at an American company abroad. Prior exposure and understanding of the American professional culture can be a strong asset on your resume for employment opportunities in the U.S.  Another way to show cultural adaptability to a potential employer is through a history of experiences in international or cross-cultural environments. Any study abroad experiences or international work assignments can serve well for this purpose. ","Cultural understanding is not just limited to national culture. Companies also look for people who can fit the organization’s culture. Every company has its own unique culture and characteristics, and it is important to understand this “company culture” before interviewing for a position in a company. In addition to research over the internet, informational interviews are the best way to understand the culture of a company. Informational interviews are also a good time to show that you are socially comfortable in new settings and able to adapt to work in conditions that are different from your home country."]
					elsif p["category_id"] == 1
						response_arr = ["Based on historic data, employers are more likely to sponsor employment visas for certain occupations over others. Pursuing a degree that is compatible with occupations that have strong visa sponsorship history can increase your chances of finding employment in the U.S. Make sure that you understand the demand for your degree and have reviewed the historic trends on visa sponsorships for your desired job.","Although GPA is an important factor, it is one of many that will determine recruiting success. Therefore, international students should not rely entirely on a high GPA for job placement. Many professional roles also require past leadership experience. Aim to have at least one leadership role during college, which you can highlight on your resume and speak about during the interview process. Leading an initiative, organization, or a team not only makes you stand out among other candidates, but also provides an opportunity to show that you are passionate about certain causes or issues. Leading a sports club, volunteering for a cause, or proactively participating in a professional or cultural organization can go a long way in the job application process.","Through participation in extracurricular activities, students show enthusiasm and ability to lead. Also, as an international student, extracurricular activities can be a great way to build your professional and social network in the U.S."]
					elsif p["category_id"] == 2
						response_arr = ["With ever increasing competition, employers focus on hiring candidates that have pre-existing knowledge about the industry, are committed to a career in their space, and are easy to train. Training a candidate is expensive, and more importantly, requires a significant time commitment from the employer. Therefore, most employers are likely to hire students that have pursued degrees relevant to their desired career.","Students with past professional experience are much more attractive to employers than those without. It can be challenging to find paid positions as you begin to build your U.S. employment history. In case of difficulty in finding paid positions, consider pursuing unpaid internships to strengthen your profile by adding U.S. employers to your resume. Even a short unpaid internship in the field of your choice can be a great way to enhance your resume.","As an international student, you either need a work authorization or an employment visa to start an internship or a job in the U.S. It is important to familiarize yourself with the various work visas and to understand the implications of each visa category. The allowed duration of your employment depends on the type of work authorization you receive from the government so it is important that you understand the rules associated with each visa category. The employment visa process can be lengthy, challenging, and time consuming so make sure you start the process as early as possible."]
					elsif p["category_id"] == 3
						response_arr = []
					elsif p["category_id"] == 5
						response_arr = ["One of the top reasons cited by employers for not hiring international students is the inability to communicate well in English. From resume reviews to in-person interviews, an applicant's communication skills are tested at every stage of the recruiting process. Increasing the chances of finding employment in the U.S. depends a great deal on honing your English language skills. Not only is it vital to read, write, and speak English well, but it is also important that others are able to understand you clearly. Strong accents can be a hindrance for some students. Enrolling in an accent reduction class or workshop can address this challenge.","The ability to speak and write in multiple languages can be a strong asset in helping an international student stand out among a pool of other qualified applicants. Highlight your language skills through your resume, LinkedIn profile, and interactions with the employer.","The top two avenues of finding employment for international students are networking events and career fairs. Both require eloquent and effective use of communication skills. Not only is attendance of on-campus career fairs and networking events important, but also knowing exactly what to do and how to communicate with employers at such events can open doors to many new opportunities. International students should be proactive in approaching recruiters and initiating conversations with hiring managers at networking events and career fairs. After approaching, try to keep the conversations short, request business cards, and follow-up after the event for a one-on-one informational interview request."]
					else p["category_id"] == 6
						response_arr = ["Familiarizing yourself with all the essential resources and channels to initiate your recruiting process is key to finding and securing employment opportunities in the U.S. Knowledge of all the tools and services offered by your campus career center should be the first step in your job search process. Take advantage of resume review sessions, mock interviews, and networking and career events offered by your career center.","Because of visa restrictions, international students have a limited time to find a job. Your ability to create professional networks at school maximizes your chance of recruiting success. Such networks can provide valuable insight and information related to employment opportunities. To do so, it is essential to maximize your interactions with classmates and friends from both U.S. and other international countries. Start building your network as soon as you start your education program in the U.S.","Professional networks can significantly enhance the chances of finding employment. Signing up to online networks such as LinkedIn is essential to build and grow your network. In addition to online networks, actively participating in both on-campus professional clubs, as well as off-campus organizations and industry conferences is highly encouraged.","Attending networking events and career fairs is the best way to get your foot in the door with prospective employers. Most recruiters who attend campus events are actively hiring students from colleges and universities. Attend as many of these events as possible. During the events, try to keep the conversations short, collect business cards, and follow-up with an informational interview request.","Informational interviews are important because they show prospective employers that you are serious about finding a job in their industry and company. Through informational interviews you can also learn more about the companies as well as the employment and visa sponsorship opportunities at these companies. Informational interviews can be great practice for professional conversations and job interviews.","The most important element of the job application process is the face to face interview. Just like informational interviews help to learn more about the company and the culture, mock help to better prepare for the hiring interview. Employing a combination of both during the job application process can strengthen your candidacy significantly."]
					end
					{
						category_id: p["category_id"],
						category_name: p["category_name"],
						percentage: percentage,
						response: response_arr
					}
                }
                @data = response_data
    				# success_response(200,t('gameplan_overview'),response_data)
    			user_overview = nil
    			response_data = nil
			else
                @data = []
                @user_answer = UserAnswer.new
                redirect_to take_test_gameplan_index_path
				# error_response(200,t('not_appeared_for_test'))
			end
        rescue Exception => e
        end

			# return
    end

    def take_test
        @degree = current_user.degree_level.blank? == false ? current_user.degree_level : "graduate"
        if @degree == "undergraduate"
			degree_arr = "'all'"+ "," +"'undergraduate'"
		elsif @degree == "U.S."
			degree_arr = "'all'"+ "," +"'U.S.'"
		elsif @degree == "Foster"
			degree_arr = "'all'"+ "," +"'Foster'"
		else
			degree_arr = "'all'"+ "," +"'graduate'"
		end

		@questions = GameplanQuestion.where("degree IN ("+degree_arr.to_s+")").as_json(:only => GameplanQuestion::JSON_LIST)
        logger.info "===questions===#{@questions.inspect}"
    end

    def user_answers
        begin
            # logger.info "==params===#{params.inspect}=======}"
            params[:gameplan_question][:user_answers].each do |ans|
                logger.info "ans=======#{ans.inspect}"
                logger.info "ans=======#{ans[1][:answer_id]} ==#{ans[1][:answer_id].class}"
                # if !ans[1][:answer_id].class.eql?(String)
                #     if ans[1][:answer_id].class.eql?(Array)
                #         params[:gameplan_question][:user_answers][ans[0]][:answer_id] = ans[1][:answer_id].join(',')
                #     else
                #         ans[1][:answer_id].each do |key,val|
                #             logger.info "===par===#{params[:gameplan_question][:user_answers][ans[0]][:gameplan_question_id]}"
                #             # params[:answer_points] = GameplanQuestion.find(params[:gameplan_question][:user_answers][ans[0]][:gameplan_question_id]).gameplan_answers.find(params[:gameplan_question][:user_answers][ans[0]][val]).points
                #             params[:gameplan_question][:user_answers][ans[0]][:answer_points] = GameplanAnswer.find_by(question_id: params[:gameplan_question][:user_answers][ans[0]][:gameplan_question_id]).points
                #         end
                #         params[:gameplan_question][:user_answers][ans[0]][:answer_id] = ans[1][:answer_id].values.join(',')
                #     end
                #     # val = ans[1][:answer_id].values
                # elsif params[:gameplan_question][:user_answers][ans[0]][:answer_points].present?
                #     # GameplanQuestion.find(params[:gameplan_question][:user_answers][ans[0]][:gameplan_question_id]).gameplan_answers.find(params[:gameplan_question][:user_answers][ans[0]][:answer_id]).points
                #     logger.info "===ans===#{params[:gameplan_question][:user_answers][ans[0]][:gameplan_question_id]}"
                #
                #     params[:gameplan_question][:user_answers][ans[0]][:answer_points] = GameplanAnswer.find_by(question_id: params[:gameplan_question][:user_answers][ans[0]][:gameplan_question_id]).points
                # end

                if ans[1][:answer_id].class.eql?(Array)
                    logger.info "===1==="
                    sum = 0
                    ans[1][:answer_id].each do |a|
                        sum += GameplanAnswer.find_by(id: a).try(:points)
                    end
                    params[:gameplan_question][:user_answers][ans[0]][:answer_points] = sum
                    params[:gameplan_question][:user_answers][ans[0]][:answer_id] = ans[1][:answer_id].join(',')
                elsif ans[1][:answer_id].class.eql?(String)
                    logger.info "===2==="
                    params[:gameplan_question][:user_answers][ans[0]][:answer_points] = GameplanAnswer.find_by(id: ans[1][:answer_id]).try(:points)
                else
                    logger.info "===3==="
                    sum = 0
                    ans[1][:answer_id].each do |key,val|
                        sum += GameplanAnswer.find_by(id: val).try(:points)
                        logger.info "===key===#{key}===val===#{val}"
                    end
                    params[:gameplan_question][:user_answers][ans[0]][:answer_points] = sum
                    params[:gameplan_question][:user_answers][ans[0]][:answer_id] = ans[1][:answer_id].values.join(',')
                end


            end
            logger.info "===answers====#{params[:gameplan_question][:user_answers].inspect}"
            user_check = UserAnswer.find_by(:user_id => current_user.id)
            if user_check
                UserAnswer.where(:user_id => current_user.id).update_all({:is_deleted => true})
            end
            params[:gameplan_question][:user_answers].each do |key,val|
                logger.info "==val---#{val.inspect}"
                user_answer = UserAnswer.new
                user_answer.user_id = current_user.id
                user_answer.gameplan_question_id = val[:gameplan_question_id]
                user_answer.question_points =  val[:question_points]
                # answer_points = GameplanQuestion.find(val[:gameplan_question_id]).gameplan_answers.find(val[:answer_id]).points
                answer_points = 100
                user_answer.answer_id = val[:answer_id]
                user_answer.answer_points = (val[:answer_points].blank? || val[:answer_points].nil?) ? answer_points : val[:answer_points]
                if user_answer.save(validate: false)
                    logger.info "===success==="
                else
                    logger.info "===error-=-=#{user_answer.errors.full_messages}"
                end
            end
            flash[:notice] = 'Test taken'
            redirect_to gameplan_index_path
        rescue Exception => e
            UserAnswer.where(:user_id => current_user.id).update_all({:is_deleted => true})
            logger.info "===err===#{e.message}"
            flash[:error] = e.message
            redirect_to gameplan_index_path
        end
    end

    private

    def user_answer_params
        params.require(:gameplan_question).permit(:id, user_answers_attributes: [ :id, :user_id, :answer_id, :answer_id_, :gameplan_question_id, :question_points, :answer_points, :is_deleted ])
        # params.require(:user_answer).permit(:user_id,:question_points, :gameplan_question_id, :answer_id,:answer_points,:is_deleted)
    end
end
