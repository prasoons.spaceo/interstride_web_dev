class HomeController < ApplicationController
    require 'uri'
    require "open-uri"

    # require "onebox"
    def index
        begin
			limit = params[:limit] ? params[:limit] : 10
			offset = params[:offset] ? params[:offset] : 0
			if current_user.user_type == 2
				school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
				deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,school_user]).pluck(:user_post_id)
				total_count =  UserPost.joins(" inner join users as a on a.id=user_posts.posted_by ").where("post_school_id in (?) AND user_posts.id NOT IN (?)" ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools).count
                if params[:search].blank?
                    logger.info { "==================1=================" }
                	posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?)" ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools).order("user_posts.created_at desc").paginate(page: params[:page], per_page: UserPost::PER_PAGE) #.limit(limit).offset(offset)
                else
                    logger.info { "==================2=================" }
                    posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?) and (user_posts.title like ? or user_posts.description like ? or user_posts.share_link like ?) " ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools, "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%").order("user_posts.created_at desc").paginate(page: params[:page], per_page: UserPost::PER_PAGE) #.limit(limit).offset(offset)
                end
			else
				deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,current_user.school_id]).pluck(:user_post_id)
				total_count =  UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").where("post_school_id in (?) AND user_posts.id NOT IN (?)" ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools).count
                if params[:search].blank?
                    logger.info { "==================3=================" }
                    params[:page] = params[:page].blank? ? '1' : params[:page]
	                posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?)" ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools).order("user_posts.created_at desc").paginate(page: params[:page], per_page: UserPost::PER_PAGE) #.limit(limit).offset(offset)
                else
                    logger.info { "==================4=================" }
                    posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?) and (user_posts.title like ? or user_posts.description like ? or user_posts.share_link like ?) " ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools, "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%").order("user_posts.created_at desc").paginate(page: params[:page], per_page: UserPost::PER_PAGE) #.limit(limit).offset(offset)
                end
			end

            @active_record_posts = posts
	        @posts = posts.as_json(only: [:id,:title,:description,:posted_by,:post_image,:created_at,:share_link,:post_type,:user_name,:search_json,:like_count,:comment_count], :methods => [:post_image_url],:current_user => current_user)
            # @posts[:total_pages] = posts.count / 10

	       # @posts = posts.as_json(only: [:id,:title,:description,:posted_by,:post_image,:created_at,:share_link,:post_type,:user_name,:search_json,:like_count,:comment_count], :methods => [:post_image_url],:current_user => current_user)
            #
	        # render :json => {
			#       success: true,
			#       message: t('post_list'),
			#       count: total_count,
			#       data: user_posts
			#     }, status: 200
            #
			# post = nil
            favorite_ids = FavoritePost.where('user_id = ? and is_favorite = ?', current_user.id, true).pluck(:user_post_id)
            @favorite_posts = UserPost.where('id in (?)', favorite_ids)
            respond_to do |format|
              format.html
              format.js
            end
			return
		rescue Exception => e
			Rails.logger.info "v1=======list(UserPostsController) errors=========#{e}"
            flash[:alert] = t('something_went_wrong')
			# error_response(400, t('something_went_wrong'))
			return
		end
    end

    def list_post_comments
        # if params[:post_id].blank?
		# 		error_response(412,t('missing_params') + "post_id" )
		# 		return
		# 	end
		if current_user.user_type == 2
			school_id = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
		elsif current_user.user_type == 3
			school_id = current_user.school_id
		end

        @post = UserPost.find(params[:post_id])

        # commented_users = User.select(" group_concat(id) as ids ").where(" id=#{school_id} OR school_id = #{school_id} OR parent_school_id = #{school_id}")
        #
        #         post = PostComment.where(" user_post_id = #{params[:post_id]} AND user_id IN (#{commented_users[0].ids})").as_json(only: PostComment::JSON_LIST)

		# commented_users = User.select(" group_concat(id) as ids ").where(" id=#{school_id} OR school_id = #{school_id} OR parent_school_id = #{school_id} ")
        commented_users = User.where(" id=#{school_id} OR school_id = #{school_id} OR parent_school_id = #{school_id} or id = #{current_user.id} ").pluck(:id).uniq
        # logger.info { "=usr=====#{commented_users[0].ids.inspect}=====#{commented_users.ids.class}" }

        # commented_users[0].ids = commented_users[0].ids  + ',' + current_user.id.to_s
        @post_comments = PostComment.where(" user_post_id = #{params[:post_id]} AND user_id IN (?)", commented_users).as_json(only: PostComment::JSON_LIST)
        # @post_comments = PostComment.where(" user_post_id = #{params[:post_id]} AND user_id IN (?)", commented_users).as_json(only: PostComment::JSON_LIST)
        logger.info { "=comm--------#{@post_comments.inspect}" }
    end

    def add_comment
        ActiveRecord::Base.transaction do
			begin
				comment = PostComment.new(post_comment_params)
				user_post = UserPost.find(params[:post_comment][:user_post_id])
				user_post.update_attributes(:comment_count => user_post.comment_count + 1)
                logger.info " ===1==="
                comment.save
		        # if !comment.save
                #     logger.info " ===2==="
                #     render js: { success: false, error: t('cant_comment_post'), data: []}
			    #     # error_response(400,t('cant_comment_post'))
			    #     return
			    # end
                logger.info " ===3==="
                if current_user.user_type == 2
                    logger.info " ===4==="
        			school_id = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
        		elsif current_user.user_type == 3
                    logger.info " ===5==="
        			school_id = current_user.school_id
        		end
                @post = UserPost.find(params[:post_comment][:user_post_id].to_i)
                logger.info " ===6==="
        		# commented_users = User.select(" group_concat(id) as ids ").where(" id=#{school_id} OR school_id = #{school_id} OR parent_school_id = #{school_id}")
                commented_users = User.where(" id=#{school_id} OR school_id = #{school_id} OR parent_school_id = #{school_id} or id = #{current_user.id} ").pluck(:id).uniq
                logger.info " ===7==="
                logger.info " ===8==="
                # flash[:notice] = 'Comment Added'
                # @post_comments = PostComment.where(" user_post_id = #{params[:post_comment][:user_post_id].to_i} AND user_id IN (#{commented_users[0].ids})").as_json(only: PostComment::JSON_LIST)
                @post_comments = PostComment.where(" user_post_id = #{params[:post_comment][:user_post_id].to_i} AND user_id IN (?)", commented_users).as_json(only: PostComment::JSON_LIST)
                # render js: { success: true, data: @post_comments }
			    # success_response(200,t('post_comment'),comment.as_json(only: PostComment::JSON_LIST))
			    # comment = nil

			    return
			rescue Exception => e
				Rails.logger.info "v1=======comment_post(UserPostsController) errors=========#{e}"
				raise ActiveRecord::Rollback
				# error_response(400, t('something_went_wrong'))
                flash[:error] = t('something_went_wrong')
                redirect_to request.referer
                # render js: { success: false, error: t('something_went_wrong'), data: []}
				return
			end
		end
    end

    def like_post
		ActiveRecord::Base.transaction do
			begin
				liked_post = PostLike.find_by(user_post_id: params[:user_post_id], user_id: current_user.id )
                logger.info "==pst==#{liked_post.inspect}"
				user_post = UserPost.find(params[:user_post_id])
                logger.info "==count===#{user_post.inspect}"

				if params[:like] == "0"
                    logger.info "===1==="
					user_post.update_attributes(like_count: user_post.like_count == 0 ? 0 : user_post.like_count - 1)
				else
                    logger.info "===2==="
					user_post.update_attributes(like_count: user_post.like_count + 1)
				end
                params[:user_id] = current_user.id
				if liked_post
                    logger.info "===3==="
					liked_post.update_attributes(like: params[:like])
				else
                    logger.info "===4==="
					liked_post = PostLike.new(post_like_params)
                    liked_post.save
			        # if !liked_post.save
				    #     error_response(401,t('cant_like_post'))
				    #     return
				    # end
				end
                logger.info "===5==="
                # flash[:notice] = params[:like] == "0"? 'Post Unliked' : 'Post liked'
                @post = UserPost.find(params[:user_post_id].to_i)
                @post.reload
			    # success_response(200,t('post_liked'),liked_post.as_json(only: PostLike::JSON_LIST))
			    # liked_post = nil
			    return

			rescue Exception => e
				Rails.logger.info "v1=======like_post(UserPostsController) errors=========#{e}"
				raise ActiveRecord::Rollback
                flash[:error] = t('something_went_wrong')
				# error_response(400, t('something_went_wrong'))
                redirect_to request.referer
				return
			end
		end
	end

    def add_post
        ActiveRecord::Base.transaction do
			begin

				# if params['user_post']['title'].blank?
                #     flash[:error] = "Title can't be blank"
				# 	# error_response(412,t('missing_params') + "title" )
				# 	return
				# end

                logger.info "===post===#{post_params.inspect}"
                params[:user_post][:title] = params[:post_title]
		        post = UserPost.new(post_params)
                post.post_image = open(params[:user_post][:image_url]) if params[:user_post][:image_url].present?
		        post.posted_by = current_user.id
		        post.post_type = "normal"
		        if current_user.user_type == 3
		        	post.ptype = 1
		        	post.post_school_id = current_user.school_id
		        elsif current_user.user_type == 2
		        	school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
		        	post.ptype = 1
		        	post.post_school_id = school_user
		        end

		        if !post.save
                    flash[:error] = t('cant_create_post')
			        # error_response(400,t('cant_create_post'))
			        return
			    end
                redirect_to request.referer
			    # success_response(200,t('post_created'),post.as_json(only: UserPost::POST_JSON_LIST, :methods => [:post_image_url],:current_user => current_user))
			    # post = nil
			    return

			rescue Exception => e
				Rails.logger.info "v1=======create(UserPostsController) errors=========#{e}"
				raise ActiveRecord::Rollback
                flash[:error] = t('something_went_wrong')
				# error_response(400, t('something_went_wrong'))
				return
			end
		end
    end

    def favorite_post
		ActiveRecord::Base.transaction do
            begin
                params[:user_id] = current_user.id
                favorite_posts	= FavoritePost.find_by(user_post_id: params[:user_post_id], user_id: current_user.id)
                logger.info "===fav===#{favorite_posts.blank?}"
                if favorite_posts.blank? == true
                    logger.info "===1==="
                    fav = FavoritePost.new(favorite_post_params)
                    if fav.save
                        # flash[:notice] = params[:is_favorite] == 'true' ? 'Article Bookmarked' : 'Article removed as bookmark'
                    else
                        flash[:error] = fav.errors.full_messages.join(', ')
                    end
                    logger.info "===fav===#{fav.errors.full_messages}"
                else
                    logger.info "===2==="
                    favorite_posts.update(favorite_post_params)
                    # flash[:notice] = params[:is_favorite] == 'true' ? 'Article Bookmarked' : 'Article removed as bookmark'
                end
                favorite_ids = FavoritePost.where('user_id = ? and is_favorite = ?', current_user.id, true).pluck(:user_post_id)
                logger.info "===1===#{favorite_ids}"
                @favorite_posts = UserPost.where('id in (?)', favorite_ids)
            rescue Exception => e
                Rails.logger.info "v1========mark_favorite(UserPostsController) errors=========#{e}"
                raise ActiveRecord::Rollback
                flash[:error] = t('something_went_wrong')
				# error_response(400, t('something_went_wrong'))
                redirect_to request.referer
            end
        end
	end

    def get_link_info
        if params[:link].present?
            object = LinkThumbnailer.generate(params[:link])
            logger.info "===obj-===#{object.inspect}"
            logger.info "===titie===#{object.title}"
            if object.present?
                response = { title: object.title.present? ? object.title : '', description: object.description.present? ? object.description : '', image: object.images.present? ? object.images[0].src : '', url: params[:link] }
                logger.info "===res===#{response.inspect}"
            else
                response = nil
            end
        else
            response = nil
            logger.info "===obj-===#{@object.inspect}"
        end
        render json: {data: response.as_json}
    end


    def delete_comment
        ActiveRecord::Base.transaction do
			begin
                comment = PostComment.find_by(id: params[:comment_id])
                user_post = UserPost.find(comment.user_post_id)
                user_post.update_attributes(comment_count: user_post.comment_count - 1)
                logger.info " ===1==="
                comment.destroy
		        # if !comment.save
                #     logger.info " ===2==="
                #     render js: { success: false, error: t('cant_comment_post'), data: []}
			    #     # error_response(400,t('cant_comment_post'))
			    #     return
			    # end
                logger.info " ===3==="
                if current_user.user_type == 2
                    logger.info " ===4==="
        			school_id = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
        		elsif current_user.user_type == 3
                    logger.info " ===5==="
        			school_id = current_user.school_id
        		end
                @post = user_post
                logger.info " ===6==="
        		# commented_users = User.select(" group_concat(id) as ids ").where(" id=#{school_id} OR school_id = #{school_id} OR parent_school_id = #{school_id}")
                commented_users = User.where(" id=#{school_id} OR school_id = #{school_id} OR parent_school_id = #{school_id} or id = #{current_user.id} ").pluck(:id).uniq
                logger.info " ===7==="
                logger.info " ===8==="
                # flash[:notice] = 'Comment Deleted'
                # @post_comments = PostComment.where(" user_post_id = #{@post.id} AND user_id IN (#{commented_users[0].ids})").as_json(only: PostComment::JSON_LIST)
                @post_comments = PostComment.where(" user_post_id = #{@post.id} AND user_id IN (?)", commented_users).as_json(only: PostComment::JSON_LIST)
                # render js: { success: true, data: @post_comments }
			    # success_response(200,t('post_comment'),comment.as_json(only: PostComment::JSON_LIST))
			    # comment = nil

			    return
			rescue Exception => e
				Rails.logger.info "v1=======comment_post(UserPostsController) errors=========#{e}"
				raise ActiveRecord::Rollback
				# error_response(400, t('something_went_wrong'))
                flash[:error] = t('something_went_wrong')
                redirect_to request.referer
                # render js: { success: false, error: t('something_went_wrong'), data: []}
				return
			end
		end
    end

    def bookmarked_articles
        begin
            limit = params[:limit] ? params[:limit] : 10
            offset = params[:offset] ? params[:offset] : 0
            if current_user.user_type == 2
                school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
                deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,school_user]).pluck(:user_post_id)
                favorite_ids = FavoritePost.where('user_id = ? and is_favorite = ?', current_user.id, true).pluck(:user_post_id)
                total_count =  UserPost.joins(" inner join users as a on a.id=user_posts.posted_by ").where("post_school_id in (?) AND user_posts.id NOT IN (?) and user_posts.id in (?)" ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools, favorite_ids).count
                posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by", :favorite_posts).select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?) and user_posts.id in (?)" ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools, favorite_ids).order("favorite_posts.updated_at desc").paginate(page: params[:page], per_page: UserPost::PER_PAGE) #.limit(limit).offset(offset)
            else
                deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,current_user.school_id]).pluck(:user_post_id)
                favorite_ids = FavoritePost.where('user_id = ? and is_favorite = ?', current_user.id, true).pluck(:user_post_id)
                total_count =  UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").where("post_school_id in (?) AND user_posts.id NOT IN (?) and user_posts.id in (?)" ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools, favorite_ids).count
                posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by", :favorite_posts).select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?) and user_posts.id in (?)" ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools, favorite_ids).order("favorite_posts.updated_at desc").paginate(page: params[:page], per_page: UserPost::PER_PAGE) #.limit(limit).offset(offset)
            end

            # logger.info "==posts==#{posts.inspect}====#{posts.class}"
            @active_record_posts = posts
            @posts = posts.as_json(only: [:id,:title,:description,:posted_by,:post_image,:created_at,:share_link,:post_type,:user_name,:search_json,:like_count,:comment_count], :methods => [:post_image_url],:current_user => current_user)
            logger.info "==post1s==#{@posts.inspect}====#{@posts.class}"
            favorite_ids = FavoritePost.where('user_id = ? and is_favorite = ?', current_user.id, true).pluck(:user_post_id)
            @favorite_posts = UserPost.where('id in (?)', favorite_ids)
            respond_to do |format|
              format.html
              format.js
            end
        rescue Exception => e
        end
    end

    def search
        begin
            logger.info { "search=========#{params[:search]}" }
			limit = params[:limit] ? params[:limit] : 10
			offset = params[:offset] ? params[:offset] : 0
            page = params[:page].blank? ? 1 : params[:page]
			# if current_user.user_type == 2
			# 	school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
			# 	deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,school_user]).pluck(:user_post_id)
			# 	#total_count =  UserPost.joins(" inner join users as a on a.id=user_posts.posted_by ").where("post_school_id in (?) AND user_posts.id NOT IN (?)" ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools).count
            #     if params[:search].blank?
            #         logger.info { "==========1===========" }
            #         posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?)" ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools).order("user_posts.created_at desc").paginate(page: page, per_page: UserPost::PER_PAGE)
            #     else
            #         logger.info { "==========2===========" }
            #         posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?) and (user_posts.title like ? or user_posts.description like ? or user_posts.share_link like ?)" ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools, "%#{params[:search]}%").order("user_posts.created_at desc").paginate(page: page, per_page: UserPost::PER_PAGE) #.limit(limit).offset(offset)
            #     end
			# else
			# 	deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,current_user.school_id]).pluck(:user_post_id)
			# 	#total_count =  UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").where("post_school_id in (?) AND user_posts.id NOT IN (?)" ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools).count
            #     if params[:search].blank? || params[:search].eql?("")
            #         posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name").where("post_school_id in (?) AND user_posts.id NOT IN (?)" ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools).order("user_posts.created_at desc").paginate(page: page, per_page: UserPost::PER_PAGE)
            #         logger.info { "===========3===========p#{posts.inspect}" }
            #     else
            #         logger.info { "==========4============" }
            #         posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name").where("post_school_id in (?) AND user_posts.id NOT IN (?) and (user_posts.title like ? or user_posts.description like ? or user_posts.share_link like ?) " ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools, "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%").order("user_posts.created_at desc").paginate(page: page, per_page: UserPost::PER_PAGE)
		    #         # posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name").where("post_school_id in (?) AND user_posts.id NOT IN (?) and (user_posts.title like ? or user_posts.description like ? or user_posts.share_link like ?) " ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools, "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%").order("user_posts.created_at desc").paginate(page: params[:page], per_page: UserPost::PER_PAGE) #.limit(limit).offset(offset)
            #     end
			# end

            if params[:search].present? && !params[:search].blank?
                if current_user.user_type == 2
                    school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
    				deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,school_user]).pluck(:user_post_id)
                    posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?) and (user_posts.title like ? or user_posts.description like ? or user_posts.share_link like ?)" ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools, "%#{params[:search]}%").order("user_posts.created_at desc") #.paginate(page: page, per_page: UserPost::PER_PAGE)
                else
                    deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,current_user.school_id]).pluck(:user_post_id)
                    posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name").where("post_school_id in (?) AND user_posts.id NOT IN (?) and (user_posts.title like ? or user_posts.description like ? or user_posts.share_link like ?) " ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools, "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%").order("user_posts.created_at desc")
                end
                @active_record_posts = posts
    	        @posts = posts.as_json(only: [:id,:title,:description,:posted_by,:post_image,:created_at,:share_link,:post_type,:user_name,:search_json,:like_count,:comment_count], :methods => [:post_image_url],:current_user => current_user)
                respond_to do |format|
                    format.js {  }
                end
            else
                if current_user.user_type == 2
                    school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
    				deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,school_user]).pluck(:user_post_id)
                    posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name	").where("post_school_id in (?) AND user_posts.id NOT IN (?)" ,[0,school_user],deleted_by_schools.blank? == true ? "" : deleted_by_schools).order("user_posts.created_at desc").paginate(page: page, per_page: UserPost::PER_PAGE)
                else
                    deleted_by_schools = DeleteSchoolPost.where(" user_id IN (?)", [0,current_user.school_id]).pluck(:user_post_id)
                    posts = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by").select("user_posts.*,a.full_name as user_name").where("post_school_id in (?) AND user_posts.id NOT IN (?) and (user_posts.title like ? or user_posts.description like ? or user_posts.share_link like ?) " ,[0,current_user.school_id],deleted_by_schools.blank? == true ? "" : deleted_by_schools, "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%").order("user_posts.created_at desc")
                end
                respond_to do |format|
                    format.js { render :index }
                end
            end

            # p "==pos=t========#{posts.inspect}"
            # @active_record_posts = posts
	        # @posts = posts.as_json(only: [:id,:title,:description,:posted_by,:post_image,:created_at,:share_link,:post_type,:user_name,:search_json,:like_count,:comment_count], :methods => [:post_image_url],:current_user => current_user)
            # # respond_to do |format|
            # #   format.html
            # #   format.js
            # # end
			# return
        rescue Exception => e
        end
    end

    def delete_post
        begin
            if params[:id].present?
                @post = UserPost.find_by(id: params[:id])
                @user_post = @post
                @post.destroy
            end
        rescue Exception => e
        end
    end

    private

    def post_comment_params
		params.require(:post_comment).permit(:user_post_id, :user_id, :comment)
	end

    def post_like_params
		params.permit(:user_post_id, :user_id, :like)
	end

    def favorite_post_params
		params.permit(:user_post_id, :user_id, :is_favorite)
	end

    def post_params
        params.require(:user_post).permit(:title, :description, :posted_by, :post_image, :share_link)
    end
end
