class JobStepsController < ApplicationController
    include Wicked::Wizard
    steps :areatype, :company_type, :keywords
    def show
        @user = current_user
        render_wizard
    end

    def update
        @user = current_user
        case step
        when :areatype
              # params[:search] =
        end
        render_wizard @user
    end

    private

        def redirect_to_finish_wizard
            redirect_to root_url, notice: "Thank you for signing up."
        end
end
