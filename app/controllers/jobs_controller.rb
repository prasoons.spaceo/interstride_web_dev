class JobsController < ApplicationController
    # require 'will_paginate/array'


    def index
        @companies = IndeedCompany.order('name ASC')
        @countries = IndeedCountry.order('name ASC')
        @favorite_companies = FavoriteCompany.get_list(current_user)
        @companies_list = Company.all.order("company_name asc").limit(100)
    end

    def search_old
        # params[:page] = 1
        IndeedAPI.publisher_id = Rails.application.secrets.indeed_publisher_id
        logger.info " ===#{params[:job_search][:page] == "1" ? params[:job_search][:page] : ((params[:job_search][:page].to_i - 1) * 10)} "
        country = params[:job_region].eql?('US') ? 'us' : params[:job_search][:country].present? ? params[:job_search][:country] : 'us'
        company = params[:job_search][:company_name].present? ? params[:job_search][:company_name] : params[:company_name].present? ? params[:company_name] : ''
        search_query = (params[:job_search][:search].present? && company.present?) ? "#{params[:job_search][:search]} company:#{company}" : (params[:job_search][:search].present? && !company.present?) ? "#{params[:job_search][:search]}" : (!params[:job_search][:search].present? && company.present?) ? "company:#{company}" : ''
        if params[:job_search][:page].to_i == 1
            result = IndeedAPI.search_jobs(q: search_query, l: params[:job_search][:city], jt: params[:job_search][:job_type], fromage: params[:job_search][:publish_date], co: country, as_cmp: company, sort: params[:sort])
        else
            result = IndeedAPI.search_jobs(q: search_query, l: params[:job_search][:city], jt: params[:job_search][:job_type], fromage: params[:job_search][:publish_date], co: country, start: params[:job_search][:page] == "1" ? params[:job_search][:page] : ((params[:job_search][:page].to_i - 1) * 10), as_cmp: company, sort: params[:sort])
        end
        @total = result.total_results
        @end = result.end
        @start = result.start
        @jobs = result.results

        # @jobs << {total_pages: @jobs.count / 10}
        logger.info "===params==#{result.inspect}=======#{result.results}"
        # exit(0)
    end


    def search
        # params[:page] = 1
        IndeedAPI.publisher_id = Rails.application.secrets.indeed_publisher_id
        Rails.logger.info { "======sdf----------#{params[:job_search]}====#{params['job_search']}" }
        logger.info " ===#{params[:job_search][:page] == "1" ? params[:job_search][:page] : ((params[:job_search][:page].to_i - 1) * 10)} "
        country = params[:job_region].eql?('US') ? 'us' : params[:job_search][:country].present? ? params[:job_search][:country] : 'us'
        company = params[:job_search][:company_name].present? ? params[:job_search][:company_name] : params[:company_name].present? ? params[:company_name] : ''
        search_query = (params[:job_search][:search].present? && company.present?) ? "company:#{company} #{params[:job_search][:search]}" : (params[:job_search][:search].present? && !company.present?) ? "#{params[:job_search][:search]}" : (!params[:job_search][:search].present? && company.present?) ? "company:#{company}" : ''
        if params[:job_search][:page].to_i == 1
            result = IndeedAPI.search_jobs(q: search_query, l: params[:job_search][:city], jt: params[:job_search][:job_type], fromage: params[:job_search][:publish_date], co: country, as_cmp: company, sort: params[:sort], rbc: company)
        else
            result = IndeedAPI.search_jobs(q: search_query, l: params[:job_search][:city], jt: params[:job_search][:job_type], fromage: params[:job_search][:publish_date], co: country, start: params[:job_search][:page] == "1" ? params[:job_search][:page] : ((params[:job_search][:page].to_i - 1) * 10), as_cmp: company, sort: params[:sort], rbc: company)
        end
        @total = result.total_results
        @end = result.end
        @start = result.start
        @jobs = result.results
        @favorite_jobs = FavoriteJob.get_list(current_user)

        # @jobs << {total_pages: @jobs.count / 10}
        logger.info "===params==#{result.inspect}=======#{result.results}"
        respond_to do |format|
            format.html {  }
            format.js {  }
        end
        # exit(0)
    end

    def get_open_jobs
        # q=Open&rbc=Dollar+General
        IndeedAPI.publisher_id = Rails.application.secrets.indeed_publisher_id
        params[:sort_by] = params[:sort_by].present? ? params[:sort_by] : 'relevance'
        params[:job_type] = params[:job_type] || 'all'
        params[:keyword] = params[:keyword] || ''


        if params[:page].to_i == 1
            result = params[:city].present? ? IndeedAPI.search_jobs(q: "#{params[:keyword]} company:#{params[:company_name]}", rbc: params[:company_name], jt: params[:job_type], sort: params[:sort], l: params[:city]) : IndeedAPI.search_jobs(q: "#{params[:keyword]} company:#{params[:company_name]}", rbc: params[:company_name], jt: params[:job_type], sort: params[:sort])
        else
            # result = IndeedAPI.search_jobs(q: search_query, l: params[:job_search][:city], jt: params[:job_search][:job_type], fromage: params[:job_search][:publish_date], co: country, start: params[:job_search][:page] == "1" ? params[:job_search][:page] : ((params[:job_search][:page].to_i - 1) * 10), as_cmp: company.downcase, sort: params[:sort])
            result = params[:city].present? ? IndeedAPI.search_jobs(q: "#{params[:keyword]} company:#{params[:company_name]}", rbc: params[:company_name], jt: params[:job_type], sort: params[:sort], l: params[:city], start: params[:page] == "1" ? params[:page] : ((params[:page].to_i - 1) * 10)) : IndeedAPI.search_jobs(q: "#{params[:keyword]} company:#{params[:company_name]}", rbc: params[:company_name], jt: params[:job_type], sort: params[:sort], start: params[:page] == "1" ? params[:page] : ((params[:page].to_i - 1) * 10))
        end


        # logger.info "====sql===#{IndeedAPI.search_jobs(q: "#{params[:keyword]} company:#{params[:company_name]}", rbc: params[:company_name], jt: params[:job_type], sort: params[:sort]).query}"
        # result = params[:city].present? ? IndeedAPI.search_jobs(q: "#{params[:keyword]} company:#{params[:company_name]}", rbc: params[:company_name], jt: params[:job_type], sort: params[:sort], l: params[:city]) : IndeedAPI.search_jobs(q: "#{params[:keyword]} company:#{params[:company_name]}", rbc: params[:company_name], jt: params[:job_type], sort: params[:sort])
        # logger.info "===res====#{result.inspect}"
        @total = result.total_results
        @end = result.end
        @start = result.start
        @jobs = result.results
        @favorite_jobs = FavoriteJob.get_list(current_user)
        respond_to do |format|
            format.html {  }
            format.js {  }
        end
        logger.info "===total==#{@total} ===#{@end}"
    end

    def filter_open_jobs
        IndeedAPI.publisher_id = Rails.application.secrets.indeed_publisher_id
        result = IndeedAPI.search_jobs(q: "Open company:#{params[:company_name]}", rbc: params[:company_name])
        @total = result.total_results
        @end = result.end
        @start = result.start
        @jobs = result.results
        logger.info "===total==#{@total} ===#{@end}"
    end

    def favorite_job
        begin
            @fav_job = FavoriteJob.where(user_id: current_user.id, job_id: params[:job_id])
            logger.info "===compfav==#{@fav_job.inspect}"
            if params[:like].eql?('like')
                logger.info { "======1=======" }
                if @fav_job.present?
                    @fav_job.destroy_all
                end
                logger.info "===2==="
                params[:user_id] = current_user.id
                params[:is_favorite] = params[:like]
                @fav_job = FavoriteJob.new(favorite_job_params)
                logger.info "===facv===#{@fav_job.inspect}"
                if @fav_job.save
                    logger.info { "=========saved=========" }
                else
                    logger.info { "=========err=========#{@fav_job.errors.full_messages}" }
                end
                @favorite_jobs = FavoriteJob.get_list(current_user)
                # @companies_list = Company.all.order("company_name asc").limit(100)
                # flash[:notice] = 'Job added as favorite'
                respond_to do |format|
                    format.json { render json: { success: true } }
                    format.js {  }
                end
                # render json: { success: true, message: 'Company added as favorite' }
            else
                if @fav_job.present?
                    logger.info "===3==="
                    @fav_job.destroy_all
                    @favorite_jobs = FavoriteJob.get_list(current_user)
                    # @companies_list = Company.all.order("company_name asc").limit(100)
                    # flash[:notice] = 'Job removed from favorite'
                    respond_to do |format|
                        format.json { render json: { success: true } }
                        format.js {  }
                    end
                end
            end
        rescue Exception => e

        end
    end

    private
    def favorite_job_params
        params.permit(:user_id, :job_id, :is_favorite, :job_title, :job_link, :company_name)
    end
end
