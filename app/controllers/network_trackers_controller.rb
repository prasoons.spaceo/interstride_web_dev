class NetworkTrackersController < ApplicationController
    before_action :get_network_tracker, only: [ :show, :edit, :update, :destroy ]

    def index
        # @upcoming = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").where("user_id = '"+current_user.id.to_s+"' AND appointment_date >= CURDATE()").group("  network_trackers.id,company_id ").order("appointment_date asc")#.as_json(:only => NetworkTracker::JSON_LIST)
		# @past = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").where("user_id = '"+current_user.id.to_s+"' AND appointment_date < CURDATE()").group("  network_trackers.id,company_id ").order("appointment_date desc")#.as_json(:only => NetworkTracker::JSON_LIST)
        @upcoming = NetworkTracker.where("user_id = '"+current_user.id.to_s+"' AND appointment_date >= CURDATE()").group("  network_trackers.id,company_id ").order("appointment_date asc")#.as_json(:only => NetworkTracker::JSON_LIST)
		@past = NetworkTracker.where("user_id = '"+current_user.id.to_s+"' AND appointment_date < CURDATE()").group("  network_trackers.id,company_id ").order("appointment_date desc")#.as_json(:only => NetworkTracker::JSON_LIST)
        @network_tracker = NetworkTracker.new
        # @companies = Company.all.order('sort_order ASC').limit(500)
		# response_hash = Hash.new
		# response_hash[:upcoming] = upcoming
        # response_hash[:past] = past
    end

    def create
		ActiveRecord::Base.transaction do
			begin
                network_trackers_params[:start_time] = Time.parse(network_trackers_params[:start_time]).strftime('%I:%M %p')
                network_trackers_params[:end_time] = Time.parse(network_trackers_params[:end_time]).strftime('%I:%M %p')
                params[:network_tracker][:appointment_date] = Date.strptime(params[:network_tracker][:appointment_date], '%m/%d/%Y').strftime('%d/%m/%Y')
                logger.info { "===params========#{params[:network_tracker].inspect}" }
                network_trackers = NetworkTracker.new(network_trackers_params)
                # network_trackers_params[:start_time] = network_trackers_params[:start_time].strftime('%I:%M %P')
			    if params[:network_tracker][:company_name].blank? == false
                    logger.info "===1===="
				   network_trackers.company_id = 0
				   network_trackers.company_name = params[:network_tracker][:company_name]
                else
                    logger.info "===2===="
                   network_trackers.company_name = Company.find_by(id: params[:network_tracker][:company_id]).company_name
				end
                network_trackers.user_id = current_user.id
                logger.info "=====net === #{network_trackers.inspect}"
		        if !network_trackers.save
                    logger.info "===3===="
			        redirect_to request.referer, alert: network_trackers.errors.full_messages.join(', ')
			        return
		    	end
		    	# @upcoming = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").find_by(:id => network_trackers.id )
                redirect_to request.referer, notice: 'Meeting added successfully.'
		    	# success_response(200,t('network_trackers_add'),network_trackers.as_json(:only => NetworkTracker::JSON_LIST))
		  		# network_trackers = nil
				return
			rescue Exception => e
                logger.info "===4====#{e.message}"
				# Rails.logger.info "v1=======create (NetworkTrackersController) errors=========#{e}"
		        # error_response(400, t('something_went_wrong'))
		        # raise ActiveRecord::Rollback
		        # return
                redirect_to request.referer, alert: 'Something went wrong'
			end
		end
	end

    def show

    end

    def search
        if params[:search].present? && !params[:search].blank?
            @upcoming = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").where("user_id = '"+current_user.id.to_s+"' AND appointment_date >= CURDATE() AND (company_name like '%#{params[:search]}%' or contact_name like '%#{params[:search]}%' or notes like '%#{params[:search]}%' or appointment_date like '%#{params[:search]}%') ").group("  network_trackers.id,company_id ").order("appointment_date asc")#.as_json(:only => NetworkTracker::JSON_LIST)
    		@past = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").where("user_id = '"+current_user.id.to_s+"' AND appointment_date < CURDATE() AND (company_name like '%#{params[:search]}%' or contact_name like '%#{params[:search]}%' or notes like '%#{params[:search]}%' or appointment_date like '%#{params[:search]}%')").group("  network_trackers.id,company_id ").order("appointment_date desc")#.as_json(:only => NetworkTracker::JSON_LIST)
            # @countries = Country.joins('left join country_guides as a on a.country_name = countries.name').where('countries.name like ?', "%#{params[:search]}%").order('countries.name')
        else
            @upcoming = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").where("user_id = '"+current_user.id.to_s+"' AND appointment_date >= CURDATE()").group("  network_trackers.id,company_id ").order("appointment_date asc")#.as_json(:only => NetworkTracker::JSON_LIST)
    		@past = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").where("user_id = '"+current_user.id.to_s+"' AND appointment_date < CURDATE()").group("  network_trackers.id,company_id ").order("appointment_date desc")#.as_json(:only => NetworkTracker::JSON_LIST)
        end
        @network_tracker = NetworkTracker.new
        @companies = Company.all.order('sort_order ASC').limit(500)

        respond_to do |format|
            format.js
        end
    end

    def edit
        @companies = Company.all.order('sort_order ASC').limit(500)
    end

    def update
        ActiveRecord::Base.transaction do
			begin
				if @network_tracker.present?
                    logger.info "===1==="
                    # params[:network_tracker][:company_name] = Company.find_by(id: params[:network_tracker][:company_id]).company_name
                    network_trackers_params[:start_time] = Time.parse(network_trackers_params[:start_time]).strftime('%I:%M %p')
                    network_trackers_params[:end_time] = Time.parse(network_trackers_params[:end_time]).strftime('%I:%M %p')
                    params[:network_tracker][:appointment_date] = Date.strptime(params[:network_tracker][:appointment_date], '%m/%d/%Y').strftime('%d/%m/%Y')
    		        if @network_tracker.update_attributes(network_trackers_params)
                        logger.info "===2==="
        		    	@upcoming = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").find_by(:id => @network_tracker.id )
                        redirect_to request.referer, notice: 'Meeting updated.'
                    else
                        logger.info "===3==="
                        redirect_to request.referer, alert: @network_tracker.errors.full_messages.join(', ')
                    end
    		    	# success_response(200,t('network_trackers_add'),network_trackers.as_json(:only => NetworkTracker::JSON_LIST))
    		  		# network_trackers = nil
                end
			rescue Exception => e
                logger.info "===4===#{e.message}"
	            redirect_to request.referer, alert: 'Something Went Wrong'
			end
		end
    end

    def destroy
        ActiveRecord::Base.transaction do
			begin
                if @network_tracker.present?
                    @network_tracker.destroy
                else
                end
            rescue Exception => e
            end
        end
    end

    private

    def get_network_tracker
      @network_tracker = NetworkTracker.find(params[:id])
    end

    def network_trackers_params
        params.require(:network_tracker).permit(:company_id, :contact_name, :contact_email, :appointment_date, :notes, :followup_notes,:icalendar_id,:google_calendar_id,:start_time,:end_time, :company_name)
    end
end
