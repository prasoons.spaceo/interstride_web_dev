class NotificationsController < ApplicationController
    def index
        @notifications = Notification.get_list(current_user)
    end

    def read
        @notification = Notification.find(params[:id])
        if @notification.present?
            @notification.is_read = true
            @notification.save
        end
        redirect_to notifications_path
    end
end
