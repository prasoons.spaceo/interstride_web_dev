class OauthController < ApplicationController
    require 'open-uri'

    def callback
        logger.info "==here==="
        begin
            oauth = OauthService.new(request.env['omniauth.auth'])

            # if oauth_account = oauth.create_oauth_account!
                # ...
                logger.info { "====auth=====#{oauth.inspect}===#{oauth.auth_hash.info}" }
                uid = oauth.auth_hash.uid
                image = oauth.auth_hash.info.image
                current_user.linkedIn_id = uid
                # if !current_user.user_profile.present?
                logger.info "===img===#{image}"
                    current_user.user_profile = open(image)
                # end
                current_user.save
                redirect_to authenticated_root_path, notice: 'LinkedIn account synced successfully'
                # if oauth.present?
                #     current_user.linkedIn_id = oauth
                # end
                # redirect_to Config.provider_login_path
            # end
        rescue => e
            logger.info { e.message }
            flash[:alert] = "There was an error while trying to authenticate your account."
            redirect_to authenticated_root_path
        end
    end

    def failure
        flash[:alert] = "There was an error while trying to authenticate your account."
        redirect_to authenticated_root_path
    end

end
