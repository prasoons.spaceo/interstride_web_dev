class PasswordsController < Devise::PasswordsController
    layout 'login'
    def create
        @user = User.find_by_email(params[:email])

	    if @user.present?
			@user.send_reset_password_instructions
            flash[:notice] = t('send_instructions_email')
			redirect_to unauthenticated_root_path
	    else
            flash[:notice] = t('email_not_found')
			redirect_to unauthenticated_root_path
	    end
    end

    def update
        # # logger.info "===@resource #{@resource.inspect}"
        if params[:user][:password].eql?(params[:user][:password_confirmation])
            logger.info "===reser====#{params[:user][:reset_password_token]}"
            # resource.password = params[:password]
            user = User.find_by(reset_password_token: params[:user][:reset_password_token])
            logger.info "===user===#{user.inspect}"
            user.password = params[:user][:password]
            if user.save
                flash[:notice] = 'Password Changed successfully'
                redirect_to new_user_session_path
            else
                flash[:alert] = user.errors.full_messages.join(', ')
                redirect_to request.referer
            end
        else
            flash[:alert] = 'Password and password confirmation do not match'
            redirect_to request.referer
        end
        # begin
        #     self.resource = resource_class.reset_password_by_token(resource_params)
        #     logger.info "===res===#{resource.inspect}"
        #     logger.info "==err===#{}"
        #     # yield resource if block_given?
        #
        #     if resource.errors.empty?
        #         logger.info "===1==="
        #         resource.unlock_access! if unlockable?(resource)
        #         if Devise.sign_in_after_reset_password
        #             logger.info "===2==="
        #             flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        #             set_flash_message!(:notice, flash_message)
        #             sign_in(resource_name, resource)
        #         else
        #             logger.info "===3==="
        #             set_flash_message!(:notice, :updated_not_active)
        #         end
        #         respond_with resource, location: after_resetting_password_path_for(resource)
        #     else
        #         logger.info "===4==="
        #         set_minimum_password_length
        #         respond_with resource
        #     end
        # rescue Exception => e
        #     logger.info "==err==#{e.message}"
        # end
        logger.info "==p[aras]===#{params.inspect}"
    end
end
