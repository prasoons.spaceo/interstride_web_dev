class RegistrationsController < Devise::RegistrationsController
    protect_from_forgery prepend: true, with: :exception
    layout 'login'
    def new
        super
    end

    def create
	    ActiveRecord::Base.transaction do
	      begin
              params[:user][:school_user_email]  = params[:user][:email]
	        @user = User.new(user_params)
	        @user.user_type = 3
	        if !@user.save
                flash[:alert] = @user.errors.full_messages.first
                logger.info { "========err===#{@user.errors.full_messages}" }
                render :new
		        return
		    end

		    if @user.user_type == 3
		   	 	school = SchoolUser.find_by(:user_email =>  @user.school_user_email,:school_id => @user.school_id)
		   	 	school.update_attributes(:is_registered => true)

		   	 	school_data = User.find_by(:id => @user.school_id)
		   	 	if school_data.school_admin_id != 0
		   	 		@user.update_attributes(:degree_level => school.degree_level,:school_admin_id => school_data.school_admin_id)
		   	 	elsif school_data.has_child_school == true
		   	 		@user.update_attributes(:degree_level => school.degree_level,:school_admin_id => school_data.id)
		   	 	else
		   	 		@user.update_attributes(:degree_level => school.degree_level)
		   	 	end
		   	end

	      	# # Save device detail
	      	# if params[:user_device]
		    #     @user_device = UserDevice.new(user_device_params)
		    #     @user_device.user_id = @user.id
		    #     @user_device.save
		    # end

		    # UserMailer.registration_confirmation(@user).deliver_now

			##Common structure
			# reponse_data = Array.new
			# response_hash = Hash.new
			# user_arr = Array.new
			# user_device_arr = Array.new
            #
			# user_arr.push(@user.as_json(only: User::JSON_LIST, :methods => [:user_profile_url]))
			# user_device_arr.push(@user_device.as_json(only: UserDevice::JSON_LIST))
            #
			# response_hash[:users] = user_arr
			# response_hash[:user_device] = user_device_arr
			# # reponse_data.push(response_hash)
            #
	  		# success_response(200,t('signup_msg'),response_hash)
	  		# # reponse_data = nil
	  		# user_arr = nil
	  		# user_device_arr = nil
            # set_flash_message! :notice, :signed_up
            flash[:notice] = 'Signed Up successfully'
            sign_up('User', @user)
            sign_in(@user)
            respond_with @user, location: after_sign_up_path_for(@user)


			return

	      rescue Exception => e
	        Rails.logger.info "v1=======create (RegistrationsController) errors=========#{e}"
	        # error_response(400, t('something_went_wrong'))
            flash[:alert] = @user.errors.full_messages.first
	        raise ActiveRecord::Rollback
	        return
	      end
	    end
	end

    def verify_school_user
        school_user = SchoolUser.find_by(user_email: params[:email])
        logger.info "===user===#{school_user.inspect}"
        if school_user.blank? == false
            logger.info "===1==="
				# user = User.find_by(:email => params[:email])
			if school_user.is_registered == true
                logger.info "===2==="
				# error_response(200, t('user_exist'))
				# return
                # flash[:error] = t('user_exist')
                # render json: { success: false, message: t('user_exist') }
                render json: { success: false, message: t('user_exist') }
			else
                logger.info "===3==="
				response_hash = {
					id: school_user.school_id,
					school_name: User.find_by(:id => school_user.school_id).full_name
				}

				# success_response(200,t('user_found'),reponse_hash)
				# return
                # flash[:notice] = t('user_found')
                # render json: { success: true, message: t('user_found'), data: response_hash }
                render json: { success: true, data: response_hash }
			end
		else
            logger.info "===4==="
			# error_response(200, t('not_school_user'))
			# return
            # flash[:alert] = t('not_school_user')
            render json: { success: false, message: t('not_school_user') }
		end
    end

    private
    def sign_up_params
        params.require(:user).permit(:email, :password, :password_confirmation, :full_name, :school_user_email, :school_id)
    end

    def account_update_params
        params.require(:user).permit(:name, :email, :password, :password_confirmation, :current_password)
    end

    def user_params
	    params.require(:user).permit(:full_name, :email, :password, :password_confirmation, :expected_graduation, :linkedIn_id, :phone, :longitude, :latitude,:address,:user_type,:school_id,:school_user_email)
	end

end

# class RegistrationsController < Devise::RegistrationsController
#     layout 'login'
#     def create
#         build_resource(sign_up_params)
#         resource.snapp_role = "Client"
#         logger.info "===res===#{resource.inspect}"
#         resource.save
#         yield resource if block_given?
#         if resource.persisted?
#             logger.info "===1===="
#             if resource.active_for_authentication?
#                 logger.info "====2==="
#                 flash[:notice] = I18n.t 'registrations.signed_up'
#                 set_flash_message :notice, :signed_up if is_flashing_format?
#                 sign_up(resource_name, resource)
#                 respond_with resource, location: after_sign_up_path_for(resource)
#             else
#                 logger.info "====3===reso#{resource.inactive_message}"
#                 flash[:notice] = I18n.t 'registrations.signed_up_but_inactive'
#                 set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
#                 expire_data_after_sign_in!
#                 respond_with resource, location: after_inactive_sign_up_path_for(resource)
#             end
#         else
#             logger.info "===4==== #{resource.errors.full_messages}"
#             flash[:error] = resource.errors.full_messages.join(', ')
#             clean_up_passwords resource
#             respond_with resource
#         end
#     end
#
#     private
#     def sign_up_params
#         params.require(:user).permit(:name, :email, :password, :password_confirmation, client_attributes: [:company_name, :company_type, :industry_type, :company_address, :city, :zip_code, :state, :country, :phone_number, :contact_person, :industry_id])
#     end
#
#     def account_update_params
#         params.require(:user).permit(:name, :email, :password, :password_confirmation, :current_password)
#     end
#
# end
