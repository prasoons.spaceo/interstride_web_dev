class UsersController < ApplicationController
    require 'base64'
    def edit
        logger.info "===curr===#{current_user.inspect}====res===#{current_user.resume.present?}"
    end

    def update
        # params[:user][:user_profile] = Base64.decode64(params[:user][:user_profile])
        ActiveRecord::Base.transaction do
            begin
                if params[:user][:user_profile].split('/').first.eql?('https:')
                    params[:user] = params[:user].except(:user_profile)
                end
                logger.info { "===========#{params[:user].inspect}=======#{params[:user].except(:user_profile)}===" }
                #data:image
                #
                if current_user.update_attributes(user_params)
                    logger.info { "=============1==============" }
                    flash[:notice] = 'Profile Updated.'
                    redirect_to request.referer
                else
                    logger.info { "=============2==============" }
                    flash[:error] = current_user.errors.full_messages.join(', ')
                    redirect_to request.referer
                end
            rescue Exception => e
                flash[:error] = e.message
                logger.info "=-========errr=================#{e.message}"
                raise ActiveRecord::Rollback
                redirect_to request.referer
            end
        end
    end

    # def show
    #
    # end

    # def change_password
    #     ActiveRecord::Base.transaction do
    #         begin
    #             # if params[:current_password].eql?(params[:new_password_confirmation])
    #             if current_user.valid_password(params[:old_password])
    #                 flash[:error] =
    #             else
    #             end
    #         rescue Exception => e
    #         end
    #     end
    # end

    def reset_password
        @user = User.find_by_email(params[:user][:email])

	    if @user.present?
			@user.send_reset_password_instructions #.deliver!
            flash[:notice] = t('send_instructions_email')
			redirect_to unauthenticated_root_path
	    else
            flash[:alert] = t('email_not_found')
			redirect_to unauthenticated_root_path
	    end
    end

    def change_password
        if current_user.valid_password?(params[:user][:old_password])
            logger.info "===new===#{params[:user][:new_password]}===conf====#{params[:user][:new_password_confirmation]}"
            if params[:user][:new_password].eql?(params[:user][:new_password_confirmation])
                current_user.password = params[:user][:new_password]
                if current_user.save
                    user= current_user
                    sign_out(current_user)
                    sign_in(user)
                    # redirect_to new_user_session_path, notice: 'Password Changed successfully. Please log in again.'
                    redirect_to request.referer, notice: 'Password Changed successfully.'
                else
                    redirect_to edit_user_path(current_user), alert: current_user.errors.full_messages.join(', ')
                end
            else
                redirect_to edit_user_path(current_user), alert: 'New Password and new password confirmation do not match'
            end
        else
            redirect_to edit_user_path(current_user), alert: 'Old Password is invalid'
        end
    end

    def upload_resume
        if current_user.update_attributes(resume: params[:user][:resume])
            logger.info "====save==="
            @response = { success: true, message: 'Resume uploaded' }
            # flash[:notice] = 'Resume uploaded'
        else
            # flash[:alert] = current_user.errors.full_messages.join(', ')
            @response = { success: false, message: current_user.errors.full_messages.join(', ') }
            logger.info "===err===#{current_user.errors.full_messages}"
        end
    end

    def download_resume
        data = open(current_user.resume.url)
        send_data data.read, filename: 'Resume.pdf', type: 'application/pdf', disposition: 'attachment'
    end

    private

    def user_params
        params.require(:user).permit(:id, :full_name, :phone, :user_profile, :resume)
    end
end
