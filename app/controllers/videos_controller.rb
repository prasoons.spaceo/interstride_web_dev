class VideosController < ApplicationController

    def index
        if current_user.user_type == 2
            school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
            @categories =  VideoCategory.where(:video_school_id => [0,school_user]).order("category_name asc")
            # default_category = VideoCategory.find_by(category_name: 'Interstride Videos')
            default_category = VideoCategory.find_by(category_name: 'OPT & CPT')
            params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : (default_category.try(:id) || @categories.first.id)
            @videos =  Video.where(video_category_id: params[:video_category_id], video_school_id: [0,school_user])
        else
            @categories =  VideoCategory.where(:video_school_id => [0,current_user.school_id]).order("category_name asc")
            # default_category = VideoCategory.find_by(category_name: 'Interstride Videos')
            default_category = VideoCategory.find_by(category_name: 'OPT & CPT')
            params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : (default_category.try(:id) || @categories.first.id)
            @videos =  Video.where(video_category_id: params[:video_category_id], video_school_id: [0,current_user.school_id])
        end
        if params[:notification_id].present?
            notif = Notification.find(params[:notification_id])
            notif.update_attributes(is_read: true)
        end
        @category_name = VideoCategory.find_by(id: params[:video_category_id]).category_name
        @category_id = params[:video_category_id]
        logger.info "===name===#{@category_name}"
        @youtube_links = @videos.youtube_links
        @web_links = @videos.web_links
        @video_links = @videos.videos
        @docs = @videos.docs
        @ppts = @videos.ppts
        @excel_sheets = @videos.excel_sheets
        @pdfs = @videos.pdfs
    end

    def get_video_list
        logger.info "===in==="
        @category_name = VideoCategory.find(params[:category_id]).category_name
        if current_user.user_type == 2
            school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
            # @categories =  VideoCategory.where(:video_school_id => [0,school_user]).order("category_name asc")
            # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
            @videos =  Video.where(video_category_id: params[:category_id], video_school_id: [0,school_user]).order('title ASC')
        else
            # @categories =  VideoCategory.where(:video_school_id => [0,current_user.school_id]).order("category_name asc")
            # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
            @videos =  Video.where(video_category_id: params[:category_id], video_school_id: [0,current_user.school_id]).order('title ASC')
        end
        @youtube_links = @videos.youtube_links
        @web_links = @videos.web_links
        @video_links = @videos.videos
        @docs = @videos.docs
        @ppts = @videos.ppts
        @excel_sheets = @videos.excel_sheets
        @pdfs = @videos.pdfs
        logger.info "====pdf===#{@videos.pdfs.inspect}"
    end

    def search
        if params[:search].present? && !params[:search].blank?
            #
            # @upcoming = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").where("user_id = '"+current_user.id.to_s+"' AND appointment_date >= CURDATE() AND company_name like '%#{params[:search]}%'").group("  network_trackers.id,company_id ").order("appointment_date asc")#.as_json(:only => NetworkTracker::JSON_LIST)
            # @past = NetworkTracker.joins(" LEFT JOIN company_details as c ON c.company_id=network_trackers.company_id ").select("employer_id,employer_name,company_logo,network_trackers.*").where("user_id = '"+current_user.id.to_s+"' AND appointment_date < CURDATE() AND company_name like '%#{params[:search]}%'").group("  network_trackers.id,company_id ").order("appointment_date desc")#.as_json(:only => NetworkTracker::JSON_LIST)
            # # @countries = Country.joins('left join country_guides as a on a.country_name = countries.name').where('countries.name like ?', "%#{params[:search]}%").order('countries.name')
            # category_id = VideoCategory.find_by(category_name: params[:category]).id
            # @category_name = params[:category]
            if current_user.user_type == 2
                school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
                # @categories =  VideoCategory.where(:video_school_id => [0,school_user]).order("category_name asc")
                # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
                @videos =  Video.where(video_school_id: [0,school_user])
                @videos = @videos.where("title like ?", "%#{params[:search]}%").order('title ASC')
            else
                # @categories =  VideoCategory.where(:video_school_id => [0,current_user.school_id]).order("category_name asc")
                # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
                @videos =  Video.where(video_school_id: [0,current_user.school_id])
                @videos = @videos.where("title like ?", "%#{params[:search]}%").order('title ASC')

            end
            logger.info "====vide===#{@videos.inspect}"
            @category_name = "Search results for '"+ params[:search] + "'"
            if @videos.present?
                @youtube_links = @videos.youtube_links
                @web_links = @videos.web_links
                @video_links = @videos.videos
                @docs = @videos.docs
                @ppts = @videos.ppts
                @excel_sheets = @videos.excel_sheets
                @pdfs = @videos.pdfs
                logger.info "====pdf===#{@videos.pdfs.inspect}"
            end
        else
            if params[:category].blank?
                logger.info "===cat====="
                # category_id = VideoCategory.order('category_name ASC').first.id
                # @category_name = VideoCategory.order('category_name ASC').first.category_name
                # if current_user.user_type == 2
                #     school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
                #     # @categories =  VideoCategory.where(:video_school_id => [0,school_user]).order("category_name asc")
                #     # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
                #     @videos =  Video.where(video_category_id: category_id, video_school_id: [0,school_user])
                # else
                #     # @categories =  VideoCategory.where(:video_school_id => [0,current_user.school_id]).order("category_name asc")
                #     # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
                #     @videos =  Video.where(video_category_id: category_id, video_school_id: [0,current_user.school_id])
                # end

                if current_user.user_type == 2
                    school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
                    @categories =  VideoCategory.where(:video_school_id => [0,school_user]).order("category_name asc")
                    # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
                    # default_category = VideoCategory.find_by(category_name: 'Interstride Videos')
                    default_category = VideoCategory.find_by(category_name: 'OPT & CPT')
                    params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : (default_category.try(:id) || @categories.first.id)
                    @category_name = VideoCategory.find_by(id: params[:video_category_id]).category_name
                    @category_id = params[:video_category_id]
                    @videos =  Video.where(video_category_id: params[:video_category_id], video_school_id: [0,school_user]).order('title ASC')
                else
                    @categories =  VideoCategory.where(:video_school_id => [0,current_user.school_id]).order("category_name asc")
                    # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
                    # default_category = VideoCategory.find_by(category_name: 'Interstride Videos')
                    default_category = VideoCategory.find_by(category_name: 'OPT & CPT')
                    params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : (default_category.try(:id) || @categories.first.id)
                    @category_name = VideoCategory.find_by(id: params[:video_category_id]).category_name
                    @category_id = params[:video_category_id]
                    @videos =  Video.where(video_category_id: params[:video_category_id], video_school_id: [0,current_user.school_id]).order('title ASC')
                end
            else
                logger.info "===cat=====there"
                if params[:category].include?('OPT')
                    category_id = VideoCategory.find_by(category_name: 'OPT & CPT').id
                else
                    category_id = VideoCategory.find_by(category_name: params[:category]).id
                end
                @category_name = params[:category]
                if current_user.user_type == 2
                    school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
                    # @categories =  VideoCategory.where(:video_school_id => [0,school_user]).order("category_name asc")
                    # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
                    @videos =  Video.where(video_category_id: category_id, video_school_id: [0,school_user]).order('title ASC')
                else
                    # @categories =  VideoCategory.where(:video_school_id => [0,current_user.school_id]).order("category_name asc")
                    # params[:video_category_id] = params[:video_category_id].present? ? params[:video_category_id] : @categories.first.id
                    @videos =  Video.where(video_category_id: category_id, video_school_id: [0,current_user.school_id]).order('title ASC')
                end
            end
            Rails.logger.debug { "===vids=====#{@videos.inspect}" }
            if @videos.present?
                @youtube_links = @videos.youtube_links
                @web_links = @videos.web_links
                @video_links = @videos.videos
                @docs = @videos.docs
                @ppts = @videos.ppts
                @excel_sheets = @videos.excel_sheets
                @pdfs = @videos.pdfs
                logger.info "====pdf===#{@videos.pdfs.inspect}"
            end
        end

        respond_to do |format|
            format.js
        end
    end

    def order_list
        if current_user.user_type == 2
            school_user = current_user.parent_school_id != 0 ? current_user.parent_school_id : current_user.id
            # @categories =  VideoCategory.where(:video_school_id => [0,school_user]).order("category_name asc")
            # params[:video_category_id] = params[:category_name].present? ? VideoCategory.find_by(category_name: params[:category_name]).try(:id) : @categories.first.id
            # @videos =  Video.where(video_category_id: params[:video_category_id], video_school_id: [0,school_user]).order('title ASC')
            @videos =  Video.where(video_school_id: [0,school_user])
            # @videos = @videos.where("title like ?", "%#{params[:search]}%").order('title ASC')
        else
            # @categories =  VideoCategory.where(:video_school_id => [0,current_user.school_id]).order("category_name asc")
            # params[:video_category_id] = params[:category_name].present? ? VideoCategory.find_by(category_name: params[:category_name]).try(:id) : @categories.first.id
            # @videos =  Video.where(video_category_id: params[:video_category_id], video_school_id: [0,current_user.school_id]).order('title ASC')
            @videos =  Video.where(video_school_id: [0,current_user.school_id])
        end
        if params[:notification_id].present?
            notif = Notification.find(params[:notification_id])
            notif.update_attributes(is_read: true)
        end
        category_id = VideoCategory.find_by(category_name: params[:category_name]).id
        @category_name = params[:category_name]
        if params[:search].present? && !params[:search].blank?
            @videos = @videos.where("title like ? and video_category_id = ?", "%#{params[:search]}%", category_id).order('title ASC')
        else
            @videos = @videos.where(video_category_id: category_id).order('title ASC')
        end
        # @category_name = VideoCategory.find_by(id: params[:video_category_id]).category_name
        # category_id = VideoCategory.find_by(category_name: params[:category]).id
        @category_name = params[:category_name]
        @youtube_links = @videos.youtube_links
        @web_links = @videos.web_links
        @video_links = @videos.videos
        @docs = @videos.docs
        @ppts = @videos.ppts
        @excel_sheets = @videos.excel_sheets
        @pdfs = @videos.pdfs
    end
end
