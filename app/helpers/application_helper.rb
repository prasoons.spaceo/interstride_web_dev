module ApplicationHelper
    def custom_bootstrap_flash
        flash_messages = []
        flash.each do |type, message|
            type = 'success' if type == 'notice'
            type = 'error'   if type == 'alert'
            text = "<script>toastr.#{type}('#{message}');</script>"
            flash_messages << text.html_safe if message
        end
        flash.clear
        flash_messages.join("\n").html_safe
    end

    def active_class(link_path)
  		current_page?(link_path) ? "active" : ""
 	end
end
