class UserMailer < ActionMailer::Base
    default from: 'contact@interstride.com'
    def registration_confirmation(user)
        attachments['Interstride - User Guide.pdf'] = File.read("#{Rails.root}/public/document/Interstride - User Guide.pdf")

        @user = user
        mail(from: 'Interstride <contact@interstride.com>', to: "#{user.full_name} <#{user.email}>", subject: "Welcome to Interstride!")
    end

    def contact_us(contact)
        @contact = contact
        @user = User.find_by(:id => contact.user_id)
        mail(from: 'Interstride <contact@interstride.com>', to: "Interstride <contact@interstride.com>", subject: "Contact Us - Interstride Application")
    end
end
