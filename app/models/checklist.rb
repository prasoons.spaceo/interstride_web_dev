class Checklist < ActiveRecord::Base
	##Relationship
	belongs_to :checklist_subcategory, foreign_key: 'subcategory_id', class_name: 'ChecklistSubcategory'
	has_many :user_checklists,:dependent => :destroy

	##Scope
  	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def self.get_list(current_user)
		categories = ChecklistCategory.where(:user_id => [0,current_user.id],:school_id => [0,current_user.school_id]).order("sort_order asc")
		final_array = Array.new

		categories.each do |c|
			category_hash = Hash.new
			category_hash[:category_id] =c.id
			category_hash[:category_name] =c.category_name
			category_mark = UserChecklistCategory.find_by(:category_id => c.id,:user_id => current_user.id)
			category_hash[:is_mark] = category_mark.blank? == false ? (category_mark.is_mark == true ? 1 : 0) : 0
			category_hash[:user_id] = c.user_id
			if current_user.user_type  == 2
				school_id = current_user.school_id == 0 ? current_user.parent_school_id : current_user.school_id
			else
				school_id = current_user.school_id
			end

			subcategories = ChecklistSubcategory.where(" category_id = #{c.id} AND ((user_id = #{current_user.id} AND  school_id = #{school_id} ) OR (user_id = 0 AND school_id = #{school_id}) OR (user_id = 0 AND school_id = 0) OR (user_id = #{current_user.id} AND school_id = #{current_user.id})) ").order("sort_order asc").uniq

			subcatgories_array = Array.new
			subcategories.each do |s|
				subcategory_hash = Hash.new
				subcategory_hash[:subcategory_id] = s.id
				subcategory_hash[:subcategory_name] = s.subcategory_name
				check_mark = UserSubchecklist.find_by(:subcategory_id => s.id,:user_id => current_user.id)
				subcategory_hash[:is_mark] = check_mark.blank? == false ? (check_mark.is_mark == true ? 1 : 0) : 0
				subcategory_hash[:user_id]=s.user_id
				checklists = Checklist.joins(" left join user_checklists as u on checklists.id = u.checklist_id AND u.user_id=#{current_user.id}").select(" checklists.*,IFNULL(is_mark,0) as is_mark").where(" subcategory_id = #{s.id} AND ((checklists.user_id = #{current_user.id} AND  school_id = #{school_id} ) OR (checklists.user_id = 0 AND school_id = #{school_id}) OR (checklists.user_id = 0 AND school_id = 0) OR (checklists.user_id = #{current_user.id} AND school_id = #{current_user.id}))").order('checklists.id asc').uniq


				subcategory_hash[:checklists] = checklists.uniq
				#Rails.logger.info"=========#{s.subcategory_name}=== hash=#{subcategory_hash.inspect}"
				subcatgories_array.push(subcategory_hash)
				#Rails.logger.info"=========#{s.subcategory_name}=== Array=#{subcategory_hash.inspect}"
			end
			category_hash[:subcategories] = subcatgories_array.uniq
			final_array.push(category_hash)
			#Rails.logger.info"=========#{c.category_name}=== final=#{final_array.inspect}"
		end
		return final_array
	end
end
