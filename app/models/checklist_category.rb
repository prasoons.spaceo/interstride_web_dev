class ChecklistCategory < ActiveRecord::Base

	##Relationship
	has_many :checklist_subcategories ,:dependent => :destroy
	has_many :user_checklist_categories, :dependent => :destroy


	##Scope
  	default_scope -> { where(is_deleted: false) }

  	
	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end
