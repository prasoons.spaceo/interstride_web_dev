class ChecklistSubcategory < ActiveRecord::Base

	##Relationship
	belongs_to :checklist_category, foreign_key: 'category_id', class_name: 'ChecklistCategory'
	has_many :checklists ,:dependent => :destroy
	has_many :user_subchecklists, :dependent => :destroy

	##Scope
  	default_scope -> { where(is_deleted: false) }

  	
	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end
