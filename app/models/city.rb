class City < ActiveRecord::Base

	JSON_LIST = [:id,:city_name]

    ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
	
	def self.sort_order(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		city_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			city_data = City.find_by(:city_name => row["city_name"])
			
			result = false
			if city_data
				last_sort_order = City.where(" sort_order != 1000 ").order("sort_order desc").first.sort_order
				city_data.update_attributes(:sort_order => last_sort_order+1)
				result = true
			end
			
			if result
				success_count += 1
			else
				failed_count += 1
				e_msg = city_data.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				city_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_company_array => city_array)
		end

		Rails.logger.info"==================update_array============#{@data.inspect}"
		return @data
	end

	#Import
	def self.import(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		update_count = 0
		failed_count = 0
		message = []
		@data = {}
		update_array = []
		city_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""

			city_data = City.find_by(:city_name => row["city_name"])
			update = false
			result = false
			if city_data
				update = true
				city_data.update_attributes(:city_name => row["city_name"])
			else
				city = City.new
				city.city_name = row["city_name"]
				if city.save
					result = true
				end
			end

			if update
				update_count += 1 
				
				update_array.push(row)
			end
	
			if result
				success_count += 1
			else
				if update == false
					failed_count += 1
					e_msg = city.errors.full_messages.first
					if message.include? e_msg
					else
						message.push(e_msg)
					end
					city_array.push(row)
				end
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_city_array => city_array,:updated_company_array => update_array)
		 
		end
		Rails.logger.info"==================update_array============#{@data.inspect}"
		return @data
	end
		

  	def self.open_spreadsheet(file)
  		Rails.logger.info "==============#{file.path}"
		case File.extname(file.original_filename)
			when ".csv" then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
			when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
			when ".xlsx" then  Roo::Excelx.new(file.path, nil, :ignore)
		else raise "Unknown file type: #{file.original_filename}"
		end
	end

end
