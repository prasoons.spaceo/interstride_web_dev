class Company < ActiveRecord::Base

	JSON_LIST = [:id,:company_name]

    ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def self.sort_order(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		company_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			company_data = Company.find_by(:company_name => row["company_name"])
			
			result = false
			if company_data
				last_sort_order = Company.where(" sort_order != 1000 ").order("sort_order desc").first.sort_order
				company_data.update_attributes(:sort_order => last_sort_order+1)
				result = true
			end
			
			if result
				success_count += 1
			else
				failed_count += 1
				e_msg = company_data.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				company_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_company_array => company_array)
		end

		Rails.logger.info"==================update_array============#{@data.inspect}"
		return @data
	end

	#Import
	def self.import(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		update_count = 0
		failed_count = 0
		message = []
		@data = {}
		update_array = []
		company_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			company_data = Company.find_by(:company_name => row["company_name"])
			update = false
			result = false
			if company_data
				update = true
				company_data.update_attributes(:company_name => row["company_name"])
			else
				company = Company.new
				company.company_name = row["company_name"]
				if company.save
					result = true
				end
			end
			if update
				update_count += 1 
				e_msg = company.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				update_array.push(row)
			end
	
			if result
				success_count += 1
			else
				failed_count += 1
				e_msg = company.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				company_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_company_array => company_array,:updated_company_array => update_array)
		end

		Rails.logger.info"==================update_array============#{@data.inspect}"
		return @data
	end
		

  	def self.open_spreadsheet(file)
  		Rails.logger.info "==============#{file.path}"
		case File.extname(file.original_filename)
			when ".csv" then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
			when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
			when ".xlsx" then  Roo::Excelx.new(file.path, nil, :ignore)
		else raise "Unknown file type: #{file.original_filename}"
		end
	end

end
