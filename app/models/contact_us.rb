class ContactUs < ActiveRecord::Base
	JSON_LIST = [:id,:user_id,:topic,:comment,:created_at,:updated_at,:is_deleted]
    ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end