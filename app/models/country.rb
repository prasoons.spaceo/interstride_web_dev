class Country < ActiveRecord::Base
	## Associations ##
	has_attached_file :flag, styles: { thumb: "85x52#", medium: "255x171#"}, default_url: ""

	## Validations ##
	validates_attachment :flag, content_type: { content_type: [/\Aimage\/.*\z/] }

	##JSON Response
	JSON_LIST = [:id,:name,:points]

	 ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end
