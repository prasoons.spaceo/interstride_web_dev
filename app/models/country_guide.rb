class CountryGuide < ActiveRecord::Base
	FileTypes = ["country_details"]

	JSON_LIST = [:id,:country_name,:job_site_link,:cia_link,:numbeo,:hofstede_insights,:us_travel_advisory,:cdc_gov,:immegration_website,:lonely_planet]

	# has_many :country_jobsites

	##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def self.get_list(params)
		wildcard_search = "#{params[:country_name]}%"
		guides = CountryGuide.joins(" left join apps_countries as a on a.country_name=country_guides.country_name").select("LOWER(country_code) as country_code,country_guides.*").where("country_guides.country_name LIKE ?",wildcard_search ).as_json(:only => CountryGuide::JSON_LIST)
		return guides
	end

	def as_json(options = {})
		json =  super(options)
		json[:extra_jobsites] = CountryJobsite.where(:country_guide_id => self.id)
		json[:country_code] = self.country_code ? self.country_code : ""
		json
	end


  	#Import
	def self.import(params)
		
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		country_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			country = CountryGuide.new
			country.country_name = row["Country Name"]
			country.job_site_link = row["Job Site Link"] == "NULL" ? " " : row["Job Site Link"]
			country.cia_link = row["CIA Link"]
			country.numbeo = row["Numbeo"]
			country.hofstede_insights = row["Hofstede Insights"]
			country.us_travel_advisory = row["US State department travel advisory"]
			country.cdc_gov = row["CDC.gov"]
			country.immegration_website = row["National immigration website"]
			country.lonely_planet = row["Lonely Planet"]
			
			if country.save
				success_count += 1
			else
				failed_count += 1
				e_msg = country.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				country_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_country_array => country_array)
		 
		end

		return @data
	end	

	def self.open_spreadsheet(file)
		case File.extname(file.original_filename)
			when ".csv" then Roo::Csv.new(file.path, packed: nil, file_warning: :ignore)
			when ".xls" then Roo::Excel.new(file.path, packed: nil, file_warning: :ignore)
			when ".xlsx" then  Roo::Excelx.new(file.path, packed: nil, file_warning: :ignore)
		else raise "Unknown file type: #{file.original_filename}"
		end
	end
end