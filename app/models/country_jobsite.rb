class CountryJobsite < ActiveRecord::Base

	##Relationship
	# belongs_to :country_guides

	##Scope
  	default_scope -> { where(is_deleted: false) }

  	
	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end
