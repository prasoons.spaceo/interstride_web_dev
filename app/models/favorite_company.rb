class FavoriteCompany < ActiveRecord::Base


	##JSON Response
	JSON_LIST = [:id,:company_id,:company_name]

	 ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def self.get_list(current_user)
		companies = FavoriteCompany.where(:user_id => current_user.id)
		return companies
	end
	
end