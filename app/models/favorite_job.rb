class FavoriteJob < ApplicationRecord
    ## Associations ##
    belongs_to :user

    def self.get_list(current_user)
		jobs = FavoriteJob.where(:user_id => current_user.id)
		return jobs
	end
end
