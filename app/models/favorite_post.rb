class FavoritePost < ActiveRecord::Base
	belongs_to :user_post
	belongs_to :user

	##JSON Response
	JSON_LIST = [:id,:user_post_id,:user_id,:is_favorite]

	 ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def self.get_list(current_user,params)
		limit = params[:limit] ? params[:limit] : 10
		offset = params[:offset] ? params[:offset] : 0

		total_count =  UserPost.joins(" inner join users as a on a.id=user_posts.posted_by inner join favorite_posts as b on b.user_post_id = user_posts.id").where("b.user_id = #{current_user.id} AND is_favorite = true ").count
		favorite = UserPost.joins(" inner join users as a on a.id=user_posts.posted_by inner join favorite_posts as b on b.user_post_id = user_posts.id").select("user_posts.*,a.full_name as user_name	").where("b.user_id = #{current_user.id} AND is_favorite = true ").order("user_posts.created_at desc").limit(limit).offset(offset)
		

		favorite = favorite.as_json(only: [:id,:title,:description,:posted_by,:post_image,:created_at,:share_link,:post_type,:user_name,:search_json,:like_count,:comment_count], :methods => [:post_image_url],:current_user => current_user)

		return favorite,total_count
	end
	
end