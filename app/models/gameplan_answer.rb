class GameplanAnswer < ActiveRecord::Base

	##JSON Response
	JSON_LIST = [:id,:answer,:points,:option_id,:question_id]

	##Relationship
	belongs_to :gameplan_question ,:foreign_key => :question_id,:dependent => :destroy
	belongs_to :gameplan_option ,:foreign_key => :option_id,:dependent => :destroy

	 ##Scope
	default_scope -> { where(is_deleted: false) }

	##Validates
	validates :question_id, presence: true

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

end
