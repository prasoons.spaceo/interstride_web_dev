class GameplanOption < ActiveRecord::Base

	##JSON Response
	JSON_LIST = [:id,:option,:question_id]

	##Relationship
	belongs_to :gameplan_question,:foreign_key => :question_id ,:dependent => :destroy
	has_many  :gameplan_answers ,:dependent => :destroy

	 ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

  	def as_json(option = {})
  		json = super(option)
  		json[:answers] = GameplanAnswer.where(:option_id => self.id).as_json(:only => GameplanAnswer::JSON_LIST)
  		json
  	end
end