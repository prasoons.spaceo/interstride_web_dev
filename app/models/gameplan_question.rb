class GameplanQuestion < ActiveRecord::Base
  QuestionTypes = ["drop-down", "single-choice", "multiple-choice", "selection"]
  QuestionDegree = ["both", "undergraduate", "graduate"]


	##JSON Response
	JSON_LIST = [:id,:question,:undergraduate_points,:graduate_points,:category_id,:question_type,:degree,:response]

	##Relationship
	has_many :gameplan_options ,:dependent => :destroy
	has_many :gameplan_answers, foreign_key: :question_id,:dependent => :destroy
    has_many :user_answers, :dependent => :destroy
	belongs_to :question_categories,:foreign_key => :category_id ,:dependent => :destroy
    accepts_nested_attributes_for :user_answers, allow_destroy: true

	##Scope
  	default_scope -> { where(is_deleted: false) }

  	def as_json(options = {})
  		json =  super(options)
  		json[:category_name] = QuestionCategory.find_by(:id => self.category_id).category_name
  		if self.question_type == "selection"
  			json[:options] = GameplanOption.where(:question_id => self.id).as_json(:only => GameplanOption::JSON_LIST)
  		else
  			json[:options] = GameplanAnswer.where(:question_id => self.id).as_json(:only => GameplanAnswer::JSON_LIST)
  		end
  		json
  	end

   ##Scope
  default_scope -> { where(is_deleted: false) }

  ##Methods
  def destroy
    update_attribute(:is_deleted, true);
  end
end
