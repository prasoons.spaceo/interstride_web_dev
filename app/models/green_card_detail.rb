class GreenCardDetail < ActiveRecord::Base
	require 'csv'

	JSON_LIST = [:id,:employer_id,:company_id,:employer_name,:job_title_id,:job_title,:industry_id,:industry,:city_id,:worksite_city,:state_id,:worksite_state,:worksite_postal_code,:website,:crunchbase_link,:wikipedia_link,:indeed_link,:linkedin_link,:employees,:company_description,:latitude,:longitude,:glassdoor_link]

	 ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def as_json(options = {})
  		json = super(options)
  		# json[:wage_rate_of_pay_from] = (self.wage_rate_of_pay_from.gsub(/[$,]/,'')).to_i
  		if options[:favorite] != false
	  		favorite = FavoriteCompany.find_by(:company_id => self.company_id,:user_id => options[:current_user])
	  		json[:is_favorite] = !favorite.blank?  ? true : false
	  		json[:favorite_id] = !favorite.blank?  ?  favorite.id : 0
	  	end

  		if options[:cSponser] != true
	  		json[:crunchbase_link] = (self.crunchbase_link == "0.0") ? "" : self.crunchbase_link
	  		json[:wikipedia_link] = (self.wikipedia_link == "0.0") ? "" : self.wikipedia_link
	  		json[:indeed_link] = (self.indeed_link == "0.0") ? "" : self.indeed_link
	  		json[:linkedin_link] = (self.linkedin_link == "0.0") ? "" : self.linkedin_link
	  		# json[:company_logo] = Rails.application.secrets.BASE_URL_IMAGE + "/assets/CompanyLogos/"+self.employer_id.to_s+".png"
			json[:company_logo] = File.exist?(Rails.root + "public/assets/CompanyLogos/#{self.employer_id}.png") ? Rails.application.secrets.BASE_URL_IMAGE + "/assets/CompanyLogos/"+self.employer_id.to_s+".png" : Rails.application.secrets.BASE_URL_IMAGE + "/assets/company_logo.png"
	  	end
  		json
  	end

  	#Import
	def self.import(params)

		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		company_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			university = GreenCardDetail.new

			#Get links From H1B data (company_details)
			company_details = CompanyDetail.find_by(:employer_id => row["Employer ID"])

			# company_id = Company.find_by(:company_name => row["EMPLOYER_NAME"])
			university.company_id = company_details.blank? == false ? company_details.company_id : 0

			university.employer_id = row["Employer ID"]
			university.employer_name = row["EMPLOYER_NAME"]
			university.wage_rate_of_pay_from = row["WAGE"]
			university.worksite_country = row["COUNTRY_OF_CITIZENSHIP"]
			university.university = row["University"]

			job_title_id = JobTitle.find_by(:job_title_name => row["Job Title"])
			university.job_title_id = job_title_id ? job_title_id.id : 0
			university.job_title = row["Job Title"]

			industry_id = Industry.find_by(:industry_name => row["Industry"])
			university.industry_id = industry_id ? industry_id.id : 0
			university.industry = row["Industry"]

			city_id = City.find_by(:city_name => row["City"])
			university.city_id = city_id ? city_id.id : 0
			university.worksite_city = row["City"]

			state_id = State.find_by(:state_name => row["State"])
			university.state_id = state_id ? state_id.id : 0
			university.worksite_state = row["State"]

			university.worksite_postal_code = row["JOB_INFO_WORK_POSTAL_CODE"]

			university.latitude = company_details.blank? == false ? company_details.latitude  : '0.0'
			university.longitude = company_details.blank? == false ? company_details.longitude  : '0.0'
			university.website = company_details.blank? == false ? company_details.website  : ''
			university.crunchbase_link = company_details.blank? == false ? company_details.crunchbase_link : ''
			university.wikipedia_link = company_details.blank? == false ? company_details.wikipedia_link : ''
			university.indeed_link = company_details.blank? == false ? company_details.indeed_link : ''
			university.linkedin_link = company_details.blank? == false ? company_details.linkedin_link : ''
			university.glassdoor_link = company_details.blank? == false ? company_details.glassdoor_link : ''
			university.employees = company_details.blank? == false ? company_details.employees : ''
			university.company_description = company_details.blank? == false ? company_details.company_description : ''

			if university.save
				success_count += 1
			else
				failed_count += 1
				e_msg = university.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				company_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_company_array => company_array)

		end
		# end
		return @data
	end

	def self.university_import(params)

		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		country_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			country = University.new
			country.name = row["Name"]
			if country.save
				success_count += 1
			else
				failed_count += 1
				e_msg = country.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				country_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_country_array => country_array)

		end

		return @data
	end

	def self.birth_country_import(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		country_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			country = BirthCountry.new
			country.name = row["Name"]

			if country.save
				success_count += 1
			else
				failed_count += 1
				e_msg = country.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				country_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_country_array => country_array)

		end

		return @data
	end


  	def self.open_spreadsheet(file)
  		Rails.logger.info "==============#{file.path}"
		case File.extname(file.original_filename)
			when ".csv" then Roo::Csv.new(file.path, packed: nil, file_warning: :ignore)
			when ".xls" then Roo::Excel.new(file.path, packed: nil, file_warning: :ignore)
			when ".xlsx" then  Roo::Excelx.new(file.path, packed: nil, file_warning: :ignore)
		else raise "Unknown file type: #{file.original_filename}"
		end
	end



end
