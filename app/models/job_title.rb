class JobTitle < ActiveRecord::Base

	JSON_LIST = [:id,:job_title_name]

    ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def self.sort_order(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		job_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			company_data = JobTitle.find_by(:job_title_name => row["job_title_name"])
			
			result = false
			if company_data
				last_sort_order = JobTitle.where(" sort_order != 1000 ").order("sort_order desc").first.sort_order
				company_data.update_attributes(:sort_order => last_sort_order+1)
				result = true
			end
			
			if result
				success_count += 1
			else
				failed_count += 1
				e_msg = company_data.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				job_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_company_array => job_array)
		end

		Rails.logger.info"==================update_array============#{@data.inspect}"
		return @data
	end


	#Import
	def self.import(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		job_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			industry = JobTitle.new
			industry.job_title_name = row["job_title_name"]
	
			if industry.save
				success_count += 1
			else
				failed_count += 1
				e_msg = industry.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				job_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_job_array => job_array)
		 
		end
		return @data
	end
		

  	def self.open_spreadsheet(file)
  		Rails.logger.info "==============#{file.path}"
		case File.extname(file.original_filename)
			when ".csv" then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
			when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
			when ".xlsx" then  Roo::Excelx.new(file.path, nil, :ignore)
		else raise "Unknown file type: #{file.original_filename}"
		end
	end

end
