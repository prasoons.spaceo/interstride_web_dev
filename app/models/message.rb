class Message < ActiveRecord::Base

	#Image 
	has_attached_file :message_media,
	styles: { thumb: "50x50", medium: "300x300"}
	# default_url: "default-profile.png"

	#Relationships
  	has_one   :message_topic,:foreign_key => :message_topic_id, :dependent => :destroy

	MESSAGE_JSON_LIST = [:id,:message_topic_id, :sender_id, :receiver_id, :message, :mtype, :message_media,:created_at]

	##Validations
    validates_attachment :message_media, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

	##Scope
    default_scope -> { where(is_deleted: false) }

    def message_media_url
    	message_media.url(:thumb)
    end

	#Methods
	def destroy
	   update_attribute(:is_deleted, true);
	end

	def as_json(options ={})
		json = super(options)
		if self.mtype == "media"
			json[:message_media] = self.message_media.url
		end
		if self.sender_id != 0
			sender = User.find_by(:id => self.sender_id)
			json[:sender_name] = sender.full_name
			json[:sender_image] = sender.user_profile ? sender.user_profile.url : ""

		end
		if self.receiver_id != 0
			receiver =  User.find_by(:id => self.receiver_id)
			json[:receiver_name] = receiver.full_name
			json[:receiver_image] = receiver.user_profile.url
		end
		json[:message_date] = self.created_at
		json[:created_at] = self.created_at.to_i
		json
	end
end
