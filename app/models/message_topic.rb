class MessageTopic < ActiveRecord::Base
	#Relationships
  	has_many   :messages,:foreign_key => :message_topic_id, :dependent => :destroy

	TOPIC_JSON_LIST = [:id,:user_id,:topic_name,:topic_description,:created_at,:updated_at]

	##Scope
    default_scope -> { where(is_deleted: false) }

	#Methods
	def destroy
	   update_attribute(:is_deleted, true);
	end

	def message_url
	    message.url
	end
	
	def as_json(options={})
		json = super(options)
		# if self.user_id
  #   		json[:full_name] = User.find_by(:id => self.user_id).full_name  
  #   	end
    	json[:created_at] = self.created_at.to_i
    	json[:updated_at] = self.updated_at.to_i
    	json
	end
end
