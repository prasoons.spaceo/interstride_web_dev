class NetworkTracker < ActiveRecord::Base

	##Relationshoip
	belongs_to :user 

	JSON_LIST = [:id,:company_id,:company_name, :user_id, :contact_name, :contact_email, :appointment_date, :notes,:followup_notes,:created_at,:updated_at,:company_logo,:employer_name,:employer_id,:google_calendar_id,:icalendar_id,:start_time,:end_time]

	##Scope
	default_scope -> { where(is_deleted: false) }

	#Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def as_json(options = {})
		json = super(options)
		json[:start_time] = self.start_time ? self.start_time.strftime("%H:%M:%S") : ""
		json[:end_time] = self.end_time ? self.end_time.strftime("%H:%M:%S") : ""
		json[:employer_id] = self.employer_id ? self.employer_id : ""
		json[:employer_name] = self.employer_name ? self.employer_name : ""
		json[:company_logo] = self.employer_id ? Rails.application.secrets.BASE_URL_IMAGE + "/assets/CompanyLogos/"+self.employer_id.to_s+".png" : ""
		json
	end
end
