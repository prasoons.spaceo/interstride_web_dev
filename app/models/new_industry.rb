class NewIndustry < ActiveRecord::Base

	JSON_LIST = [:id,:industry_name]

    ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	#Import
	def self.import(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		industry_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			
			industry_data = NewIndustry.find_by(:industry_name => row["industry_name"])
			result = false
			if industry_data
				result = true
				industry_data.update_attributes(:industry_name => row["industry_name"])
			else
				industry = NewIndustry.new
				industry.industry_name = row["industry_name"]
				if industry.save
					result = true
				end
			end
			if result
				success_count += 1
			else
				failed_count += 1
				e_msg = industry.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				industry_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_industry_array => industry_array)
		 
		end
		return @data
	end
		

  	def self.open_spreadsheet(file)
  		Rails.logger.info "==============#{file.path}"
		case File.extname(file.original_filename)
			when ".csv" then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
			when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
			when ".xlsx" then  Roo::Excelx.new(file.path, nil, :ignore)
		else raise "Unknown file type: #{file.original_filename}"
		end
	end

end
