class Notification < ActiveRecord::Base
	self.inheritance_column = nil

	##Scope
	default_scope -> { where(is_deleted: false) }
	scope :unread, -> { where(is_read: false) }

	#Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def self.get_list(current_user)
		notifications = Notification.where(:user_id => current_user.id).order("created_at desc")

		return notifications
	end

	def self.read_unread(params)
		notification =  Notification.find_by(:id => params[:notification_id])
		notification.update_attributes(:is_read => params[:status])
		return notification
	end
end
