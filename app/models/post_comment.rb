class PostComment < ActiveRecord::Base

	#Relationships
  	belongs_to   :user,:foreign_key => :user_id, :dependent => :destroy
  	belongs_to   :user_post, :foreign_key => :user_post_id, :dependent => :destroy

  	JSON_LIST = [:id,:user_post_id,:user_id,:comment,:created_at]

	##Scope
	default_scope -> { where(is_deleted: false) }


	#Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def as_json(options={})
		json = super(options)
		if self.user_id
			json[:user_name] = User.find_by(:id => self.user_id).full_name
		end
		json[:created_at] = self.created_at.to_i
		json
	end
end