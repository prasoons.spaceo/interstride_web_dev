class PostLike < ActiveRecord::Base

	#Relationships
  	belongs_to   :user,:foreign_key => :user_id, :dependent => :destroy
  	belongs_to   :user_post, :foreign_key => :user_post_id, :dependent => :destroy

  	JSON_LIST = [:id,:user_post_id,:post_id,:like,:created_at]

	##Scope
	default_scope -> { where(is_deleted: false) }

	#Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end