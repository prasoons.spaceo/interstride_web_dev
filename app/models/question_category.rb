class QuestionCategory < ActiveRecord::Base

	##Relationship
	has_many :question_categories ,:dependent => :destroy

	##Scope
  	default_scope -> { where(is_deleted: false) }

  	
	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end
