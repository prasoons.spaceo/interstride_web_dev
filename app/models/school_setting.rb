class SchoolSetting < ActiveRecord::Base
	

    ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end