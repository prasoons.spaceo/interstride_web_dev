class SchoolUser < ActiveRecord::Base

	has_one :user, :dependent => :destroy 
	DegreeTypes = ["graduate", "undergraduate"]
	
	##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		
		update_attribute(:is_deleted, true);
		
	end
end