class SendNotification < ActiveRecord::Base

	JSON_LIST = [:id, :notification]

	 ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end
