class State < ActiveRecord::Base

	JSON_LIST = [:id,:state_name]

    ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def self.sort_order(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		state_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			state_data = State.find_by(:state_name => row["state_name"])
			
			result = false
			if state_data
				last_sort_order = State.where(" sort_order != 1000 ").order("sort_order desc").first.sort_order
				Rails.logger.info"=========last_sort_order======#{last_sort_order}"
				
				state_data.update_attributes(:sort_order => last_sort_order+1)
				result = true
			end
			
			if result
				success_count += 1
			else
				failed_count += 1
				e_msg = state_data.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				state_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_company_array => state_array)
		end

		Rails.logger.info"==================update_array============#{@data.inspect}"
		return @data
	end


	#Import
	def self.import(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		state_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			
			state_data = State.find_by(:state_name => row["state_name"])
			result = false
			if state_data
				result = true
				state_data.update_attributes(:state_name => row["state_name"])
			else
				state = State.new
				state.state_name = row["state_name"]
				if state.save
					result = true
				end
			end
	
			if result
				success_count += 1
			else
				failed_count += 1
				e_msg = state.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				state_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_state_array => state_array)
		 
		end
		return @data
	end
		

  	def self.open_spreadsheet(file)
  		Rails.logger.info "==============#{file.path}"
		case File.extname(file.original_filename)
			when ".csv" then Roo::Csv.new(file.path, packed: nil, file_warning: :ignore)
			when ".xls" then Roo::Excel.new(file.path,  packed: nil, file_warning: :ignore)
			when ".xlsx" then  Roo::Excelx.new(file.path, packed: nil, file_warning: :ignore)
		else raise "Unknown file type: #{file.original_filename}"
		end
	end

end
