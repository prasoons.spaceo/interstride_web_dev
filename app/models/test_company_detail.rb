class TestCompanyDetail < ActiveRecord::Base
	require 'csv'    
	FileTypes = ["company_detail", "company", "city", "state","industry","job_title"]

	JSON_LIST = [:id,:petition_id,:employer_id,:company_id,:employer_name,:job_title_id,:job_title,:industry_id,:industry,:wage_rate_of_pay_from,:city_id,:worksite_city,:worksite_country,:state_id,:worksite_state,:headquaters,:worksite_postal_code,:website,:crunchbase_link,:wikipedia_link,:indeed_link,:linkedin_link,:no_of_petition,:employees,:company_description,:company_logo,:revenue,:company_ceo,:news_link,:latitude,:longitude]

	 ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

  	def as_json(options = {})
  		json = super(options)
  		# json[:wage_rate_of_pay_from] = (self.wage_rate_of_pay_from.gsub(/[$,]/,'')).to_i
  		json[:crunchbase_link] = (self.crunchbase_link == "0.0") ? "" : self.crunchbase_link
  		json[:wikipedia_link] = (self.wikipedia_link == "0.0") ? "" : self.wikipedia_link
  		json[:indeed_link] = (self.indeed_link == "0.0") ? "" : self.indeed_link
  		json[:linkedin_link] = (self.linkedin_link == "0.0") ? "" : self.linkedin_link
  		json[:company_logo] = Rails.application.secrets.BASE_URL_IMAGE + "/assets/CompanyLogos/"+self.employer_id.to_s+".png"
  		json
  	end

  	#Import
	def self.import(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		company_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			company = TestCompanyDetail.new

			company_id = Company.find_by(:company_name => row["EMPLOYER_NAME"])
			company.company_id = company_id ? company_id.id : 0
			# Rails.logger.info"=========row======#{row}"
			company.petition_id = row["Petition ID"]
			company.employer_id = row["employer_id"]
			company.employer_name = row["EMPLOYER_NAME"]
			company.headquaters = row["Headquaters"]

			job_title_id = JobTitle.find_by(:job_title_name => row["Job Title"])
			company.job_title_id = job_title_id ? job_title_id.id : 0
			company.job_title = row["Job Title"]
			
			industry_id = Industry.find_by(:industry_name => row["Industry"])
			company.industry_id = industry_id ? industry_id.id : 0
			company.industry = row["Industry"]

			city_id = City.find_by(:city_name => row["WORKSITE_CITY"])
			company.city_id = city_id ? city_id.id : 0
			company.worksite_city = row["WORKSITE_CITY"]

			state_id = State.find_by(:state_name => row["WORKSITE_STATE"])
			company.state_id = state_id ? state_id.id : 0
			company.worksite_state = row["WORKSITE_STATE"]

			company.wage_rate_of_pay_from = row["WAGE_RATE_OF_PAY_FROM (y)"]
			company.worksite_country = row["WORKSITE_COUNTY"]
			company.worksite_postal_code = row["WORKSITE_POSTAL_CODE"]
			company.website = row["Website"]
			company.crunchbase_link = row["Crunchbase Link"]
			company.wikipedia_link = row["Wikipedia Link"]
			company.indeed_link = row["Indeed Link"]
			company.linkedin_link = row["Linkedin Link"]
			company.no_of_petition = row["Number of Petitions"]
			company.employees = row["Number of Employees"]
			company.company_description = row["Company Description"]

			if company.save
				success_count += 1
			else
				failed_count += 1
				e_msg = company.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				company_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_company_array => company_array)
		 
		end
		# end
		return @data
	end	
	# def self.import(params)
	# 	success_count = 0
	# 	failed_count = 0
	# 	message = []
	# 	@data = {}
	# 	company_array = []
	# 	11.upto(129) do |i|
	# 		file_name = "#{Rails.root}/public/import_data/App Export_v7_4 (copy)_"+i.to_s+".csv"
	# 		Rails.logger.info"=========#{file_name}"
		
	# 		CSV.foreach(file_name, :headers => true) do |row|
	# 			e_msg = ""
	# 			company = TestCompanyDetail.new

	# 			company_id = Company.find_by(:company_name => row[2])
	# 			company.company_id = company_id ? company_id.id : 0
	# 			# Rails.logger.info"=========row======#{row}"
	# 			company.petition_id = row[0]
	# 			company.employer_id = row[1]
	# 			company.employer_name = row[2]
	# 			company.headquaters = row[3]

	# 			job_title_id = JobTitle.find_by(:job_title_name => row[4])
	# 			company.job_title_id = job_title_id ? job_title_id.id : 0
	# 			company.job_title = row[4]
				
	# 			industry_id = Industry.find_by(:industry_name => row[5])
	# 			company.industry_id = industry_id ? industry_id.id : 0
	# 			company.industry = row[5]

	# 			city_id = City.find_by(:city_name => row[7])
	# 			company.city_id = city_id ? city_id.id : 0
	# 			company.worksite_city = row[7]

	# 			state_id = State.find_by(:state_name => row[9])
	# 			company.state_id = state_id ? state_id.id : 0
	# 			company.worksite_state = row[9]

	# 			company.wage_rate_of_pay_from = row[6]
	# 			company.worksite_country = row[8]
	# 			company.worksite_postal_code = row[10]
	# 			company.website = row[11]
	# 			company.crunchbase_link = row[12]
	# 			company.wikipedia_link = row[13]
	# 			company.indeed_link = row[14]
	# 			company.linkedin_link = row[15]
	# 			company.no_of_petition = row[16]
	# 			company.employees = row[17]
	# 			company.company_description = row[18]

	# 			if company.save
	# 				success_count += 1
	# 			else
	# 				failed_count += 1
	# 				e_msg = company.errors.full_messages.first
	# 				if message.include? e_msg
	# 				else
	# 					message.push(e_msg)
	# 				end
	# 				company_array.push(row)
	# 			end
	# 			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_company_array => company_array)
	# 		end
	# 	end
	# 	return @data
	# end

  	def self.open_spreadsheet(file)
  		Rails.logger.info "==============#{file.path}"
		case File.extname(file.original_filename)
			when ".csv" then Roo::Csv.new(file.path, packed: nil, file_warning: :ignore)
			when ".xls" then Roo::Excel.new(file.path, packed: nil, file_warning: :ignore)
			when ".xlsx" then  Roo::Excelx.new(file.path, packed: nil, file_warning: :ignore)
		else raise "Unknown file type: #{file.original_filename}"
		end
	end
end