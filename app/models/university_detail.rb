class UniversityDetail < ActiveRecord::Base
	require 'csv'

	JSON_LIST = [:id,:employer_id,:employer_name,:job_title_id,:job_title,:industry_id,:industry,:city_id,:worksite_city,:state_id,:worksite_state,:worksite_postal_code,:website,:crunchbase_link,:wikipedia_link,:indeed_link,:linkedin_link,:employees,:company_description,:latitude,:longitude,:glassdoor_link, :company_id]

	 ##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def as_json(options = {})
  		json = super(options)
  		# json[:wage_rate_of_pay_from] = (self.wage_rate_of_pay_from.gsub(/[$,]/,'')).to_i
  		if options[:favorite] != false
	  		favorite = FavoriteCompany.find_by(:company_id => self.company_id,:user_id => options[:current_user])
	  		json[:is_favorite] = !favorite.blank?  ? true : false
	  		json[:favorite_id] = !favorite.blank?  ?  favorite.id : 0
	  	end

  		if options[:cSponser] != true
	  		json[:crunchbase_link] = (self.crunchbase_link == "0.0") ? "" : self.crunchbase_link
	  		json[:wikipedia_link] = (self.wikipedia_link == "0.0") ? "" : self.wikipedia_link
	  		json[:indeed_link] = (self.indeed_link == "0.0") ? "" : self.indeed_link
	  		json[:linkedin_link] = (self.linkedin_link == "0.0") ? "" : self.linkedin_link
	  		# json[:company_logo] = Rails.application.secrets.BASE_URL_IMAGE + "/assets/CompanyLogos/"+self.employer_id.to_s+".png"
			json[:company_logo] = File.exist?(Rails.root + "public/assets/CompanyLogos/#{self.employer_id}.png") ? Rails.application.secrets.BASE_URL_IMAGE + "/assets/CompanyLogos/"+self.employer_id.to_s+".png" : Rails.application.secrets.BASE_URL_IMAGE + "/assets/company_logo.png"
	  	end
  		json
  	end


  	#Import
	def self.import(params)

		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		company_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			green_card = UniversityDetail.new

			#Get links From H1B data (company_details)
			company_details = CompanyDetail.find_by(:employer_id => row["Employer ID"])

			# company_id = Company.find_by(:company_name => row["EMPLOYER_NAME"])
			green_card.company_id = company_details.blank? == false ? company_details.company_id : 0

			green_card.employer_id = row["Employer ID"]
			green_card.employer_name = row["EMPLOYER_NAME"]
			green_card.graduation_year = row["Graduation Year"]
			green_card.emp_function = row["Function"]

			job_title_id = JobTitle.find_by(:job_title_name => row["Job Title"])
			green_card.job_title_id = job_title_id ? job_title_id.id : 0
			green_card.job_title = row["Job Title"]

			industry_id = Industry.find_by(:industry_name => row["Industry"])
			green_card.industry_id = industry_id ? industry_id.id : 0
			green_card.industry = row["Industry"]

			city_id = City.find_by(:city_name => row["Job City"])
			green_card.city_id = city_id ? city_id.id : 0
			green_card.worksite_city = row["Job City"]

			state_id = State.find_by(:state_name => row["State"])
			green_card.state_id = state_id ? state_id.id : 0
			green_card.worksite_state = row["State"]

			green_card.worksite_postal_code = row["US Zip Code"]

			green_card.latitude = company_details.blank? == false ? company_details.latitude  : '0.0'
			green_card.longitude = company_details.blank? == false ? company_details.longitude  : '0.0'
			green_card.website = company_details.blank? == false ? company_details.website  : ''
			green_card.crunchbase_link = company_details.blank? == false ? company_details.crunchbase_link : ''
			green_card.wikipedia_link = company_details.blank? == false ? company_details.wikipedia_link : ''
			green_card.indeed_link = company_details.blank? == false ? company_details.indeed_link : ''
			green_card.linkedin_link = company_details.blank? == false ? company_details.linkedin_link : ''
			green_card.glassdoor_link = company_details.blank? == false ? company_details.glassdoor_link : ''
			green_card.employees = company_details.blank? == false ? company_details.employees : ''
			green_card.company_description = company_details.blank? == false ? company_details.company_description : ''

			if green_card.save
				success_count += 1
			else
				failed_count += 1
				e_msg = green_card.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				company_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_company_array => company_array)

		end
		# end
		return @data
	end

	def self.graduation_import(params)

		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		country_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			country = GraduationYear.new
			country.year = row["Year"]
			if country.save
				success_count += 1
			else
				failed_count += 1
				e_msg = country.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				country_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_country_array => country_array)

		end

		return @data
	end

	def self.function_import(params)
		spreadsheet = open_spreadsheet(params[:file])
		header = spreadsheet.row(1)
		success_count = 0
		failed_count = 0
		message = []
		@data = {}
		country_array = []
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			e_msg = ""
			country = EmpFunction.new
			country.name = row["Name"]

			if country.save
				success_count += 1
			else
				failed_count += 1
				e_msg = country.errors.full_messages.first
				if message.include? e_msg
				else
					message.push(e_msg)
				end
				country_array.push(row)
			end
			@data.merge!(:failed_count => failed_count,:messages => message,:success_count=> success_count , :failed_country_array => country_array)
		end

		return @data
	end


  	def self.open_spreadsheet(file)
  		Rails.logger.info "==============#{file.path}"
		case File.extname(file.original_filename)
			when ".csv" then Roo::Csv.new(file.path, packed: nil, file_warning: :ignore)
			when ".xls" then Roo::Excel.new(file.path, packed: nil, file_warning: :ignore)
			when ".xlsx" then  Roo::Excelx.new(file.path, packed: nil, file_warning: :ignore)
		else raise "Unknown file type: #{file.original_filename}"
		end
	end
end
