class User < ActiveRecord::Base
    devise :database_authenticatable, :registerable, :validatable, :recoverable, :rememberable, :trackable, authentication_keys: [:email], case_insensitive_keys: [:email]
    # attr_accessor :password, :password_confirmation
  # require 'securerandom'
  # # acts_as_token_authenticatable
  # before_save :set_auth_token
  #
  # # Include default devise modules. Others available are:
  # # :confirmable, :lockable, :timeoutable and :omniauthable
  #
   validates_uniqueness_of :email, :scope => [:user_type, :is_deleted]
   validates_presence_of   :email, if: :email_required?
   validates_format_of     :email, with: Devise.email_regexp, allow_blank: true, if: :email_changed?

   validates_presence_of     :password , if: :id_present?
   validates_confirmation_of :password , if: :id_present?
   validates_length_of       :password, within: Devise.password_length, allow_blank: false, if: :id_present?
  #

  ## Associations ##
  has_attached_file :user_profile, styles: { thumb: "50x50", medium: "300x300"}, default_url: ""
  has_attached_file :resume#, styles: { thumb: "85x52#", medium: "255x171#"}, default_url: ""

  ## Validations ##
  validates_attachment :user_profile, content_type: { content_type: [/\Aimage\/.*\z/] }
  # validates_attachment :resume, content_type: { content_type: [ "application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ], message: 'Invalid format' }
  validate :resume_content_type_validate

  #
  #Relationships
  has_one   :user_device,:foreign_key => :user_id, :dependent => :destroy
  has_many  :user_checklists,:foreign_key => :user_id, :dependent => :destroy
  belongs_to :school_user,:class_name => "SchoolUser", :foreign_key => :school_user_email, primary_key: :user_email
  has_many  :posts, :foreign_key => :posted_by, :dependent => :destroy
  has_many  :network_trackers, :foreign_key => :user_id, :dependent => :destroy
  has_many  :delete_school_posts, :foreign_key => :user_id, :dependent => :destroy
  has_many :favorite_posts, dependent: :destroy
  has_many :favorite_jobs, dependent: :destroy

  ## Scopes ##
  default_scope -> { where(is_deleted: false) }


  private
  def resume_content_type_validate
      if self.resume.present?
      # logger.info "==1====Self----#{self.inspect}"
          content_type = [ "application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ]
          return if content_type.include?(self.resume_content_type)
          unless content_type.include?(self.resume_content_type)
              errors[:base] << "Resume Document Format Invalid" # or errors.add
              return
          end
      end
  end

  def id_present?
      !id.present?
  end

  def user_profile_url
    user_profile.url
  end

  def is_word_document?
    self.resume_content_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || self.resume_content_type == 'application/msword'
  end

  def is_pdf?
    self.resume_content_type == 'application/pdf'
  end

  #
  # def email_activate
  #   self.is_email_verified = true
  #   save!
  # end
  #
  protected

  #From Devise module Validatable
  # def password_required?
  #     logger.info { "-======presernt======#{password.present?}=======per=====#{persisted?}" }
  #   !persisted? || (password.present? && password_confirmation.present?)
  # end
  #

 #  def password_required?
 #   !persisted? || !password.nil? || !password_confirmation.nil?
 # end


  #From Devise module Validatable
  def email_required?
    true
  end

  # def send_devise_notification(notification, *args)
  #     devise_mailer.send(notification, self, *args).deliver_now
  # end

  #
  #
  private
    def set_auth_token
      return if authentication_token.present?
      self.authentication_token = generate_auth_token
    end

    def generate_auth_token
      loop do
        token = SecureRandom.hex
        break token unless self.class.exists?(authentication_token: token)
      end
    end

  # def self.find_for_database_authentication(warden_conditions)
  #       conditions = warden_conditions.dup
  #       if email = conditions.delete(:email)
  #           logger.info "==1==="
  #           where(conditions.to_h).where(["lower(email) = :value", { :value => email.downcase }]).first
  #       elsif conditions.has_key?(:email)
  #           logger.info "==2==="
  #           where(conditions.to_h).first
  #       end
  # end
end
