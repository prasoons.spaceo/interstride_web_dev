class UserAnswer < ActiveRecord::Base

	##Relationship
	belongs_to :gameplan_questions, :foreign_key => :gameplan_question_id, dependent: :destroy

	##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def self.get_user_answers(params,current_user)
		user_answer = UserAnswer.joins("left join gameplan_questions as b on user_answers.gameplan_question_id=b.id left join question_categories as c on b.category_id=c.id").select(" category_id,sum(question_points) as question_points,sum(answer_points) as answer_points,count(b.id)*100 as total,group_concat(response SEPARATOR '@@') as response,category_name").group("category_id,category_name").where("user_answers.user_id=?",current_user.id)
		return user_answer
	end
end
