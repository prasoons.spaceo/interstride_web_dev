class UserChecklist < ActiveRecord::Base

	##Relationship
	belongs_to :users, foreign_key: :user_id, class_name: 'User'
	belongs_to :checklist, foreign_key: :checklist_id, class_name: 'Checklist'

	##Scope
  	default_scope -> { where(is_deleted: false) }

  	
	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def as_json(options ={})
		json = super(options)
		json[:is_mark] = self.is_mark == true ? 1 : 0
		json
	end
end
