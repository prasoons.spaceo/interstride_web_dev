class UserChecklistCategory < ActiveRecord::Base

	##Relationship
	belongs_to :checklist_category 
	belongs_to :user

	##Scope
  	default_scope -> { where(is_deleted: false) }

  	
	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def as_json(options ={})
		json = super(options)
		json[:is_mark] = self.is_mark == true ? 1 : 0
		json
	end
end
