class UserDevice < ActiveRecord::Base

	#Relationship
	belongs_to :user

   	JSON_LIST = [:id, :user_id, :device_token, :platform, :os_version, :device_name,:app_version,:app_version]

   	##Scope
	default_scope -> { where(is_deleted: false) }

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end
end
