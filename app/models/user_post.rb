class UserPost < ActiveRecord::Base
	PostTypes = ["normal", "search"]

	#Image
	has_attached_file :post_image,
	styles: { thumb: "50x50", medium: "300x300"},
	default_url: ""

	#Relationships
  	has_one   :user, :dependent => :destroy
  	has_many  :delete_school_posts, :foreign_key => :user_post_id, :dependent => :destroy
	has_many :favorite_posts
	## Added by Dhara ##
	has_many :post_comments, dependent: :destroy
	has_many :post_likes, dependent: :destroy

  	POST_JSON_LIST = [:id,:title,:description,:posted_by,:post_image,:created_at,:share_link,:post_type,:search_json,:ptype,:post_school_id,:like_count,:comment_count]
	PER_PAGE = 10

	##Scope
	default_scope -> { where(is_deleted: false) }

	##Validations
	validates_attachment :post_image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
	# validates :title, presence: true
	# validates :post_type, presence: true

	def post_image_url
	    post_image.url != "" ? post_image.url : ""
	end

	#Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	# def as_json(options ={})
	# 	json =  super(options)
	# 	json[:search_json] = self.search_json ? self.search_json : {}
	#
	# 	if options[:current_user].user_type == 2
	# 		school_id = options[:current_user].parent_school_id != 0 ? options[:current_user].parent_school_id : options[:current_user].id
	# 	elsif options[:current_user].user_type == 3
	# 		school_id = options[:current_user].school_id
	# 	end
	# 	commented_users = User.select(" group_concat(id) as ids ").where(" id=#{school_id} OR school_id = #{school_id} OR parent_school_id = #{school_id}")
    #     post_comment = PostComment.where(" user_post_id = #{self.id} AND user_id IN (#{commented_users[0].ids})")
	# 	json[:comment_count] = post_comment.count
	#
	# 	if self.id
	# 		json[:is_like] = PostLike.find_by(:user_post_id => self.id,:user_id => options[:current_user].id,:like => 1) ? true : false
	# 		favorite_post = FavoritePost.find_by(:user_post_id => self.id,:user_id => options[:current_user].id)
	# 		# logger.info "====favport----#{favorite_post.inspect}"
	# 		json[:is_favorite] = favorite_post.blank? == false ? favorite_post.is_favorite : false
	# 	end
	# 	# logger.info "==json==#{json.inspect}"
	# 	json
	#
	# end

	def as_json(options ={})
		   json =  super(options)
		   json[:search_json] = self.search_json ? self.search_json : {}

		   if options[:current_user].user_type == 2
				   school_id = options[:current_user].parent_school_id != 0 ? options[:current_user].parent_school_id : options[:current_user].id
		   elsif options[:current_user].user_type == 3
				   school_id = options[:current_user].school_id
		   end
		   commented_users = User.select(" group_concat(id) as ids ").where(" id=#{school_id} OR school_id = #{school_id} OR parent_school_id = #{school_id}")
   		   post_comment = PostComment.where(" user_post_id = #{self.id} AND user_id IN (#{commented_users[0].ids})")
		   json[:comment_count] = post_comment.count
		   if self.id
				   json[:is_like] = PostLike.find_by(:user_post_id => self.id,:user_id => options[:current_user].id,:like => 1) ? true : false
				   favorite_post = FavoritePost.find_by(:user_post_id => self.id,:user_id => options[:current_user].id)
				   json[:is_favorite] = favorite_post.blank? == false ? favorite_post.is_favorite : false
		   end

		   json

   end

end
