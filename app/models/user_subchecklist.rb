class UserSubchecklist < ActiveRecord::Base

	##Relationship
	belongs_to :users, foreign_key: :user_id, class_name: 'User'
	belongs_to :checklist_subcategory, foreign_key: :subcategory_id, class_name: 'ChecklistSubcategory'

	##Scope
  	default_scope -> { where(is_deleted: false) }

  	
	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

	def as_json(options ={})
		json = super(options)
		json[:is_mark] = self.is_mark == true ? 1 : 0
		json
	end
end
