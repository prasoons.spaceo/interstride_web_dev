class Video < ActiveRecord::Base

  #has_attached_file :video_file, styles: {thumbnail: "60x60#"}, if: lambda { |a| p "==ins===#{a.instance.inspect}"}
  # has_attached_file :video_file, styles: {thumbnail: "60x60#"}, if: :is_application? == 0
  # has_attached_file :video_file, :styles => {
  #   :medium => { :geometry => "640x480", :format => 'flv' },
  #   :thumb => { :geometry => "100x100", :format => 'jpg' }
  # }, :processors => [:transcoder]

  # has_attached_file :video_file, styles: lambda { |a| a.instance.is_application? ? {thumbnail: "700x700#"} : {:thumb => { :geometry => "200x200", :format => 'jpg',:processors => [:transcoder] }}},default_url: ""

  has_attached_file :video_file,styles: lambda { |a| a.instance.check_file_type },
    processors: lambda {
      |a| a.is_video? ? [ :transcoder ] : [ :thumbnail ]
    }

  has_attached_file :display_image


  VIDEO_JSON_LIST = [:id,:video_category_id,:video_file_meta,:video_file,:video_file_file_name,:title,:vtype,:video_school_id,:duration,:youtube_link,:video_file_content_type,:web_link]

  ##relationshoip
  belongs_to :video_category, :dependent => :destroy

	##Scope
  default_scope -> { where(is_deleted: false) }
  scope :videos, -> { where('video_file_file_name is not null and video_file_content_type = ?', 'video/*') }
  scope :pdfs, -> { where('video_file_file_name is not null and video_file_content_type = ?', 'application/pdf') }
  scope :docs, -> { where('video_file_file_name is not null and video_file_content_type in (?)', ["application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/doc", "application/msword"]) }
  scope :excel_sheets, -> { where('video_file_file_name is not null and video_file_content_type in (?)', ["application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]) }
  scope :ppts, -> { where('video_file_file_name is not null and video_file_content_type in (?)', ["application/vnd.ms-powerpoint"]) }
  scope :web_links, -> { where('web_link is not null and video_file_file_name is null and youtube_link = ?', "") }
  scope :youtube_links, -> { where('youtube_link is not null and video_file_file_name is null and web_link = ?', "") }
    # scope :web_links, -> where('web_link is not null')

  ##Validations
  # validates_attachment_content_type :video_file, :content_type => ["application/pdf","application/msword","video/mp4","video/3gpp","video/x-ms-wmv","video/mov","video/flv"]
  # validates_attachment_content_type :video_file, :content_type => ["application/pdf","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","video/mp4","video/3gpp","video/x-ms-wmv","video/mov","video/flv"]

  validates_attachment_content_type :video_file,
    :content_type => [
      "video/mp4",
      "video/quicktime",
      "video/3gpp",
      "video/x-ms-wmv",
      "video/mov",
      "video/flv",

      # "image/jpg",
      # "image/jpeg",
      # "image/png",
      # "image/gif",
      "application/pdf",

      # "audio/mpeg",
      # "audio/x-mpeg",
      # "audio/mp3",
      # "audio/x-mp3",

      # "file/txt",
      # "text/plain",
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      "application/doc",
      "application/msword",

      "application/vnd.ms-excel",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

      # "application/vnd.ms-powerpoint"
      ]

  validates_attachment_content_type :display_image, content_type: [ 'image/*' ]

  #Methods
  def destroy
    update_attribute(:is_deleted, true);
  end

  def is_application?
    p "====is application===#{video_file.instance.video_file_content_type =~ %r(application)}"
    video_file.instance.video_file_content_type =~ %r(application)
  end

  def is_image?
    self.video_file.content_type =~ %r(image)
  end

  def is_video?
    self.video_file.content_type =~ %r(video)
  end

  def is_audio?
    self.video_file.content_type =~ /\Aaudio\/.*\Z/
  end

  def is_plain_text?
    self.video_file_file_name =~ %r{\.(txt)$}i
  end

  def is_excel?
    self.video_file_file_name =~ %r{\.(xls|xlt|xla|xlsx|xlsm|xltx|xltm|xlsb|xlam|csv|tsv)$}i
  end

  def is_word_document?
    self.video_file_file_name =~ %r{\.(docx|doc|dotx|docm|dotm)$}i
  end

  def is_powerpoint?
    self.video_file_file_name =~ %r{\.(pptx|ppt|potx|pot|ppsx|pps|pptm|potm|ppsm|ppam)$}i
  end

  def is_pdf?
    self.video_file_file_name =~ %r{\.(pdf)$}i
  end

  def has_default_image?
    is_audio?
    is_plain_text?
    is_excel?
    is_word_document?
  end

  # If the uploaded content type is an audio file,
  # return false so that we'll skip audio post processing
  def apply_post_processing?
    if self.has_default_image?
      return false
    else
      return true
    end
  end

  # Method to be called in order to determine what styles we should
  # save of a file.
  def check_file_type
    if self.is_image?
      {
        :thumb => "200x200>",
        :medium => "500x500>"
      }
    elsif self.is_pdf?
      {
        :thumb => ["200x200>", :png],
        :medium => ["500x500>", :png]
      }

    elsif self.is_video?
      {
        :thumb => {
          :geometry => "200x200>",
          :format => 'jpg',
          :time => 0
        },
        :medium => {
          :geometry => "500x500>",
          :format => 'jpg',
          :time => 0
        }
      }
    elsif self.is_audio?
      {
        :audio => {
          :format => "mp3"
        }
      }
    else
      {}
    end
  end

  def video_file_url
    video_file.url
  end

  def as_json(options={})
    json = super(options)
    json[:video_thumbnail] = self.video_file.blank? == false ? video_file.url(:thumbnail) : ""
    json[:video_file_url] = self.video_file.blank? == false ? video_file.url(:original) : ""
    json[:youtube_link] = self.youtube_link ? self.youtube_link : ""
    json
  end

end
