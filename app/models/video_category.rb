class VideoCategory < ActiveRecord::Base

	VIDEO_CATEGORY_JSON_LIST = [:id,:category_name]

	#Relationships
    has_many  :videos, :foreign_key => :video_category_id, :dependent => :destroy

    ##Scope
	default_scope -> { where(is_deleted: false) }

	validates :category_name, presence: true

	##Methods
	def destroy
		update_attribute(:is_deleted, true);
	end

end
