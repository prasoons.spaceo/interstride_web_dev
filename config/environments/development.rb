Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = false

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :local

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # config.action_mailer.delivery_method ||= :smtp
  # config.action_mailer.default :charset => "utf-8"
  # config.action_mailer.default_url_options = { :host => 'http://34.233.24.50/interstride/' }

  # ActionMailer::Base.delivery_method = :smtp
  # Rails.logger "=====env===#{ENV['GMAIL_USERNAME']}"
  # RAILS_DEFAULT_LOGGER.info { "=====env===#{ENV['GMAIL_USERNAME']}" }
    # ActionMailer::Base.smtp_settings = {
    #    address: "smtp.gmail.com",
    #    port: 587,
    #    domain: "http://34.233.24.50/interstride/",
    #    user_name: Rails.application.secrets.GMAIL_USERNAME,
    #    password: Rails.application.secrets.GMAIL_PASSWORD,
    #    authentication: "plain",
    #    enable_starttls_auto: true
    # }

  config.action_mailer.delivery_method ||= :smtp
  config.action_mailer.default :charset => "utf-8"
  config.action_mailer.default_url_options = { :host => 'https://web.interstride.com' }
  config.action_mailer.smtp_settings = {
    address: "smtp.gmail.com",
    port: 587,
    domain: "admin.interstride.com",
    authentication: "plain",
    user_name: ENV['GMAIL_USERNAME'],
    password: ENV['GMAIL_PASSWORD']
  }


  # config.action_mailer.smtp_settings = {
  #     address: "smtp.gmail.com",
  #     port: 587,
  #     domain: "http://34.233.24.50/interstride/",
  #     authentication: "plain",
  #     user_name: ENV['GMAIL_USERNAME'],
  #     password: ENV['GMAIL_PASSWORD']
  # }
  # config.action_mailer.default_url_options = { :host => "https://dev1.spaceo.in/interstride_web" }

  config.paperclip_defaults = {
                                :storage => :s3,
                                :s3_region => 'us-east-1',
                                :s3_protocol => :https,
                                :s3_permissions => 'public-read',
                                :s3_credentials => {
                                        :bucket => "interstride",
                                        :access_key_id => 'AKIAIBBAFKEMKYGWA5XQ',
                                        :secret_access_key => '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',
                                      },
                                :url => ":s3_domain_url",
                                :path => "/development/:class/:attachment/:id/:style/:filename"
                              }
  Paperclip.options[:image_magick_path] = "/opt/ImageMagick/bin"
  Paperclip.options[:command_path] = "/opt/ImageMagick/bin"

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker
end
