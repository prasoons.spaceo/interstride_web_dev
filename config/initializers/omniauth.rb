Rails.application.config.middleware.use OmniAuth::Builder do
    provider :linkedin, Rails.application.secrets.LINKEDIN_KEY, Rails.application.secrets.LINKEDIN_SECRET
end
