SocialShareButton.configure do |config|
    config.allow_sites = %w(facebook twitter linkedin email)
end
