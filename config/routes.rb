Rails.application.routes.draw do
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    # resources :users do
    #     get :sign_up, on: :collection
    #     # get :sign_in, on: :collection
    #     post :sign_in_user
    # end

    get '/404' => "errors#not_found"
    get '/422' => "errors#unacceptable"
    get '/500' => "errors#internal_error"

    get '/auth/:provider/callback', to: 'oauth#callback', as: 'oauth_callback'
    get '/auth/failure', to: 'oauth#failure', as: 'oauth_failure'


    devise_scope :user do
        authenticated :user do
            get '/' => 'home#index', as: :authenticated_root
        end

        unauthenticated do
            root 'sessions#new', as: :unauthenticated_root
        end
    end

    devise_for :users, controllers: { registrations: 'registrations', sessions: "sessions", passwords: "passwords" }
    devise_scope :user do
        get "user/password/successful_reset_password" , :to => "devise/passwords#successful_reset_password" , :as => :successful_reset_password
        post "verify_school_user", :to => "registrations#verify_school_user"
    end

    resources :users, only: [ :edit, :update ] do
        post :reset_password, on: :collection
        put :change_password, on: :collection
        post :upload_resume, on: :collection
        get :download_resume, on: :collection
    end

    resources :companies do
        get :get_cities, on: :collection
        post :search, on: :collection
        get :fetch_master_data, on: :collection
        post :add_company_as_favorite, on: :collection
        get :sponsored_petitions, on: :collection
        get :search_industry, on: :collection
        get :search_company, on: :collection
        get :company_select2, on: :collection
        get :industry_select2, on: :collection
        get :university_select2, on: :collection
        get :birth_country_select2, on: :collection
        get :job_title_select2, on: :collection
        get :state_select2, on: :collection
        get :city_select2, on: :collection
        post :fetch_map_data, on: :collection
    end

    resources :videos do
        get :get_video_list, on: :collection
        get :search, on: :collection
        get :order_list, on: :collection
    end

    resources :jobs, only: :index do
        post :search, on: :collection
        get :get_open_jobs, on: :collection
        post :favorite_job, on: :collection
    end

    resources :country_insights, only: [:index, :show] do
        get :search, on: :collection
    end

    resources :notifications, only: [ :index ] do
        get :read, on: :member
    end
    resources :gameplan, only: [ :index ] do
        get :take_test, on: :collection
        post :user_answers, on: :collection
    end
    resources :checklists, only: [ :index, :create ] do
        post 'delete_checklist', on: :collection, as: 'delete_checklist'
        post 'edit_checklist', on: :collection, as: 'edit_checklist'
        post 'mark_checklist', on: :collection, as: 'mark_checklist'
    end
    resources :network_trackers do
        get :search, on: :collection
    end
    resources :favorite_companies, only: [ :index, :create ]
    resources :feedbacks, only: [ :new, :create ]
    resources :contact_us, only: [ :new, :create ]

    get :list_post_comments, to: 'home#list_post_comments'
    post :add_comment, to: 'home#add_comment'
    post :like_post, to: 'home#like_post'
    post :add_post, to: 'home#add_post'
    post :favorite_post, to: 'home#favorite_post'
    get :get_link_info, to: 'home#get_link_info'
    post :delete_comment, to: 'home#delete_comment'
    post :bookmarked_articles, to: 'home#bookmarked_articles'
    get :search, to: 'home#search'
    get :delete_post, to: 'home#delete_post'

end
