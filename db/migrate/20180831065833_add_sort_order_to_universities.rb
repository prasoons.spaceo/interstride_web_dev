class AddSortOrderToUniversities < ActiveRecord::Migration[5.2]
  def change
    add_column :universities, :sort_order, :integer
  end
end
