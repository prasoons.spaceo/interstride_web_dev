class CreateFavoriteJobs < ActiveRecord::Migration[5.2]
    def change
        create_table :favorite_jobs do |t|
            t.integer :user_id, index: true, foreign_key: true
            t.string :job_id
            t.boolean :is_favorite, default: false
            t.boolean :is_deleted, default: false

            t.timestamps
        end
    end
end
