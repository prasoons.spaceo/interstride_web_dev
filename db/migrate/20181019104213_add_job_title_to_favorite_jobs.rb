class AddJobTitleToFavoriteJobs < ActiveRecord::Migration[5.2]
    def change
        add_column :favorite_jobs, :job_title, :string
        add_column :favorite_jobs, :job_link, :text
    end
end
