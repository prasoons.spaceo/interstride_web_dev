class AddCompanyNameToFavoriteJobs < ActiveRecord::Migration[5.2]
    def change
        add_column :favorite_jobs, :company_name, :string
    end
end
