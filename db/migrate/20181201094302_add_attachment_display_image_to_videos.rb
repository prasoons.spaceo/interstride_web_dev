class AddAttachmentDisplayImageToVideos < ActiveRecord::Migration[5.2]
    def self.up
        change_table :videos do |t|
            t.attachment :display_image
        end
    end

    def self.down
        remove_attachment :videos, :display_image
    end
end
