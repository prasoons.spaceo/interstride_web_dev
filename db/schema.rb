# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_01_094302) do

  create_table "apps_countries", id: :integer, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "country_code", limit: 2, default: "", null: false
    t.string "country_name", limit: 100, default: "", null: false
  end

  create_table "birth_countries", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "checklist_categories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "category_name"
    t.integer "school_id", default: 0, null: false
    t.integer "ctype", default: 0, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id", default: 0
    t.integer "sort_order", default: 0
  end

  create_table "checklist_subcategories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "subcategory_name"
    t.integer "category_id"
    t.integer "user_id", default: 0
    t.integer "school_id", default: 0, null: false
    t.integer "ctype", default: 0, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0
  end

  create_table "checklists", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "title"
    t.integer "subcategory_id", default: 0, null: false
    t.integer "school_id", default: 0, null: false
    t.integer "ctype", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0
  end

  create_table "cities", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "city_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0, null: false
    t.integer "state_id", default: 0, null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "cities_16_17", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "city_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0, null: false
    t.integer "state_id", default: 0, null: false
  end

  create_table "companies", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "company_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0, null: false
  end

  create_table "company_details", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "id", null: false
    t.integer "petition_id"
    t.integer "employer_id"
    t.integer "company_id"
    t.string "employer_name"
    t.integer "job_title_id"
    t.string "job_title"
    t.integer "industry_id"
    t.string "industry"
    t.bigint "wage_rate_of_pay_from"
    t.integer "city_id"
    t.string "worksite_city"
    t.string "worksite_country"
    t.integer "state_id"
    t.string "worksite_state"
    t.string "headquaters"
    t.string "worksite_postal_code", limit: 100
    t.string "website"
    t.string "crunchbase_link"
    t.string "wikipedia_link"
    t.string "indeed_link"
    t.string "linkedin_link"
    t.integer "no_of_petition"
    t.string "employees"
    t.string "company_description", limit: 5000
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "company_logo"
    t.integer "revenue"
    t.string "company_ceo"
    t.string "news_link"
    t.decimal "latitude", precision: 11, scale: 8, default: "0.0", null: false
    t.decimal "longitude", precision: 11, scale: 8, default: "0.0", null: false
    t.string "glassdoor_link", default: ""
    t.integer "year", default: 2016, null: false
  end

  create_table "company_details_16_17", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "petition_id"
    t.integer "employer_id"
    t.integer "company_id"
    t.string "employer_name"
    t.integer "job_title_id"
    t.string "job_title"
    t.integer "industry_id"
    t.string "industry"
    t.bigint "wage_rate_of_pay_from"
    t.integer "city_id"
    t.string "worksite_city"
    t.string "worksite_country"
    t.integer "state_id"
    t.string "worksite_state"
    t.string "headquaters"
    t.string "worksite_postal_code", limit: 100
    t.string "website"
    t.string "crunchbase_link"
    t.string "wikipedia_link"
    t.string "indeed_link"
    t.string "linkedin_link"
    t.integer "no_of_petition"
    t.string "employees"
    t.string "company_description", limit: 5000
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "company_logo"
    t.integer "revenue"
    t.string "company_ceo"
    t.string "news_link"
    t.decimal "latitude", precision: 11, scale: 8, default: "0.0", null: false
    t.decimal "longitude", precision: 11, scale: 8, default: "0.0", null: false
    t.string "glassdoor_link", default: ""
    t.integer "year", default: 0, null: false
  end

  create_table "contact_us", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.string "topic", limit: 200, default: "", null: false
    t.string "comment"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "name", null: false
    t.integer "points", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "flag_file_name"
    t.string "flag_content_type"
    t.integer "flag_file_size"
    t.datetime "flag_updated_at"
  end

  create_table "country_guides", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "country_name", default: "", null: false
    t.string "job_site_link", default: ""
    t.string "cia_link", default: ""
    t.string "numbeo", default: ""
    t.string "hofstede_insights", default: ""
    t.string "us_travel_advisory", default: ""
    t.string "cdc_gov", default: ""
    t.string "immegration_website", default: ""
    t.string "lonely_planet", default: ""
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "country_image", default: ""
    t.string "background", limit: 5000, default: ""
    t.string "capital", limit: 5000, default: ""
    t.string "unemployment", default: ""
    t.string "population", default: ""
    t.string "gdp_growth", default: ""
    t.string "gdp_capita", default: ""
  end

  create_table "country_jobsites", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "country_guide_id", default: 0, null: false
    t.string "jobsite_link"
    t.string "title"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delete_school_posts", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "deleted_by", default: 0, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_post_id"
    t.integer "user_id"
    t.index ["user_id"], name: "index_delete_school_posts_on_user_id"
    t.index ["user_post_id"], name: "index_delete_school_posts_on_user_post_id"
  end

  create_table "emp_functions", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "favorite_companies", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "company_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.string "company_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tab_type", default: "null"
  end

  create_table "favorite_jobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "user_id"
    t.string "job_id"
    t.boolean "is_favorite", default: false
    t.boolean "is_deleted", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "job_title"
    t.text "job_link"
    t.string "company_name"
    t.index ["user_id"], name: "index_favorite_jobs_on_user_id"
  end

  create_table "favorite_posts", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.boolean "is_favorite"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_post_id"
    t.integer "user_id"
    t.index ["user_id"], name: "index_favorite_posts_on_user_id"
    t.index ["user_post_id"], name: "index_favorite_posts_on_user_post_id"
  end

  create_table "gameplan_answers", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "answer"
    t.integer "points"
    t.integer "option_id"
    t.integer "question_id"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gameplan_options", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "option"
    t.integer "question_id"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gameplan_questions", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "question"
    t.integer "undergraduate_points"
    t.integer "graduate_points"
    t.integer "category_id"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "question_type"
    t.string "degree"
    t.string "response", limit: 5000
    t.integer "us_points", default: 0, null: false
    t.integer "foster_points", default: 0, null: false
  end

  create_table "graduation_years", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "year", default: "", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "green_card_cities", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "city_name"
    t.boolean "is_deleted", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "state_id", default: 0, null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "green_card_companies", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "company_name"
    t.boolean "is_deleted", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "green_card_details", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "employer_id", default: 0, null: false
    t.string "employer_name", null: false
    t.string "job_title"
    t.string "industry", null: false
    t.string "worksite_city", null: false
    t.string "worksite_state", null: false
    t.integer "worksite_postal_code", null: false
    t.decimal "latitude", precision: 11, scale: 8, default: "0.0", null: false
    t.decimal "longitude", precision: 11, scale: 8, default: "0.0", null: false
    t.bigint "wage_rate_of_pay_from"
    t.string "worksite_country"
    t.string "university"
    t.integer "company_id"
    t.integer "industry_id"
    t.integer "job_title_id"
    t.integer "city_id"
    t.integer "state_id"
    t.integer "university_id"
    t.integer "birth_country_id"
    t.string "website"
    t.string "crunchbase_link"
    t.string "wikipedia_link"
    t.string "indeed_link"
    t.string "linkedin_link"
    t.string "glassdoor_link"
    t.string "employees"
    t.string "company_description", limit: 5000
    t.integer "is_deleted", limit: 1, default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "green_card_details_16_17", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "employer_id", default: 0, null: false
    t.string "employer_name", null: false
    t.string "job_title"
    t.string "industry", null: false
    t.string "worksite_city", null: false
    t.string "worksite_state", null: false
    t.integer "worksite_postal_code", null: false
    t.decimal "latitude", precision: 11, scale: 8, default: "0.0", null: false
    t.decimal "longitude", precision: 11, scale: 8, default: "0.0", null: false
    t.bigint "wage_rate_of_pay_from"
    t.string "worksite_country"
    t.string "university"
    t.integer "company_id"
    t.integer "industry_id"
    t.integer "job_title_id"
    t.integer "city_id"
    t.integer "state_id"
    t.integer "university_id"
    t.integer "birth_country_id"
    t.string "website"
    t.string "crunchbase_link"
    t.string "wikipedia_link"
    t.string "indeed_link"
    t.string "linkedin_link"
    t.string "glassdoor_link"
    t.string "employees"
    t.string "company_description", limit: 5000
    t.integer "is_deleted", limit: 1, default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "year", default: 0, null: false
    t.index ["birth_country_id"], name: "birth_country_id"
    t.index ["city_id"], name: "city_id"
    t.index ["company_id"], name: "company_id"
    t.index ["industry_id"], name: "industry_id"
    t.index ["job_title_id"], name: "job_title_id"
    t.index ["state_id"], name: "state_id"
    t.index ["university_id"], name: "university_id"
  end

  create_table "green_card_industries", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "industry_name"
    t.boolean "is_deleted", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "green_card_job_titles", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "job_title_name"
    t.boolean "is_deleted", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "green_card_states", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "state_name"
    t.boolean "is_deleted"
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "indeed_companies", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name"
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "indeed_countries", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "industries", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "industry_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0, null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "industries_16_17", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "industry_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0, null: false
  end

  create_table "job_titles", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "job_title_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0, null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "job_titles_16_17", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "job_title_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0, null: false
  end

  create_table "message_topics", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "topic_name", limit: 1000
    t.string "topic_description", limit: 5000
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "last_message_id"
  end

  create_table "messages", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "message_topic_id", null: false
    t.integer "sender_id", null: false
    t.integer "receiver_id", null: false
    t.string "message", limit: 5000, default: "", null: false
    t.boolean "is_read", default: false, null: false
    t.boolean "is_sender_delete", default: false, null: false
    t.boolean "is_receiver_delete", default: false, null: false
    t.boolean "is_deleted", default: false, null: false
    t.string "mtype"
    t.string "message_media_file_name"
    t.string "message_media_content_type"
    t.integer "message_media_file_size"
    t.datetime "message_media_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "network_trackers", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "company_id", null: false
    t.integer "user_id"
    t.string "contact_name"
    t.string "contact_email"
    t.date "appointment_date"
    t.string "notes", limit: 5000
    t.string "followup_notes", limit: 5000
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "company_name", default: "", null: false
    t.string "icalendar_id"
    t.string "google_calendar_id"
    t.time "start_time"
    t.time "end_time"
  end

  create_table "notifications", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.string "message"
    t.string "type"
    t.integer "type_id"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_read", default: false
    t.string "title", default: ""
  end

  create_table "old_country_guides", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "country_name", default: "", null: false
    t.string "job_site_link", default: ""
    t.string "cia_link", default: ""
    t.string "numbeo", default: ""
    t.string "hofstede_insights", default: ""
    t.string "us_travel_advisory", default: ""
    t.string "cdc_gov", default: ""
    t.string "immegration_website", default: ""
    t.string "lonely_planet", default: ""
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "country_image", default: ""
    t.string "background", limit: 5000, default: ""
    t.string "capital", limit: 5000, default: ""
    t.string "unemployment", default: ""
    t.string "population", default: ""
    t.string "gdp_growth", default: ""
    t.string "gdp_capita", default: ""
  end

  create_table "old_green_card_details", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "employer_id", default: 0, null: false
    t.string "employer_name", default: "", null: false
    t.integer "company_id"
    t.integer "industry_id"
    t.string "industry", default: "", null: false
    t.integer "job_title_id"
    t.string "job_title", default: "", null: false
    t.string "wage_rate_of_pay_from", default: ""
    t.integer "city_id"
    t.string "worksite_city", default: "", null: false
    t.integer "state_id"
    t.string "worksite_state", default: "", null: false
    t.integer "birth_country_id"
    t.string "worksite_country", default: "", null: false
    t.integer "university_id"
    t.string "university", default: ""
    t.integer "worksite_postal_code", default: 0, null: false
    t.string "website", default: "", null: false
    t.string "crunchbase_link", default: ""
    t.string "wikipedia_link", default: ""
    t.string "indeed_link", default: ""
    t.string "linkedin_link", default: ""
    t.string "glassdoor_link", default: ""
    t.string "employees", default: "", null: false
    t.string "company_description", limit: 5000, default: ""
    t.decimal "latitude", precision: 11, scale: 8, default: "0.0", null: false
    t.decimal "longitude", precision: 11, scale: 8, default: "0.0", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["birth_country_id"], name: "index_university_details_on_birth_country_id"
    t.index ["city_id"], name: "index_university_details_on_city_id"
    t.index ["company_id"], name: "index_university_details_on_company_id"
    t.index ["industry_id"], name: "index_university_details_on_industry_id"
    t.index ["job_title_id"], name: "index_university_details_on_job_title_id"
    t.index ["state_id"], name: "index_university_details_on_state_id"
    t.index ["university_id"], name: "index_university_details_on_universities_id"
  end

  create_table "post_comments", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_post_id"
    t.integer "user_id"
    t.string "comment"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_post_comments_on_user_id"
    t.index ["user_post_id"], name: "index_post_comments_on_user_post_id"
  end

  create_table "post_likes", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "user_post_id"
    t.integer "user_id"
    t.integer "like"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_post_likes_on_user_id"
    t.index ["user_post_id"], name: "index_post_likes_on_user_post_id"
  end

  create_table "postal_code_latitude_longitudes", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "postal_code", limit: 500, null: false
    t.decimal "latitude", precision: 11, scale: 8
    t.decimal "longitude", precision: 11, scale: 8
    t.index ["postal_code"], name: "postal_code", length: 191
  end

  create_table "question_categories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "category_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "school_settings", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "school_id", default: 0, null: false
    t.boolean "force_gameplan", default: false, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "school_users", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "school_id", null: false
    t.string "user_email", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_registered", default: false, null: false
    t.string "degree_level"
  end

  create_table "send_feedbacks", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "rate", default: 0, null: false
    t.string "comment"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "send_invitations", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "type", default: "", null: false
    t.integer "school_id", default: 0, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "send_notifications", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "notification", default: "", null: false
    t.string "type", default: "", null: false
    t.integer "school_id", default: 0, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sessions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "states", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "state_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_order", default: 0, null: false
    t.integer "year", default: 2016, null: false
  end

  create_table "static_pages", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "vTitle", limit: 30, default: "", null: false
    t.text "vContent"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "universities", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "sort_order"
    t.integer "year", default: 2016, null: false
  end

  create_table "university_cities", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "city_name"
    t.boolean "is_deleted", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "state_id", default: 0, null: false
  end

  create_table "university_companies", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "company_name"
    t.boolean "is_deleted", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "university_details", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "employer_id", default: 0, null: false
    t.string "employer_name", default: "", null: false
    t.integer "graduation_year", default: 0, null: false
    t.string "emp_function", default: "", null: false
    t.string "industry", default: "", null: false
    t.string "job_title", default: "", null: false
    t.string "worksite_city", default: "", null: false
    t.string "worksite_state", default: "", null: false
    t.integer "worksite_postal_code", default: 0, null: false
    t.string "website", default: "", null: false
    t.string "crunchbase_link", default: "", null: false
    t.string "wikipedia_link", default: "", null: false
    t.string "indeed_link", default: "", null: false
    t.string "linkedin_link", default: "", null: false
    t.string "glassdoor_link", default: "", null: false
    t.string "employees", default: "", null: false
    t.string "company_description", limit: 5000, default: ""
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "company_id"
    t.integer "job_title_id"
    t.integer "industry_id"
    t.integer "state_id"
    t.integer "city_id"
    t.decimal "latitude", precision: 11, scale: 8, default: "0.0", null: false
    t.decimal "longitude", precision: 11, scale: 8, default: "0.0", null: false
    t.integer "graduation_year_id"
    t.integer "emp_function_id"
    t.integer "user_id"
    t.index ["city_id"], name: "index_green_card_details_on_city_id"
    t.index ["company_id"], name: "index_green_card_details_on_company_id"
    t.index ["emp_function_id"], name: "index_green_card_details_on_emp_function_id"
    t.index ["graduation_year_id"], name: "index_green_card_details_on_graduation_year_id"
    t.index ["industry_id"], name: "index_green_card_details_on_industry_id"
    t.index ["job_title_id"], name: "index_green_card_details_on_job_title_id"
    t.index ["state_id"], name: "index_green_card_details_on_state_id"
    t.index ["user_id"], name: "index_university_details_on_user_id"
  end

  create_table "university_industries", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "industry_name"
    t.boolean "is_deleted", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "university_job_titles", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "job_title_name"
    t.boolean "is_deleted", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "university_states", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "state_name"
    t.boolean "is_deleted", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_answers", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "gameplan_question_id"
    t.integer "question_points", null: false
    t.string "answer_id", null: false
    t.integer "answer_points", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gameplan_question_id"], name: "index_user_answers_on_gameplan_question_id"
  end

  create_table "user_checklist_categories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "category_id", default: 0, null: false
    t.boolean "is_mark", default: false, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_checklists", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "checklist_id", default: 0, null: false
    t.boolean "is_mark", default: false, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_devices", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "device_token", default: "", null: false
    t.integer "platform", limit: 3, default: 0, null: false
    t.string "device_name", limit: 100, default: "", null: false
    t.string "os_version", limit: 30, default: "", null: false
    t.string "app_version", limit: 30, default: "", null: false
    t.string "api_version", limit: 30, default: "1", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_posts", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "title", limit: 1000
    t.string "description", limit: 5000
    t.string "posted_by"
    t.string "post_image_file_name"
    t.string "post_image_content_type"
    t.integer "post_image_file_size"
    t.datetime "post_image_updated_at"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "post_type"
    t.string "search_json"
    t.string "share_link", limit: 5000
    t.integer "ptype", default: 0, null: false
    t.integer "post_school_id", default: 0, null: false
    t.integer "like_count", default: 0, null: false
    t.integer "comment_count", default: 0, null: false
  end

  create_table "user_subchecklists", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "subcategory_id", default: 0, null: false
    t.boolean "is_mark", default: false, null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_types", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "user_type", limit: 30, default: "", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "authentication_token", limit: 500
    t.integer "user_type", default: 0
    t.integer "school_id", default: 0, null: false
    t.string "school_user_email", default: "", null: false
    t.string "full_name", limit: 100, default: "", null: false
    t.boolean "is_deleted", default: false, null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "phone", limit: 30, default: "", null: false
    t.string "linkedIn_id", limit: 100, default: ""
    t.date "expected_graduation"
    t.string "address", limit: 300, default: ""
    t.decimal "latitude", precision: 10, default: "0", null: false
    t.decimal "longitude", precision: 10, default: "0", null: false
    t.boolean "is_push", default: true, null: false
    t.boolean "is_active", default: true, null: false
    t.boolean "is_admin", default: false, null: false
    t.boolean "is_verified", default: false, null: false
    t.boolean "is_blocked", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user_profile_file_name"
    t.string "user_profile_content_type"
    t.integer "user_profile_file_size"
    t.datetime "user_profile_updated_at"
    t.string "gender"
    t.integer "parent_school_id", default: 0, null: false
    t.string "degree_level"
    t.integer "school_admin_id", default: 0, null: false
    t.boolean "has_child_school", default: false, null: false
    t.string "resume_file_name"
    t.string "resume_content_type"
    t.integer "resume_file_size"
    t.datetime "resume_updated_at"
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "video_categories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "category_name"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "ptype", default: 0, null: false
    t.integer "video_school_id", default: 0, null: false
  end

  create_table "videos", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "video_category_id"
    t.string "video_file_meta", default: "", null: false
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.string "video_file_file_name"
    t.string "video_file_content_type", default: ""
    t.integer "video_file_file_size"
    t.datetime "video_file_updated_at"
    t.string "duration"
    t.integer "vtype", default: 0, null: false
    t.integer "video_school_id", default: 0, null: false
    t.string "youtube_link"
    t.string "web_link", default: ""
    t.string "display_image_file_name"
    t.string "display_image_content_type"
    t.integer "display_image_file_size"
    t.datetime "display_image_updated_at"
    t.index ["video_category_id"], name: "index_videos_on_video_category_id"
  end

  create_table "web_settings", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "screen_name"
    t.string "section_key"
    t.string "section_title"
    t.text "description"
    t.string "page_header"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  add_foreign_key "delete_school_posts", "user_posts"
  add_foreign_key "delete_school_posts", "users"
  add_foreign_key "favorite_posts", "user_posts"
  add_foreign_key "favorite_posts", "users"
  add_foreign_key "university_details", "users"
end
