require 'csv'
namespace :master_data do

    desc "Country Flag Images"
    task import_flag_images: :environment do
        # countries = Country.where('flag_file_name like ?', "%#{'svg'}")
        Country.all.each do |c|
            # p "===c===#{c.name}====f ===#{f.split('.')[0]}"
            # if c.name.eql?(f.split('.')[0])
            remaining = []
            if File.exist?("#{Rails.root}/app/assets/images/flags/#{c.name.strip}.png")
                c.flag = File.open("#{Rails.root}/app/assets/images/flags/"+c.name.strip+".png", 'r')
                c.save
            else
                remaining << c.name
            end
        end
    end

    desc "Indeed Companies"
    task import_indeed_companies: :environment do
        # countries = Country.where('flag_file_name like ?', "%#{'svg'}")
        csv_text = File.read(Rails.root.join('public', 'indeed_companies.csv'))
        csv = CSV.parse(csv_text, :headers => false, :encoding => 'ISO-8859-1')
        csv.each do |row|
            company = IndeedCompany.find_or_create_by(name: row[0], display_name: row[1])
            Rails.logger.info "====inserted===#{row[0]}"
        end
    end

    desc "Indeed Countries"
    task import_indeed_countries: :environment do
        # countries = Country.where('flag_file_name like ?', "%#{'svg'}")
        csv_text = File.read(Rails.root.join('public', 'indeed_countries.csv'))
        csv = CSV.parse(csv_text, :headers => false, :encoding => 'ISO-8859-1')
        csv.each do |row|
            company = IndeedCountry.find_or_create_by(name: row[0], code: row[1])
            Rails.logger.info "====inserted===#{row[0]}"
        end
    end

    desc "update city sort order"
    task update_city_sort_order: :environment do
        # countries = Country.where('flag_file_name like ?', "%#{'svg'}")
        csv_text = File.read(Rails.root.join('public', 'cities.csv'))
        csv = CSV.parse(csv_text, :headers => false, :encoding => 'ISO-8859-1')
        csv.each_with_index do |row, index|
            p "====index===#{index}"
            city = City.find_by(city_name: row[0])
            if city.present?
                city.sort_order = index + 1
                city.save
            end
            p "====inserted===#{city.inspect}"
        end
    end

    desc "update job title sort order"
    task update_job_title_sort_order: :environment do
        # countries = Country.where('flag_file_name like ?', "%#{'svg'}")
        csv_text = File.read(Rails.root.join('public', 'job_titles.csv'))
        csv = CSV.parse(csv_text, :headers => false, :encoding => 'ISO-8859-1')
        JobTitle.update_all(sort_order: 1000)
        csv.each_with_index do |row, index|
            p "====index===#{index}"
            job = JobTitle.find_by(job_title_name: row[0])
            if job.present?
                job.sort_order = index + 1
                job.save
            end
            p "====inserted===#{job.inspect}"
        end
    end

    desc "update uni sort order"
    task update_universities_sort_order: :environment do
        # countries = Country.where('flag_file_name like ?', "%#{'svg'}")
        csv_text = File.read(Rails.root.join('public', 'universities.csv'))
        csv = CSV.parse(csv_text, :headers => false, :encoding => 'ISO-8859-1')
        University.update_all(sort_order: 1000)
        csv.each_with_index do |row, index|
            p "====index===#{index}"
            uni = University.find_by(name: row[0])
            if uni.present?
                uni.sort_order = index + 1
                uni.save
            end
            p "====inserted===#{uni.inspect}"
        end
    end

    desc "update GC cities sort order"
    task update_gc_cities_sort_order: :environment do
        # countries = Country.where('flag_file_name like ?', "%#{'svg'}")
        csv_text = File.read(Rails.root.join('public', 'cities.csv'))
        csv = CSV.parse(csv_text, :headers => false, :encoding => 'ISO-8859-1')
        GreenCardCity.update_all(sort_order: 1000)
        csv.each_with_index do |row, index|
            p "====index===#{index}"
            uni = GreenCardCity.find_by(city_name: row[0])
            if uni.present?
                uni.sort_order = index + 1
                uni.save
            end
            p "====inserted===#{uni.inspect}"
        end
    end

    desc "update GC job titles sort order"
    task update_gc_job_titles_sort_order: :environment do
        # countries = Country.where('flag_file_name like ?', "%#{'svg'}")
        csv_text = File.read(Rails.root.join('public', 'job_titles.csv'))
        csv = CSV.parse(csv_text, :headers => false, :encoding => 'ISO-8859-1')
        GreenCardJobTitle.update_all(sort_order: 1000)
        csv.each_with_index do |row, index|
            p "====index===#{index}"
            uni = GreenCardJobTitle.find_by(job_title_name: row[0])
            if uni.present?
                uni.sort_order = index + 1
                uni.save
            end
            p "====inserted===#{uni.inspect}"
        end
    end

    desc "Update Flags"
    task update_country_flags: :environment do
        Country.all.each do |country|
            code = CountryGuide.joins("left join apps_countries as a on a.country_name=country_guides.country_name").where("lower(country_guides.country_name) = ?", "#{country.name.downcase}").select('lower(country_code) as country_code').first.present? ? CountryGuide.joins("left join apps_countries as a on a.country_name=country_guides.country_name").where("lower(country_guides.country_name) = ?", "#{country.name.downcase}").select('lower(country_code) as country_code').first.country_code : ''
            p "==c===#{code}"
            if code != ""
                country.flag = File.open("#{Rails.root}/app/assets/images/flags/#{code}.png", 'r')
                country.save
                p "---ci=#{country.inspect}"
            end
        end
    end
end
