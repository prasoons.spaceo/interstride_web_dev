require 'open-uri'
namespace :save_thumbnail do

    desc "Save Video thumbnail"
    task save_thumbnail: :environment do
        # countries = Country.where('flag_file_name like ?', "%#{'svg'}")
        # Country.all.each do |c|
        #     # p "===c===#{c.name}====f ===#{f.split('.')[0]}"
        #     # if c.name.eql?(f.split('.')[0])
        #     remaining = []
        #     if File.exist?("#{Rails.root}/app/assets/images/flags/#{c.name.strip}.png")
        #         c.flag = File.open("#{Rails.root}/app/assets/images/flags/"+c.name.strip+".png", 'r')
        #         c.save
        #     else
        #         remaining << c.name
        #     end
        # end
        Video.where('display_image_file_name is null').each do |video|
            if video.youtube_link.present?
                p "====uri----#{LinkThumbnailer.generate(video.youtube_link, verify_ssl: false).images[0].to_s}"
                img = LinkThumbnailer.generate(video.youtube_link, verify_ssl: false).images[0].to_s
                p "===img===#{img}"
                if img.present? && !img.nil?
                    thumb = URI.parse(img)
                else
                    thumb = File.open("#{Rails.root}/app/assets/images/default-icon.png", 'r')
                end
            elsif video.web_link.present?
                thumb = File.open("#{Rails.root}/app/assets/images/weblink.png", 'r')
            elsif video.is_pdf?
                thumb = File.open("#{Rails.root}/app/assets/images/pdf.png", 'r')
            elsif video.is_word_document?
                thumb = File.open("#{Rails.root}/app/assets/images/word.png", 'r')
            elsif video.is_excel?
                thumb = File.open("#{Rails.root}/app/assets/images/excel.png", 'r')
            elsif video.is_powerpoint?
                thumb = File.open("#{Rails.root}/app/assets/images/powerpoint.png", 'r')
            else
                thumb = File.open("#{Rails.root}/app/assets/images/default-icon.png", 'r')
            end
            p "===thumb===#{thumb.inspect}====#{video.id}===#{thumb.present?}====#{thumb.nil?}===#{thumb.blank?}"
            # if thumb.present? && !thumb.blank? || !!thumb.nil?
            #     p "===1==="
            #     video.display_image = thumb
            #     video.save
            # else
            #     p "===2==="
            #     thumb = File.open("#{Rails.root}/assets/default-icon.png", 'r')
            #     video.display_image = thumb
            #     video.save
            # end
            video.display_image = thumb
            if video.save(validate: false)
                p "===saved ===#{video.id}"
            else
                p "==error saving ===#{video.errors.full_messages}"
            end
        end
    end
end
